package com.atlassian.streams.testing.pageobjects.confluence;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.streams.pageobjects.Gadget;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

// STRM-1282: Share this with the common pageobject implementation
public class InsertMacroDialog
{
    private static final String DIALOG_TITLE_CLASS = "dialog-title";

    private final EditPage editPage;

    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder pageBinder;

    @FindBy(id = "macro-browser-dialog")
    private WebElement dialog;

    @FindBy(className = "ok")
    private WebElement insertButton;

    public InsertMacroDialog(EditPage editPage)
    {
        this.editPage = editPage;
    }

    @WaitUntil
    public void waitForDialogToBeVisible()
    {
        driver.waitUntilElementIsVisibleAt(By.className(DIALOG_TITLE_CLASS), dialog);
    }

    public InsertMacroDialog add(String gadgetTitle)
    {
        WebElement element = dialog.findElement(By.className("macro-list"));
        final WebElement gadgetButton = element.findElement(By.cssSelector("img[title=\"" + gadgetTitle + "\"]"));
        gadgetButton.click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver input)
            {
                return gadgetButton.isEnabled();
            }
        });
        return this;
    }

    public EditPage finished()
    {
        insertButton.click();
        driver.waitUntilElementIsNotVisibleAt(By.className(DIALOG_TITLE_CLASS), dialog);
        return editPage;
    }

    public Gadget getGadget()
    {
        driver.waitUntilElementIsLocated(By.id("macro-preview-iframe"));
        driver.switchTo().frame("macro-preview-iframe");
        driver.waitUntilElementIsVisible(By.className("gadget")); //gadget takes a second to render
        String gadgetId = driver.findElement(By.className("gadget")).getAttribute("id");

        // The Insert button may still be disabled, making tests fail.
        // The Insert button often disables itself. You need to change a configuration value to reenable it.
//        if (!driver.findElement(By.cssSelector("button.ok")).isEnabled())
//        {
//            WebElement widthConfig = driver.findElement(By.id("macro-param-width"));
//            widthConfig.clear();
//            widthConfig.sendKeys("500");
//        }

        return pageBinder.bind(Gadget.class, gadgetId);
    }

}
