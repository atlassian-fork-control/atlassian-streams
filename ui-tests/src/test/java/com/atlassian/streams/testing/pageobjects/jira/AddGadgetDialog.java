package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

public class AddGadgetDialog
{
    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder pageBinder;

    @FindBy(id = "gadget-dialog")
    private WebElement dialog;

    @FindBy(id = "category-all")
    private final String currentCategory = "all";

    @FindBy(className = "aui-dialog2-header-close")
    private WebElement finishButton;

    @WaitUntil
    public void waitForDialogToBeVisible()
    {
        driver.waitUntilElementIsVisible(By.id("gadget-dialog"));
    }

    public AddGadgetDialog addGadget(String gadgetTitle)
    {
        final WebElement addButton = dialog.findElement(By.xpath("//*/div[@data-item-title='" + gadgetTitle + "']")).findElement(By.className("add-dashboard-item-selector"));
        addButton.click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver input)
            {
                return addButton.isEnabled();
            }
        });
        return this;
    }

    public AddGadgetDialog loadAllGadgets() {
        waitForLoadAllGadgetsButtonToAppear();
        driver.findElement(By.id("load-more-directory-items")).click();
        return this;
    }

    private void waitForLoadAllGadgetsButtonToAppear() {
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver input)
            {
                return  !driver.findElements(By.id("load-more-directory-items")).isEmpty();
            }
        });
    }

    private String gadgetId(String gadgetTitle)
    {
        String prefix = currentCategory.equals("all") ? "" : currentCategory + "-";
        return prefix + "macro-" + gadgetTitle.replaceAll(" ", "").replaceAll(":", "");
    }

    public ExtendedDashboardPage finished()
    {
        finishButton.click();
        driver.waitUntilElementIsNotVisibleAt(By.className("dialog-title"), dialog);
        return pageBinder.bind(ExtendedDashboardPage.class);
    }
}
