package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.base.Function;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.inject.Inject;

public class ConfigurePortalPage extends AbstractJiraPage {


    @ElementBy(className = "dashboardList")
    private PageElement dashboardList;
    @Inject
    Backdoor backdoor;

    @Override
    public TimedCondition isAt() {
        return dashboardList.timed().isPresent();
    }


    public CreateDashboardPage clickCreateNewDashBoard() {
        backdoor.flags().clearFlags();
        waitForCreatePageButtonToAppear();
        driver.findElement(By.id("create_page")).click();
        CreateDashboardPage bind = pageBinder.bind(CreateDashboardPage.class);
        return bind;
    }

    private void waitForCreatePageButtonToAppear() {
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver input)
            {
                return  !driver.findElements(By.id("create_page")).isEmpty();
            }
        });
    }

    @Override
    public String getUrl() {
        return "secure/ConfigurePortalPages.jspa";
    }
}
