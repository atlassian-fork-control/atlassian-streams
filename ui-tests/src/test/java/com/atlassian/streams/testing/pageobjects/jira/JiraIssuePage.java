package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.inject.Inject;
import java.net.URI;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.Matchers.is;

public class JiraIssuePage implements Page
{
    private final String issueKey;
    
    @Inject
    private AtlassianWebDriver driver;
    
    @ElementBy(id = "vote-data")
    private PageElement votes;
    
    @ElementBy(id = "watcher-data")
    private PageElement watchers;
    
    @ElementBy(id = "watching-toggle")
    private PageElement watcherToggle;
    
    @ElementBy(id = "vote-toggle")
    private PageElement voteToggle;
    
    @ElementBy(className = "watch-state-off")
    private PageElement watchStateOff;

    @ElementBy(className = "watch-state-on")
    private PageElement watchStateOn;

    @ElementBy(className = "vote-state-off")
    private PageElement voteStateOff;

    @ElementBy(className = "vote-state-on")
    private PageElement voteStateOn;
    
    public JiraIssuePage(String issueKey)
    {
        this.issueKey = issueKey;
    }
    
    public JiraIssuePage(URI uri)
    {
        String path = uri.getPath();
        this.issueKey = path.substring(path.lastIndexOf('/') + 1);
    }

    public String getUrl()
    {
        return "browse/" + issueKey;
    }
    
    public String getWatcherCount()
    {
        return watchers.getText();
    }
    
    public JiraIssuePage toggleWatching()
    {
        boolean wasWatching = isWatching();
        watcherToggle.click();
        if (wasWatching)
        {
            waitUntil(watchStateOff.timed().isPresent(), is(true));
        }
        else
        {
            waitUntil(watchStateOn.timed().isPresent(), is(true));
        }
        return this;
    }
    
    public boolean isWatching()
    {
        return watchStateOn.isPresent();
    }

    public String getVoterCount()
    {
        return votes.getText();
    }
    
    public JiraIssuePage toggleVote()
    {
        boolean hadVoted = hasVotedForIssue();
        voteToggle.click();
        if (hadVoted)
        {
            waitUntil(voteStateOff.timed().isPresent(), is(true));
        }
        else
        {
            waitUntil(voteStateOn.timed().isPresent(), is(true));
        }
        return this;
    }

    public boolean hasVotedForIssue()
    {
        return voteStateOn.isPresent();
    }
}
