package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.streams.pageobjects.Gadget;
import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

import static com.atlassian.streams.pageobjects.Predicates.text;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

public class ExtendedDashboardPage extends DashboardPage
{
    @FindBy(id = "dashboard")
    WebElement dashboard;

    @FindBy(css = "a[aria-controls=\"tools-dropdown-items\"]")
    WebElement toolsMenu;

    @FindBy(id = "add-gadget")
    WebElement addGadget;

    @FindBy(id = "home_link")
    WebElement homeLink;
    @Inject
    Backdoor backdoor;

    public DashboardToolsMenu openToolsMenu()
    {
        toolsMenu.click();
        return pageBinder.bind(DashboardToolsMenu.class);
    }

    public CreateDashboardPage navigateToDashBoardCreationPage() {
        backdoor.flags().clearFlags();
        homeLink.click();
        elementFinder.find(By.id("manage_dash_link_lnk")).click();
        return pageBinder.bind(ConfigurePortalPage.class).clickCreateNewDashBoard();
    }

    public AddGadgetDialog openAddGadgetDialog()
    {
        backdoor.flags().clearFlags();
        driver.waitUntil(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver input) {
                return addGadget.isDisplayed() && addGadget.isEnabled();
            }
        });
        addGadget.click();
        return pageBinder.bind(AddGadgetDialog.class);
    }

    public Gadget getGadgetWithTitle(String gadgetTitle)
    {
        backdoor.flags().clearFlags();
        String gadgetId = find(dashboard.findElements(By.className("dashboard-item-title")), text(equalTo(gadgetTitle))).
            getAttribute("id").
            replace("-title", "");
        return pageBinder.bind(Gadget.class, gadgetId);
    }
}
