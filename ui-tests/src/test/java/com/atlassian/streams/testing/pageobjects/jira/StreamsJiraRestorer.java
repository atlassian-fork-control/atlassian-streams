package com.atlassian.streams.testing.pageobjects.jira;

import com.atlassian.integrationtesting.ui.RestoreFromBackupException;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static com.atlassian.integrationtesting.ui.UiTesters.getHome;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

public class StreamsJiraRestorer
{
    private final JiraTestedProduct product;

    public StreamsJiraRestorer(JiraTestedProduct product)
    {
        this.product = product;
    }

    public void restore(String filename)
    {
        product.gotoLoginPage().
            loginAsSysAdmin(WebSudoAuthenticatePage.class).
            loginAsSysAdmin();

        XmlRestorePage restore = product.visit(XmlRestorePage.class).
            setFile(processBackupFile(filename)).
            setQuickImport(true).
            submit();
        if (restore.hasErrors())
        {
            throw new RestoreFromBackupException(restore.getErrors());
        }
    }

    private void processZip(File in, File out) throws IOException {
        try (final ZipFile inFile = new ZipFile(in);
             final FileOutputStream outputStream = new FileOutputStream(out);
             final ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {

            final Enumeration<? extends ZipEntry> entries = inFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry inEntry = entries.nextElement();
                ZipEntry outEntry = new ZipEntry(inEntry.getName());
                zipOutputStream.putNextEntry(outEntry);
                try (InputStream is = inFile.getInputStream(inEntry)) {
                    processXml(is, zipOutputStream);
                }
            }
        }

    }

    private void processXml(InputStream is, OutputStream os) throws IOException {
        String data = IOUtils.toString(is)
                .replaceAll("@jira-home@", getHome())
                .replaceAll("@base-url@", product.getProductInstance().getBaseUrl());
        os.write(data.getBytes(StandardCharsets.UTF_8));
    }

    private File processBackupFile(String filename)
    {
        try {
            File destFile = new File(getHome(), "import/backup" + randomAlphanumeric(5) + ".zip");
            processZip(new File(getClass().getClassLoader().getResource(filename).toURI()), destFile);
            return destFile;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
