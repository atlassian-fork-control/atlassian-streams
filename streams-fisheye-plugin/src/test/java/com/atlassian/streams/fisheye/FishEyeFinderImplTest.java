package com.atlassian.streams.fisheye;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RecentChangesetsService;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;

import com.cenqua.fisheye.config.RepositoryManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FishEyeFinderImplTest
{
    @Mock ChangesetDataFE changeSet1;
    @Mock ChangesetDataFE changeSet2;
    @Mock ChangesetDataFE changeSet3;
    @Mock RecentChangesetsService recentChangesetsService;
    @Mock FishEyeEntryFactory fishEyeEntryFactory;
    @Mock RepositoryManager repositoryManager;
    @Mock FishEyePermissionAccessor permissionAccessor;
    @Mock ActivityRequest requestIS;
    @Mock ActivityRequest requestNOT;
    @Mock ActivityRequest requestEmpty;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> filtersIS;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> filtersNOT;
    @Mock Multimap<String, Pair<Operator, Iterable<String>>> filtersEmpty;

    FishEyeFinderImpl test;

    @Before
    public void setUp()
    {
        test = new FishEyeFinderImpl(recentChangesetsService, fishEyeEntryFactory, repositoryManager, permissionAccessor);
        setUpChangesetData();
        setUpRequest();
    }

    public void setUpChangesetData()
    {
        when(changeSet1.getBranches()).thenReturn(ImmutableSet.of("trunk"));

        when(changeSet2.getBranches()).thenReturn(ImmutableSet.of("branch2"));

        when(changeSet3.getBranches()).thenReturn(ImmutableSet.of("trunk", "branch2"));
    }

    public void setUpRequest()
    {
        when(requestIS.getProviderFilters()).thenReturn(filtersIS);
        when(requestNOT.getProviderFilters()).thenReturn(filtersNOT);
        when(requestEmpty.getProviderFilters()).thenReturn(filtersEmpty);
        when(filtersIS.get(FishEyeFilterOptionProvider.BRANCH))
            .thenReturn(ImmutableSet.of(Pair.pair(Operator.IS, ImmutableList.of("trunk", "branch1"))));
        when(filtersNOT.get(FishEyeFilterOptionProvider.BRANCH))
            .thenReturn(ImmutableSet.of(Pair.pair(Operator.NOT, ImmutableList.of("trunk", "branch1"))));
        when(filtersEmpty.get(FishEyeFilterOptionProvider.BRANCH))
            .thenReturn(ImmutableSet.of());
    }

    @Test
    public void testHasCorrectBranchWithSingleMatchingBranchISFilter()
    {
        assertTrue("changeset has a filtered branch", test.hasCorrectBranch(requestIS).apply(changeSet1));
    }

    @Test
    public void testHasCorrectBranchWithNoMatchingBranchISFilter()
    {
        assertFalse("changeset doesn't have any of the filtered branches",
                    test.hasCorrectBranch(requestIS).apply(changeSet2));
    }

    @Test
    public void testHasCorrectBranchWithMultipleMatchingBranchesISFilter()
    {
        assertTrue("changeset has all filtered branches", test.hasCorrectBranch(requestIS).apply(changeSet3));
    }

    @Test
    public void testHasCorrectBranchWithSingleMatchingBranchNOTFilter()
    {
        assertFalse("changeset has a filtered branch", test.hasCorrectBranch(requestNOT).apply(changeSet1));
    }

    @Test
    public void testHasCorrectBranchWithNoMatchingBranchNOTFilter()
    {
        assertTrue("changeset doesn't have any of the filtered branches",
                   test.hasCorrectBranch(requestNOT).apply(changeSet2));
    }

    @Test
    public void testHasCorrectBranchWithMultipleMatchingBranchesNOTFilter()
    {
        assertFalse("changeset has all filtered branches", test.hasCorrectBranch(requestNOT).apply(changeSet3));
    }

    @Test
    public void testHasCorrectBranchWithNoFilter1()
    {
        assertTrue("changeset has a filtered branch", test.hasCorrectBranch(requestEmpty).apply(changeSet1));
    }

    @Test
    public void testHasCorrectBranchWithNoFilter2()
    {
        assertTrue("changeset doesn't have any of the filtered branches",
                   test.hasCorrectBranch(requestEmpty).apply(changeSet2));
    }

    @Test
    public void testHasCorrectBranchWithNoFilter3()
    {
        assertTrue("changeset has all filtered branches", test.hasCorrectBranch(requestEmpty).apply(changeSet3));
    }

}
