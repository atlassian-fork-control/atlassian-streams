package com.atlassian.streams.fisheye;

import java.net.URI;

import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import com.atlassian.streams.testing.AbstractEntityAssociationProviderTest;

import com.cenqua.fisheye.config.RepositoryManager;
import com.cenqua.fisheye.rep.RepositoryHandle;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.repository;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FishEyeEntityAssociationProviderTest extends AbstractEntityAssociationProviderTest
{
    @Mock
    private RepositoryManager repositoryManager;
    @Mock
    private FishEyePermissionAccessor permissionAccessor;
    @Mock
    private RepositoryHandle repo;
    
    @Override
    public StreamsEntityAssociationProvider createProvider()
    {
        return new FishEyeEntityAssociationProvider(applicationProperties, repositoryManager, permissionAccessor);
    }
    
    @Override
    protected String getProjectUriPath(String key)
    {
        return "/browse/" + key;
    }
    
    @Override
    protected URI getProjectEntityType()
    {
        return repository().iri(); 
    }
    
    @Override
    protected void setProjectExists(String key, boolean exists)
    {
        when(repositoryManager.getRepository(key)).thenReturn(exists ? repo : null);
    }
    
    @Override
    protected void setProjectViewPermission(String key, boolean permitted)
    {
        when(repositoryManager.getRepository(key)).thenReturn(repo);
        when(permissionAccessor.currentUserCanView(repo)).thenReturn(permitted);
    }
    
    @Override
    protected void setProjectEditPermission(String key, boolean permitted)
    {
        setProjectViewPermission(key, permitted);
    }
}
