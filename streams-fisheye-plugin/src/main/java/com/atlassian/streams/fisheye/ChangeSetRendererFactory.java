package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.fisheye.jira.JiraIssueUtil;
import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.data.FileRevisionData;
import com.atlassian.fisheye.spi.data.FileRevisionData.FileRevisionState;
import com.atlassian.fisheye.spi.data.FileRevisionKeyData;
import com.atlassian.fisheye.spi.services.RevisionDataService;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang3.StringEscapeUtils;

import static com.atlassian.fisheye.spi.data.FileRevisionData.FileRevisionState.ADDED;
import static com.atlassian.fisheye.spi.data.FileRevisionData.FileRevisionState.CHANGED;
import static com.atlassian.fisheye.spi.data.FileRevisionData.FileRevisionState.REMOVED;
import static com.atlassian.streams.api.Html.htmlToString;
import static com.atlassian.streams.api.Html.trimHtmlToNone;
import static com.atlassian.streams.api.common.Iterables.drop;
import static com.atlassian.streams.api.common.Iterables.memoize;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;

public class ChangeSetRendererFactory
{
    private static final int MAX_SUMMARY_FILE_LIST = 3;
    private static final int MAX_CONTENT_FILE_LIST = 15;
    private static final int MEMOIZED_REVISIONS_LIMIT = 100; /* should be at least max(MAX_SUMMARY_FILE_LIST, MAX_CONTENT_FILE_LIST) */
    private static final int SUMMARY_BREAKDOWN_CUTOFF = 1000;

    private final RevisionDataService revisionDataService;
    private final TemplateRenderer templateRenderer;
    private final UriProvider uriProvider;
    private final StreamsEntryRendererFactory rendererFactory;
    private final I18nResolver i18nResolver;

    public static class CommitMessageRenderer {
        private  Map<String,String> jiraIssues;

        public CommitMessageRenderer(Map<String, String> jiraIssues) {
            this.jiraIssues = jiraIssues;
        }

        public String render(String message) {
            message = StringEscapeUtils.escapeHtml4(message);
            for (String key : jiraIssues.keySet()) {
                message = message.replaceAll("(?<!-)\\b" + key + "\\b", "<a href='" + jiraIssues.get(key) + "'>" + key + "</a>");
            }
            return message;
        }
    }

    public ChangeSetRendererFactory(RevisionDataService revisionDataService,
            TemplateRenderer templateRenderer,
            StreamsEntryRendererFactory rendererFactory,
            UriProvider uriProvider,
            I18nResolver i18nResolver)
    {
        this.revisionDataService = checkNotNull(revisionDataService, "revisionDataService");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public Renderer newRenderer(ChangesetDataFE changeSet,
            RepositoryHandle repositoryHandle, URI baseUri)
    {
        return new ChangeSetRenderer(changeSet, repositoryHandle, baseUri);
    }

    public final class ChangeSetRenderer implements Renderer
    {
        private final ChangesetDataFE changeSet;
        private final RepositoryHandle repositoryHandle;
        private final URI baseUri;

        private final Function<SummaryItem, Option<Html>> summaryItemRenderer = new SummaryItemRenderer();
        private final Function<Iterable<SummaryItem>, Option<Html>> summaryInfoRenderer = new SummaryInfoRenderer(summaryItemRenderer);

        private final Supplier<Iterable<FileRevisionData>> revisions = Suppliers.memoize(new Supplier<Iterable<FileRevisionData>>()
        {
            public Iterable<FileRevisionData> get()
            {
                return filter(transform(changeSet.getRevisionsIterable(), toRevisionData(repositoryHandle.getName())),
                              nonZeroModifiedLines);
            }
        });
        /**
         * Intended to be a memoized {@code Iterable} with a small number of items so that a large number of revisions isn't stored
         * in memory.  Use {@code revisions} for accessing more than the first {@code MEMOIZED_REVISIONS_LIMIT} items.
         */
        private final Supplier<Iterable<FileRevisionData>> memoizedRevisions = Suppliers.memoize(new Supplier<Iterable<FileRevisionData>>()
        {
            public Iterable<FileRevisionData> get()
            {
                return memoize(revisions.get());
            }
        });

        ChangeSetRenderer(ChangesetDataFE changeSet, RepositoryHandle repositoryHandle,
                URI baseUri)
        {
            this.changeSet = changeSet;
            this.repositoryHandle = repositoryHandle;
            this.baseUri = baseUri;
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            return some(new Html(buildCommittedFilesDescription(baseUri, entry, false)));
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return some(new Html(buildCommittedFilesDescription(baseUri, entry, true)));
        }

        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return rendererFactory.newTitleRenderer("streams.item.fisheye.commit").apply(entry);
        }

        private Function<FileRevisionKeyData, FileRevisionData> toRevisionData(final String repository)
        {
            return new Function<FileRevisionKeyData, FileRevisionData>()
            {
                public FileRevisionData apply(FileRevisionKeyData revision)
                {
                    return revisionDataService.getRevision(repository, revision.getPath(), revision.getRev());
                }
            };
        }

        private final Predicate<FileRevisionData> nonZeroModifiedLines = new Predicate<FileRevisionData>()
        {
            public boolean apply(FileRevisionData revision)
            {
                return revision.getLinesAdded() > 0 || revision.getLinesRemoved() > 0;
            }
        };

        private String buildCommittedFilesDescription(final URI baseUri, StreamsEntry entry, boolean summarize)
        {
            ImmutableMap.Builder<String, Object> context = ImmutableMap.builder();
            final Map<String, String> jiraIssues = getJiraIssues(baseUri, JiraIssueUtil.getJiraKeysFromString(changeSet.getComment()));

            context.put("baseUri", baseUri).
                put("changesetUri", uriProvider.getChangeSetUri(baseUri, changeSet, repositoryHandle).toASCIIString()).
                put("entry", entry).
                put("changeSet", changeSet).
                put("repositoryHandle", repositoryHandle).
                put("renderer", new CommitMessageRenderer(jiraIssues));

            if (summarize)
            {
                addRevisionsToContext(baseUri, context, MAX_SUMMARY_FILE_LIST);
            }
            else
            {
                addRevisionsToContext(baseUri, context, MAX_CONTENT_FILE_LIST);
            }

            return render(templateRenderer, "commit-content.vm", context.build());
        }

        private Map<String,String> getJiraIssues(URI baseUri, List<String> jiraIssueKeys)
        {
            HashMap<String,String> jiraIssues = new HashMap<String,String>();

            for(String jiraIssueKey : jiraIssueKeys)
            {
                if(jiraIssues.containsKey(jiraIssueKey))
                {
                    continue;
                }
                jiraIssues.put(jiraIssueKey, baseUri + "/action/jira-issue.do?key=" + jiraIssueKey);
            }
            return jiraIssues;
        }

        private void addRevisionsToContext(final URI baseUri,
                                           final ImmutableMap.Builder<String, Object> context,
                                           final int numRevsToList)
        {
            Supplier<Iterable<FileRevisionData>> revisionSupplier = revisions;
            // Use the memoized revisions if there are only a small number of revisions.
            if (isEmpty(drop(MEMOIZED_REVISIONS_LIMIT, memoizedRevisions.get())))
            {
                revisionSupplier = memoizedRevisions;
            }
            final Iterable<FileRevisionData> revs = take(numRevsToList, memoizedRevisions.get());

            final ImmutableMap<FileRevisionData, String> revisionUris = getRevisionUriMap(baseUri, revs);
            context.put("uriMap", revisionUris);
            context.put("revisions", revs);
            context.put("summary", summaryInfoRenderer.apply(buildSummaryInfo(drop(numRevsToList, revisionSupplier.get()),
                                                                              changeSet.getNumFileRevisions() - numRevsToList)));
        }

        private ImmutableMap<FileRevisionData, String>
        getRevisionUriMap(final URI baseUri, final Iterable<FileRevisionData> revs)
        {
            final ImmutableMap.Builder<FileRevisionData, String> mapBuilder = ImmutableMap.builder();
            for (final FileRevisionData rev : revs)
            {
                final String s = uriProvider.getChangelogUri(baseUri, rev, repositoryHandle).toASCIIString();
                mapBuilder.put(rev, s);
            }
            return mapBuilder.build();
        }

        private final class SummaryInfoRenderer implements Function<Iterable<SummaryItem>, Option<Html>>
        {
            private final Function<Iterable<SummaryItem>, Option<Html>> compoundRenderer;

            public SummaryInfoRenderer(Function<SummaryItem, Option<Html>> summaryItemRenderer)
            {
                this.compoundRenderer = rendererFactory.newCompoundStatementRenderer(summaryItemRenderer);
            }

            public Option<Html> apply(Iterable<SummaryItem> items)
            {
                return compoundRenderer.apply(items).flatMap(trimHtmlToNone()).map(htmlToString()).map(capitalizeFirstLetter);
            }
        }

        private final class SummaryItemRenderer implements Function<SummaryItem, Option<Html>>
        {
            public Option<Html> apply(SummaryItem item)
            {
                if (item.getFileRevisionCount() > 0)
                {
                    return some(new Html(i18nResolver.getText("streams.item.fisheye.more.files." +
                                item.getState().toString().toLowerCase(),
                            item.getFileRevisionCount())));
                }
                return none();
            }
        }

        private final Function<String, Html> capitalizeFirstLetter = new Function<String, Html>()
        {
            public Html apply(String s)
            {
                return new Html(Character.toUpperCase(s.charAt(0)) + s.substring(1));
            }
        };

        private Iterable<SummaryItem> buildSummaryInfo(Iterable<FileRevisionData> revisions, int remainingRevisions)
        {
            int removed = 0;
            int changed = 0;
            int added = 0;

            if (remainingRevisions > SUMMARY_BREAKDOWN_CUTOFF)
            {
                // STRM-1977: Shortcut the revision state breakdown for large changesets.
                return ImmutableList.of(new SummaryItem(CHANGED, remainingRevisions));
            }

            for (FileRevisionData revision : revisions)
            {
                switch (revision.getState())
                {
                    case ADDED:
                        added++;
                        break;
                    case REMOVED:
                        removed++;
                        break;
                    case CHANGED:
                        changed++;
                        break;
                }
            }
            return ImmutableList.of(new SummaryItem(CHANGED, changed),
                    new SummaryItem(ADDED, added),
                    new SummaryItem(REMOVED, removed));
        }
    }

    public static class SummaryItem
    {
        private final FileRevisionState state;
        private final int fileRevisionCount;

        public SummaryItem(FileRevisionState state, int fileRevisionCount)
        {
            this.state = state;
            this.fileRevisionCount = fileRevisionCount;
        }

        public FileRevisionState getState()
        {
            return state;
        }

        public int getFileRevisionCount()
        {
            return fileRevisionCount;
        }
    }
}
