package com.atlassian.streams.fisheye.external.activity;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.fisheye.activity.ExternalActivityItemProvider;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;
import com.atlassian.streams.fisheye.external.activity.ExternalActivityItemSearchParamsFactory;

import com.cenqua.crucible.model.Principal;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;

public class ActivityStreamsExternalActivityItemProvider implements ExternalActivityItemProvider
{
    private static final Logger log = LoggerFactory.getLogger(ActivityStreamFeedManagerImpl.class);
    private final ApplicationLinkService applinkService;
    private final ActivityStreamFeedManager feedManager;
    private final ExternalActivityItemSearchParamsFactory searchParamsFactory;

    public ActivityStreamsExternalActivityItemProvider(ApplicationLinkService applinkService,
                                                       ActivityStreamFeedManager feedManager, ExternalActivityItemSearchParamsFactory searchParamsFactory)
    {
        this.applinkService = checkNotNull(applinkService, "applinkService");
        this.feedManager = checkNotNull(feedManager, "feedManager");
        this.searchParamsFactory = checkNotNull(searchParamsFactory, "searchParamsFactory");
    }

    public Iterable<ExternalActivityItem> findActivityItems(Principal user, ExternalActivityItemSearchParams params, Map<ApplicationLink, Exception> exceptions)
    {
        Builder<ExternalActivityItem> itemBuilder = ImmutableList.builder();

        Iterable<ApplicationLink> applinks = applinkService.getApplicationLinks(JiraApplicationType.class);
        for (ApplicationLink applink : applinks)
        {
            if (isActivityEnabled(applink))
            {
                try
                {
                    itemBuilder.addAll(feedManager.getItems(applink, params));
                }
                catch (Exception e)
                {
                    if (exceptions != null)
                    {
                        exceptions.put(applink, e);
                    }
                    else
                    {
                        log.warn("Cannot fetch remote feed from applink: " + applink.getName());
                    }
                }
            }
        }

        return itemBuilder.build();
    }

    public boolean hasEarlierActivity(Principal user, final ExternalActivityItem item, ExternalActivityItemSearchParams params)
    {
        return !isEmpty(findActivityItems(user, searchParamsFactory.earlierActivityParams(item), null));
    }

    public boolean hasLaterActivity(Principal user, final ExternalActivityItem item, ExternalActivityItemSearchParams params)
    {
        return !isEmpty(findActivityItems(user, searchParamsFactory.laterActivityParams(item), null));
    }

    private boolean isActivityEnabled(ApplicationLink applink)
    {
        // These are FeCru-internal properties that supposedly will never change.
        // See FE/src/java/com/atlassian/fisheye/jira/JiraServerImpl.java for details.
        Properties appLinkProperties = (Properties) applink.getProperty("SERVER_PROPERTIES");
        return Boolean.parseBoolean(appLinkProperties != null ? appLinkProperties.getProperty("IS_ACTIVITY_ITEM_PROVIDER") : null);
    }
}
