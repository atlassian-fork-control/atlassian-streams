package com.atlassian.streams.fisheye.external.activity;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;

/**
 * Finds {@code ExternalActivityItem}s for {@code ApplicationLink}s with specific
 * {@code ExternalActivityItemSearchParams}.
 */
public interface ActivityStreamFeedManager
{
    Iterable<ExternalActivityItem> getItems(ApplicationLink appLink, ExternalActivityItemSearchParams params) throws Exception;
}
