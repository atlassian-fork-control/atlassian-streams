package com.atlassian.streams.fisheye.external.activity;

import com.atlassian.fisheye.activity.ExternalActivityItem;

/**
 * Generates {@code ExternalActivityItem}s based off of a textual version
 * of a feed's contents.
 */
public interface ExternalActivityItemFactory
{
    Iterable<ExternalActivityItem> getItems(String feedContents);
}
