package com.atlassian.streams.fisheye;

import java.net.URI;
import java.util.Date;
import java.util.Set;

import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RecentChangesetsService;
import com.atlassian.fisheye.spi.services.RecentChangesetsService.QueryParameters;
import com.atlassian.fisheye.spi.services.RecentChangesetsService.QueryParameters.Builder;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.CancelledException;

import com.cenqua.fisheye.config.RepositoryManager;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets.SetView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Iterables.memoize;
import static com.atlassian.streams.api.common.Iterables.mergeSorted;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.api.common.Predicates.containsAnyIssueKey;
import static com.atlassian.streams.spi.Filters.getAllValues;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getIssueKeys;
import static com.atlassian.streams.spi.Filters.getNotIssueKeys;
import static com.atlassian.streams.spi.Filters.inDateRange;
import static com.atlassian.streams.spi.Filters.inProjectKeys;
import static com.atlassian.streams.spi.Filters.inUsers;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.all;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.intersection;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class FishEyeFinderImpl implements FishEyeFinder
{
    private static final Logger log = LoggerFactory.getLogger(FishEyeFinderImpl.class);

    private final RecentChangesetsService recentChangesetsService;
    private final RepositoryManager repositoryManager;
    private final FishEyeEntryFactory fishEyeEntryFactory;
    private final FishEyePermissionAccessor permissionAccessor;

    public FishEyeFinderImpl(RecentChangesetsService recentChangesetsService,
            FishEyeEntryFactory fishEyeEntryFactory,
            RepositoryManager repositoryManager,
            FishEyePermissionAccessor permissionAccessor)
    {
        this.recentChangesetsService = checkNotNull(recentChangesetsService, "recentChangesetsService");
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
        this.fishEyeEntryFactory = checkNotNull(fishEyeEntryFactory, "fishEyeEntryFactory");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
    }

    public Iterable<RepositoryHandle> getRepositories(ActivityRequest request)
    {
        Iterable<String> projectKeys = getIsValues(request.getStandardFilters().get(PROJECT_KEY));

        Iterable<RepositoryHandle> repoHandles;
        if (!isEmpty(projectKeys))
        {
            repoHandles = filter(transform(projectKeys, toRepositoryHandles()), notNull());
        }
        else
        {
            repoHandles = repositoryManager.getHandles();
        }

        // Filter out any repository handles that match the NOT project_key filter option
        return filter(repoHandles, repoName(inProjectKeys(request)));
    }

    public Iterable<StreamsEntry> getChangesets(final URI baseUri,
                                                final RepositoryHandle repository,
                                                final ActivityRequest request,
                                                Supplier<Boolean> cancelled)
    {
        if (!permissionAccessor.currentUserCanView(repository) || !repository.isRunning())
        {
            return ImmutableList.of();
        }

        Iterable<ChangesetDataFE> changesets;
        Set<String> users = getIsValues(request.getStandardFilters().get(USER.getKey()));
        if (!isEmpty(users))
        {
            changesets = mergeSorted(transform(users, toChangesetsPerUser(request, repository.getName(), cancelled)), byCommitDate);
        }
        else
        {
            changesets = listChangesets(repository.getName(), getQueryParametersBuilder(request).build());
        }

        // Filter out any changesets that match the NOT filter_user filter option
        if (!isEmpty(getAllValues(NOT, request.getStandardFilters().get(USER.getKey()))))
        {
            changesets = filter(changesets, committedBy(inUsers(request)));
        }

        // Filter out any changesets that don't match date range, issue key, or branch filters
        Predicate<ChangesetDataFE> filters = and(ImmutableList.of(
            commitDate(inDateRange(request)),
            isEmpty(getIssueKeys(request)) ? Predicates.<ChangesetDataFE>alwaysTrue() : commitComment(containsAnyIssueKey(getIssueKeys(request))),
            isEmpty(getNotIssueKeys(request)) ? Predicates.<ChangesetDataFE>alwaysTrue() : commitComment(not(containsAnyIssueKey(getNotIssueKeys(request)))),
            hasCorrectBranch(request)));
        return memoize(take(request.getMaxResults(), catOptions(transform(filter(changesets, filters),
                toStreamsEntry(baseUri, repository)))));
    }

    private static final Ordering<ChangesetDataFE> byCommitDate = (new Ordering<ChangesetDataFE>()
    {
        public int compare(ChangesetDataFE lhs, ChangesetDataFE rhs)
        {
            int d = lhs.getDate().compareTo(rhs.getDate());
            if (d != 0)
            {
                return d;
            }
            return lhs.getCsid().compareTo(rhs.getCsid());
        }
    }).reverse();

    private Function<ChangesetDataFE, Option<StreamsEntry>> toStreamsEntry(final URI baseUri,
                                                                           final RepositoryHandle repository)
    {
        return new Function<ChangesetDataFE, Option<StreamsEntry>>()
        {
            public Option<StreamsEntry> apply(ChangesetDataFE changeset)
            {
                try
                {
                    return option(fishEyeEntryFactory.getEntry(baseUri, changeset, repository));
                }
                catch (Exception e)
                {
                    log.warn("Error creating streams entry", e);
                    return none(StreamsEntry.class);
                }
            }
        };
    }

    private Function<String, RepositoryHandle> toRepositoryHandles()
    {
        return new ToRepositoryHandles();
    }

    private final class ToRepositoryHandles implements Function<String, RepositoryHandle>
    {
        public RepositoryHandle apply(String key)
        {
            return repositoryManager.getRepository(key);
        }
    }

    private Function<String, Iterable<ChangesetDataFE>> toChangesetsPerUser(final ActivityRequest request,
            final String repositoryName,
            final Supplier<Boolean> cancelled)
    {
        return new Function<String, Iterable<ChangesetDataFE>>()
        {
            public Iterable<ChangesetDataFE> apply(String username)
            {
                if (isNotEmpty(username))
                {
                    if (cancelled.get())
                    {
                        throw new CancelledException();
                    }
                    final Builder queryBuilder = getQueryParametersBuilder(request).committer(username);
                    return listChangesets(repositoryName, queryBuilder.build());
                }
                return ImmutableList.of();
            }
        };
    }

    private Iterable<ChangesetDataFE> listChangesets(String repositoryName, QueryParameters parameters)
    {
        // This will handle STRM-1593, so that the fisheye feed will not fail when trying to anonymously retrieve
        // a changeset from a particular repository.
        try
        {
            return recentChangesetsService.listChangesets(repositoryName, parameters);
        }
        catch (Exception e)
        {
            log.error("Error retrieving changesets", e);
            return ImmutableList.of();
        }

    }

    private Builder getQueryParametersBuilder(ActivityRequest request)
    {
        Iterable<String> issueKeys = getIssueKeys(request);
        final Builder queryBuilder = recentChangesetsService.getQueryBuilder();
        if (!isEmpty(issueKeys))
        {
            queryBuilder.comment(Joiner.on(" ").join(issueKeys));
        }
        return queryBuilder;
    }

    private Predicate<RepositoryHandle> repoName(Predicate<String> p)
    {
        return new RepoName(p);
    }

    private static class RepoName implements Predicate<RepositoryHandle>
    {
        private final Predicate<String> p;

        public RepoName(Predicate<String> p)
        {
            this.p = p;
        }

        public boolean apply(RepositoryHandle repoHandle)
        {
            return p.apply(repoHandle.getName());
        }

        @Override
        public String toString()
        {
            return String.format("repoName(%s)", p);
        }
    }

    private Predicate<ChangesetDataFE> committedBy(Predicate<String> p)
    {
        return new CommittedBy(p);
    }

    private static class CommittedBy implements Predicate<ChangesetDataFE>
    {
        private final Predicate<String> p;

        public CommittedBy(Predicate<String> p)
        {
            this.p = p;
        }

        public boolean apply(ChangesetDataFE changeset)
        {
            return p.apply(changeset.getAuthor());
        }

        @Override
        public String toString()
        {
            return String.format("committedBy(%s)", p);
        }
    }

    private Predicate<ChangesetDataFE> commitDate(final Predicate<Date> p)
    {
        return new CommitDate(p);
    }

    private static class CommitDate implements Predicate<ChangesetDataFE>
    {
        private final Predicate<Date> p;

        public CommitDate(Predicate<Date> p)
        {
            this.p = p;
        }

        public boolean apply(ChangesetDataFE changeset)
        {
            return p.apply(changeset.getDate());
        }

        @Override
        public String toString()
        {
            return String.format("commitDate(%s)", p);
        }
    }

    private Predicate<ChangesetDataFE> commitComment(final Predicate<String> p)
    {
        return new CommitComment(p);
    }

    private final class CommitComment implements Predicate<ChangesetDataFE>
    {
        private final Predicate<String> p;

        private CommitComment(Predicate<String> p)
        {
            this.p = p;
        }

        public boolean apply(ChangesetDataFE changeset)
        {
            return p.apply(changeset.getComment());
        }

        @Override
        public String toString()
        {
            return String.format("commitComment(%s)", p);
        }
    }

    protected Predicate<ChangesetDataFE> hasCorrectBranch(ActivityRequest request)
    {
        return new HasCorrectBranch(request);
    }

    private class HasCorrectBranch implements Predicate<ChangesetDataFE>
    {
        private final ActivityRequest request;

        public HasCorrectBranch(ActivityRequest request)
        {
            this.request = request;
        }

        public boolean apply(final ChangesetDataFE changeset)
        {
            if (changeset == null)
            {
                return false;
            }
            return all(request.getProviderFilters().get(FishEyeFilterOptionProvider.BRANCH), new Predicate<Pair<Operator, Iterable<String>>>()
            {
                @Override
                public boolean apply(Pair<Operator, Iterable<String>> filter)
                {
                    SetView<String> intersection =
                            intersection(changeset.getBranches(), ImmutableSet.copyOf(filter.second()));
                     if (Operator.IS.equals(filter.first()))
                     { // we are looking for an intersection
                         return !intersection.isEmpty();
                     }
                     if (Operator.NOT.equals(filter.first()))
                     { // we are looking for NO intersection
                         return intersection.isEmpty();
                     }
                     // the only operators we should really see are IS and NOT, but this is a
                     // fail-safe
                     return false;
                 }
             });
        }
    };
}
