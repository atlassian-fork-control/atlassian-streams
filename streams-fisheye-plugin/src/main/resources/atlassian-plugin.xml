<atlassian-plugin key="com.atlassian.streams.fisheye" name="FishEye Streams Plugin" pluginsVersion="2">
    <plugin-info>
        <description>FishEye Streams Plugin</description>
        <version>${project.version}</version>
        <vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com/"/>
    </plugin-info>

    <!-- Fisheye components -->
    <component-import key="recentChangesetsService" interface="com.atlassian.fisheye.spi.services.RecentChangesetsService" />
    <component-import key="revisionDataService" interface="com.atlassian.fisheye.spi.services.RevisionDataService" />
    <component-import key="txTemplate" interface="com.atlassian.fisheye.spi.TxTemplate" />
    <component-import key="cenquaUserManager" interface="com.cenqua.fisheye.user.UserManager" />
    <component-import key="repositoryManager" interface="com.cenqua.fisheye.config.RepositoryManager" />
    <component-import key="projectService" interface="com.atlassian.crucible.spi.services.ProjectService" />
    <component-import key="userService" interface="com.atlassian.crucible.spi.services.UserService" />
    <component-import key="repositoryService" interface="com.atlassian.fisheye.spi.services.RepositoryService" />

    <!-- SAL components -->
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="salUserManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />

    <!-- Streams components -->
    <component-import key="streamsI18nResolver" interface="com.atlassian.streams.spi.StreamsI18nResolver" />    
    <component-import key="streamsEntryRendererFactory" interface="com.atlassian.streams.api.renderer.StreamsEntryRendererFactory" />
    <component-import key="streamsFeedUriBuilderFactory" interface="com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory" />
    
    <component-import key="applinksService" interface="com.atlassian.applinks.api.ApplicationLinkService" />
    <component-import key="entityLinkService" interface="com.atlassian.applinks.api.EntityLinkService" />

    <component-import key="templateRendererFactory" interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRendererFactory" />
    <component key="templateRenderer" class="com.atlassian.streams.spi.renderer.CachingTemplateRenderer" /> 
    
    <activity-streams-provider key="source" name="FishEye" i18n-name-key="streams.fisheye.provider.name"
            class="com.atlassian.streams.fisheye.FishEyeStreamsActivityProvider">
        <filter-provider class="com.atlassian.streams.fisheye.FishEyeFilterOptionProvider" />
        <validator class="com.atlassian.streams.fisheye.FishEyeStreamsValidator" />
        <entity-association-provider class="com.atlassian.streams.fisheye.FishEyeEntityAssociationProvider" />
        <key-provider class="com.atlassian.streams.fisheye.FishEyeKeyProvider" />
    </activity-streams-provider>
    
    <component key="fisheyeSessionManager" class="com.atlassian.streams.fisheye.FishEyeSessionManager" public="true">
        <interface>com.atlassian.streams.spi.SessionManager</interface>
    </component>

    <component key="userProfileAccessor" name="Fisheye User Profile Accessor"
            class="com.atlassian.streams.fisheye.FishEyeUserProfileAccessor" public="true">
        <interface>com.atlassian.streams.spi.UserProfileAccessor</interface>
    </component>
    
    <component key="uriAuthParamProvider" public="true"
            class="com.atlassian.streams.fisheye.FisheyeUriAuthenticationParameterProvider">
        <interface>com.atlassian.streams.spi.UriAuthenticationParameterProvider</interface>
    </component>
    
    <component key="streamsLocaleProvider" name="Streams Locale Provider"
               class="com.atlassian.streams.spi.DefaultStreamsLocaleProvider" public="true">
        <interface>com.atlassian.streams.spi.StreamsLocaleProvider</interface>
    </component>

    <component key="formatPreferenceProvider" name="Format Preference Provider"
               class="com.atlassian.streams.fisheye.FishEyeFormatPreferenceProvider" public="true">
        <interface>com.atlassian.streams.spi.FormatPreferenceProvider</interface>
    </component>
    
    <component key="repositoryEntityResolver" class="com.atlassian.streams.fisheye.RepositoryEntityResolver" public="true">
        <interface>com.atlassian.streams.spi.EntityResolver</interface>
    </component>
    
    <component key="fishEyeEntryFactory" name="FishEye Entry Factory"
            class="com.atlassian.streams.fisheye.FishEyeEntryFactoryImpl" />
    <component key="fishEyeFinder" name="FishEye Finder" class="com.atlassian.streams.fisheye.FishEyeFinderImpl" />
    <component key="projectKeys" class="com.atlassian.streams.fisheye.ProjectKeys" />
    <component key="uriProvider" class="com.atlassian.streams.fisheye.UriProvider" />
    <component key="changeSetRendererFactory" class="com.atlassian.streams.fisheye.ChangeSetRendererFactory" />
    <component key="permissionAccessor" class="com.atlassian.streams.fisheye.FishEyePermissionAccessorImpl"/>
    
    <component key="itemFactory" name="External Activity Item Factory" class="com.atlassian.streams.fisheye.external.activity.RomeExternalActivityItemFactory" public="true">
        <interface>com.atlassian.streams.fisheye.external.activity.ExternalActivityItemFactory</interface>
    </component>
    <component key="remoteStreamsFeedUriBuilder" name="Remote Streams Uri Builder" class="com.atlassian.streams.fisheye.external.activity.RemoteStreamsFeedUriBuilderImpl" public="true">
        <interface>com.atlassian.streams.fisheye.external.activity.RemoteStreamsFeedUriBuilder</interface>
    </component>
    <component key="externalSearchParamsFactory" name="External Search Params Factory" class="com.atlassian.streams.fisheye.external.activity.ExternalActivityItemSearchParamsFactoryImpl" public="true">
        <interface>com.atlassian.streams.fisheye.external.activity.ExternalActivityItemSearchParamsFactory</interface>
    </component>
    <component key="activityStreamFeedManager" name="Streams Feed Manager" class="com.atlassian.streams.fisheye.external.activity.ActivityStreamFeedManagerImpl"/>

    <activity-item-provider key="streams-activity-item-provider" class="com.atlassian.streams.fisheye.external.activity.ActivityStreamsExternalActivityItemProvider"/>

    <servlet-filter key="requestFilter" location="after-encoding" weight="1"
            class="com.atlassian.streams.fisheye.RequestFilter">
        <url-pattern>/activity</url-pattern>
        <url-pattern>/plugins/servlet/streams</url-pattern>
    </servlet-filter>

    <resource type="i18n" name="fisheye-i18n" location="com.atlassian.streams.fisheye.i18n"/>
    <resource type="download" name="images/" location="images/"/>

</atlassian-plugin>
