package com.atlassian.streams.internal.atom.abdera;

import java.io.InputStream;
import java.io.InputStreamReader;

import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedModel;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_DATE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_DATE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_SUBTITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_URI;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class AbderaAtomFeedParserTest
{
    AbderaAtomFeedParser parser;

    @Before
    public void createParser()
    {
        parser = new AbderaAtomFeedParser();
    }

    @Test
    public void parsedFeedContainsBasicProperties() throws Exception
    {
        FeedModel feed = parseFeed("atom-feed-parsing.xml");
        
        assertThat(feed.getUri(), equalTo(FEED_URI));
        assertThat(feed.getTitle(), equalTo(some(FEED_TITLE)));
        assertThat(feed.getSubtitle(), equalTo(some(FEED_SUBTITLE)));
        assertThat(feed.getUpdated().get().toDateTime(DateTimeZone.UTC), equalTo(FEED_DATE));
    }
    
    @Test
    public void parsedFeedContainsEncodedAtomData() throws Exception
    {
        FeedModel feed = parseFeed("atom-feed-parsing.xml");
        assertThat(feed.getEncodedContent().isDefined(), equalTo(true));
    }

    @Test
    public void parsedFeedContainsEntryWithPublishedDate() throws Exception
    {
        FeedModel feed = parseFeed("atom-feed-parsing.xml");
        
        assertThat(size(feed.getEntries()), equalTo(1));
        FeedEntry entry = get(feed.getEntries(), 0);
        assertThat(entry.getEntryDate().toDateTime(DateTimeZone.UTC), equalTo(ENTRY_DATE));
    }
    
    private FeedModel parseFeed(String filename) throws Exception
    {
        InputStream input = getClass().getClassLoader().getResourceAsStream(filename);
        return parser.readFeed(new InputStreamReader(input));
    }
}
