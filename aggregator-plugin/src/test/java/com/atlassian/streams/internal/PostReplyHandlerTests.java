package com.atlassian.streams.internal;


import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.sal.api.xsrf.XsrfHeaderValidator.TOKEN_HEADER;
import static com.atlassian.streams.internal.PostReplyHandler.NO_CHECK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;

@RunWith (MockitoJUnitRunner.class)
public class PostReplyHandlerTests
{
    private PostReplyHandler postReplyHandler;

    @Mock
    private LocalActivityProviders localProviders;
    @Mock
    private AppLinksActivityProviders applinksProviders;
    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private Request<?, Response> request;

    @Before
    public void setup()
    {
        ActivityProviders activitiesProviders = new ActivityProviders(localProviders, applinksProviders);
        postReplyHandler = new PostReplyHandler(activitiesProviders, applicationProperties);
    }

    @Test
    public void assertExecuteRequestIncludeNoTokenCheckHeader() throws ResponseException
    {
        postReplyHandler.executeRequest(request, "a comment");

        // Verify that the header is set before sending the request.
        InOrder inOrder = inOrder(request);
        inOrder.verify(request).setHeader(TOKEN_HEADER, NO_CHECK);
        inOrder.verify(request).executeAndReturn(any(ReturningResponseHandler.class));
    }
}
