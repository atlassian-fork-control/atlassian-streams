package com.atlassian.streams.internal;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.lang3.ObjectUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit Test for {@link HttpParameters}.
 *
 * @since v5.3.12
 */
@RunWith(MockitoJUnitRunner.class)
public class HttpParametersTest
{
    private ImmutableMultimap.Builder<String, String> bld;
    private HttpParameters toTest;

    @Before
    public void setUp()
    {
        bld = new ImmutableListMultimap.Builder<>();
    }

    public void construct()
    {
        toTest = HttpParameters.parameters(bld.build());
    }

    @Test
    public void testUseAcceptLanguageWithEmptyMap()
    {
        // Empty map
        construct();

        assertFalse(toTest.useAcceptLanguage());
    }

    @Test
    public void testUseAcceptLanguageWithFalseValue()
    {
        bld.put(ActivityRequest.USE_ACCEPT_LANG_KEY, "false");
        construct();
        assertFalse(toTest.useAcceptLanguage());
    }

    @Test
    public void testUseAcceptLanguageWithTrueValue()
    {
        bld.put(ActivityRequest.USE_ACCEPT_LANG_KEY, "true");
        construct();
        assertTrue(toTest.useAcceptLanguage());
    }


    @Test
    public void testFetchLocalOnlyWithEmptyMap()
    {
        construct();

        Predicate<ActivityProvider> pred = toTest.fetchLocalOnly();
        final ActivityProvider provider = mock(ActivityProvider.class);
        assertTrue(pred.apply(provider));
    }

    @Test
    public void testFetchLocalOnlyWithFalseValue()
    {
        bld.put(HttpParameters.LOCAL_KEY, "false");
        construct();

        Predicate<ActivityProvider> pred = toTest.fetchLocalOnly();
        final ActivityProvider provider = mock(ActivityProvider.class);
        assertTrue(pred.apply(provider));
    }

    @Test
    public void testFetchLocalOnlyWithTrueValue()
    {
        bld.put(HttpParameters.LOCAL_KEY, "true");
        construct();

        Predicate<ActivityProvider> pred = toTest.fetchLocalOnly();
        final ActivityProvider provider = mock(ActivityProvider.class);
        assertFalse(pred.apply(provider));
    }

    @Test
    public void testGetTitleNoParams()
    {
        construct();

        assertFalse(toTest.getTitle().isDefined());
    }

    @Test
    public void testGetTitleWithParam()
    {
        bld.put(HttpParameters.PARAM_TITLE, "test");
        construct();

        assertThat(toTest.getTitle().get(), equalTo("test"));
    }

    @Test
    public void testIsSelectedProviderOmittingProvidersParamReturnsAllProviders()
    {
        construct();
        assertThat(com.google.common.base.Predicates.alwaysTrue(),
                equalTo(toTest.isSelectedProvider()));
    }

    @Test
    public void assertThatProvidersInParamAreReturned()
    {
        final Multimap<String, String> parameters = ImmutableMultimap.of(ActivityRequest.PROVIDERS_KEY,
                "wiki applinks:Bamboo");
        final Iterable<String> expectedParams = ImmutableList.of("wiki", "applinks:Bamboo");
        assertThat(HttpParameters.getSelectedProviders(parameters), is(equalTo(expectedParams)));
    }

    @Test
    public void testIsSelectedProviderWithParams()
    {
        bld.put(ActivityRequest.PROVIDERS_KEY, "happy gilmore france");
        construct();
        final Predicate<ActivityProvider> pCheck = toTest.isSelectedProvider();

        ActivityProvider provExists = mock(ActivityProvider.class);
        ActivityProvider provExists2 = mock(ActivityProvider.class);
        ActivityProvider provNotExists = mock(ActivityProvider.class);

        when(provExists.matches("happy")).thenReturn(true);
        when(provExists2.matches("gilmore")).thenReturn(true);

        assertTrue(pCheck.apply(provExists));
        assertTrue(pCheck.apply(provExists2));
        assertFalse(pCheck.apply(provNotExists));
    }

    @Test
    public void testModuleNoParam()
    {
        construct();

        assertThat(toTest.module(), equalTo(com.google.common.base.Predicates.alwaysTrue()));
    }

    @Test
    public void testModuleWithParam()
    {
        bld.put(HttpParameters.PARAM_MODULE, "bob");

        construct();
        final Predicate<ActivityProvider> pCheck = toTest.module();

        ActivityProvider provExists = mock(ActivityProvider.class);
        ActivityProvider provNotExists = mock(ActivityProvider.class);
        ActivityProvider provNotExists2 = mock(ActivityProvider.class);

        when(provExists.matches("bob")).thenReturn(true);

        assertTrue(pCheck.apply(provExists));
        assertFalse(pCheck.apply(provNotExists));
        assertFalse(pCheck.apply(provNotExists2));
    }

    @Test
    public void testIsTimeoutTestFalse()
    {
        construct();
        assertFalse(toTest.isTimeoutTest());
    }

    @Test
    public void testIsTimeoutTestExplicitFalse()
    {
        bld.put(HttpParameters.TIMEOUT_TEST, "false");
        construct();
        assertFalse(toTest.isTimeoutTest());
    }

    @Test
    public void testIsTimeoutTestTrue()
    {
        bld.put(HttpParameters.TIMEOUT_TEST, "true");
        construct();
        assertTrue(toTest.isTimeoutTest());
    }

    @Test
    public void testParseMaxResultsDefault()
    {
        construct();
        assertThat(toTest.parseMaxResults(24), equalTo(24));
    }

    @Test
    public void testParseMaxResultsInvalidValue()
    {
        bld.put(HttpParameters.MAX_RESULTS, "invalid");
        construct();
        assertThat(toTest.parseMaxResults(24), equalTo(24));
    }

    @Test
    public void testParseMaxResultsWithParam()
    {
        bld.put(HttpParameters.MAX_RESULTS, "43");
        construct();
        assertThat(toTest.parseMaxResults(24), equalTo(43));
    }

    @Test
    public void testParseMaxResultsExceedingBoundary()
    {
        bld.put(HttpParameters.MAX_RESULTS, "1100");
        construct();
        assertThat(toTest.parseMaxResults(Integer.MAX_VALUE), equalTo(1000));
    }

    @Test
    public void testGetProviderKeyNoParams()
    {
        construct();
        ActivityProvider provider = mock(ActivityProvider.class);

        // There's no params, so even though the provider always matches, there's nothing to match.
        assertFalse(toTest.getProviderKey(provider).isDefined());

        verifyZeroInteractions(provider);
    }

    @Test
    public void testGetProviderKeyNonMatchingProvider()
    {
        bld.put("zinc", "I don't know");
        construct();
        ActivityProvider provider = mock(ActivityProvider.class);
        when(provider.matches("zinc")).thenReturn(false);

        assertFalse(toTest.getProviderKey(provider).isDefined());
    }

    @Test
    public void testGetProviderKeyMatchingProvider()
    {
        bld.put("zinc", "I don't know");
        construct();
        ActivityProvider provider = mock(ActivityProvider.class);
        when(provider.matches("zinc")).thenReturn(true);

        assertThat(toTest.getProviderKey(provider), equalTo(Option.option("zinc")));
    }

    @Test
    public void testBuildParams()
    {
        HttpServletRequest req = mock(HttpServletRequest.class);
        final Map<String, String[]> items = new HashMap<>();
        items.put("Zero", new String[] {});
        items.put("ZeroTwo", new String[] {""});
        items.put("One", new String[] {"Item1"});
        items.put("Two", new String[] {"Item1", "Item2"});

        when(req.getParameterMap()).thenReturn(items);
        ImmutableMultimap<String, String> toTest = HttpParameters.buildParams(req);
        assertThat(toTest.entries(), containsInAnyOrder(
                isEntry("ZeroTwo", ""), isEntry("One", "Item1"), isEntry("Two", "Item1"), isEntry("Two", "Item2")
        ));
    }

    @Test
    public void testParseStandardFiltersNoParams()
    {
        construct();

        assertTrue(toTest.parseStandardFilters().isEmpty());
    }

    @Test
    public void testParseStandardFiltersStandardFilters()
    {
        bld.putAll(STANDARD_FILTERS_PROVIDER_KEY, "bob IS happening", "bob CONTAINS raisins berries", "jane NOT hip");
        construct();

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> filters = toTest.parseStandardFilters();

        assertThat(filters.get("bob"), containsInAnyOrder(
                matchPair(equalTo(StreamsFilterType.Operator.IS), contains("happening")),
                matchPair(equalTo(StreamsFilterType.Operator.CONTAINS), contains("raisins", "berries"))));
        assertThat(filters.get("jane"), contains(matchPair(equalTo(StreamsFilterType.Operator.NOT), contains("hip"))));
    }

    @Test
    public void testParseStandardFiltersLegacyFilters()
    {
        bld.putAll(HttpParameters.LEGACY_AUTHOR, "frankenbok");
        bld.putAll(HttpParameters.LEGACY_FILTER, "adhd");
        construct();

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> filters = toTest.parseStandardFilters();

        assertThat(filters.get("issue-key"), contains(matchPair(equalTo(StreamsFilterType.Operator.IS), contains("adhd"))));
        assertThat(filters.get("user"), contains(matchPair(equalTo(StreamsFilterType.Operator.IS), contains("frankenbok"))));
    }

    @Test
    public void testCalculateContextUrlDefault()
    {
        construct();

        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl()).thenReturn("http://www.example.com");

        final URI ans = toTest.calculateContextUrl(applicationProperties, "/test");

        assertThat(ans, equalTo(URI.create("http://www.example.com")));
    }

    @Test
    public void testCalculateContextNotRelative()
    {
        bld.put(HttpParameters.RELATIVE_LINKS_KEY, "false");
        construct();

        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl()).thenReturn("http://www.example.com");

        final URI ans = toTest.calculateContextUrl(applicationProperties, "/test");

        assertThat(ans, equalTo(URI.create("http://www.example.com")));
    }

    @Test
    public void testCalculateContextRelativeUrl()
    {
        bld.put(HttpParameters.RELATIVE_LINKS_KEY, "true");
        construct();

        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        final URI ans = toTest.calculateContextUrl(applicationProperties, "/test");
        assertThat(ans, equalTo(URI.create("/test")));

        verifyZeroInteractions(applicationProperties);
    }

    @Test
    public void testGetProviderFilterNoParams()
    {
        construct();
        final ActivityProvider provider = mock(ActivityProvider.class);

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> filters =
                Multimaps.forMap(Maps.newHashMap());

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> ans =
                toTest.getProviderFilter(filters, provider);

        // Same as original, since there's no params.
        assertThat(ans, sameInstance(filters));
    }


    @Test
    public void testGetProviderFilterNotInParams()
    {
        bld.put("zinc", "lupin IS phat");
        construct();
        final ActivityProvider provider = mock(ActivityProvider.class);
        when(provider.matches("zinc")).thenReturn(false);

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> filters =
                Multimaps.forMap(Maps.newHashMap());

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> ans =
                toTest.getProviderFilter(filters, provider);

        // Params don't match, so we're still the same.
        assertThat(ans, sameInstance(filters));
    }

    @Test
    public void testGetProviderFilterInParams()
    {
        bld.put("zinc", "lupin IS phat");
        construct();
        final ActivityProvider provider = mock(ActivityProvider.class);
        when(provider.matches("zinc")).thenReturn(true);

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> filters =
                Multimaps.forMap(Maps.newHashMap());

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> ans =
                toTest.getProviderFilter(filters, provider);

        Collection<Pair<StreamsFilterType.Operator, Iterable<String>>> zinc = ans.get("lupin");
        // Params match!
        assertThat(zinc, contains(matchPair(equalTo(StreamsFilterType.Operator.IS), contains("phat"))));
    }

    @Test
    public void testGetProvidersEmpty()
    {
        construct();
        assertTrue(toTest.getProviders().isEmpty());
    }

    @Test
    public void testGetProvidersOneItem()
    {
        bld.put(ActivityRequest.PROVIDERS_KEY, "yaya");
        construct();
        assertThat(toTest.getProviders(), contains("yaya"));
    }

    private Matcher<Map.Entry<String, String>> isEntry(final String key, final String value)
    {
        return new BaseMatcher<Map.Entry<String, String>>()
        {
            @Override
            public boolean matches(final Object o)
            {
                if (!(o instanceof Map.Entry))
                {
                    return false;
                }
                Map.Entry<String, String> e = (Map.Entry<String, String>) o;
                return ObjectUtils.equals(key, e.getKey()) && ObjectUtils.equals(value, e.getValue());
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("equals('" + key + "'='" + value + "')");
            }
        };
    }

    private static <A, B> Matcher<Pair<A, B>> matchPair(final Matcher<? super A> aMatch,
            final Matcher<? super B> bMatch) {
        return new BaseMatcher<Pair<A, B>>()
        {
            @Override
            public boolean matches(final Object o)
            {
                if (!(o instanceof Pair))
                {
                    return false;
                }
                Pair<A, B> item = (Pair<A, B>) o;

                return aMatch.matches(item.first()) && bMatch.matches(item.second());
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("(");
                aMatch.describeTo(description);
                description.appendText(", ");
                bMatch.describeTo(description);
                description.appendText(")");
            }
        };
    }
}
