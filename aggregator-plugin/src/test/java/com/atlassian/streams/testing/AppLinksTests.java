package com.atlassian.streams.testing;

import com.atlassian.streams.internal.ProviderFilterOrdering;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.fisheye.FishEyeActivityObjectTypes.changeset;
import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.provider;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.FishEye.Data.changeSet2;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.Data.createdTwo1;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasProviderFilter;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.withProviderKey;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.haveAtlassianApplicationElement;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertTrue;

public abstract class AppLinksTests
{
    protected static final String ADMIN_USER = "admin";
    protected static final String USER_USER = "user";

    /** This needs to be high enough so all the content from each application will be in the stream **/
    protected static final int MAX_RESULTS = 500;

    @Inject static protected FeedClient client;
    @Inject static protected RestTester restTester;

    protected static String jira;
    protected static String fisheye;
    protected static String crucible;

    @BeforeClass
    public static void getProviderKeys()
    {
        StreamsConfigRepresentation config = restTester.getStreamsConfigRepresentation();
        jira = getProviderKeyStartingWith(config, "issues");
        fisheye = getProviderKeyStartingWith(config, "source");
        crucible = getProviderKeyStartingWith(config, "reviews");
    }

    private static String getProviderKeyStartingWith(StreamsConfigRepresentation config, final String keyPrefix)
    {
        return getOnlyElement(filter(config.getFilters(), new Predicate<ProviderFilterRepresentation>()
        {
            public boolean apply(ProviderFilterRepresentation filter)
            {
                return filter.getKey().startsWith(keyPrefix);
            }
        })).getKey();
    }

    /* Basic tests showing that content from each app shows up in each apps feed */

    @Test
    public void assertThatFisheyeActivityIsInFeed()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(fisheye));

        assertThat(feed.getEntries(), hasEntry(projectOneChangeSet2()));
    }

    @Test
    public void assertThatCrucibleActivityIsInFeed()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(crucible));

        assertThat(feed.getEntries(), hasEntry(createdReviewOne1()));
    }

    @Test
    public void assertThatJiraActivityIsInFeed()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(jira));

        assertThat(feed.getEntries(), hasEntry(createdOne1()));
    }

    /* Test showing that when a specific provider is specified, only entries from that provider are included in the feed */

    @Test
    public void assertThatOnlyEntriesFromJiraProviderAreInFeedWhenJiraProviderIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(jira));
        assertThat(feed.getEntries(), allOf(
            hasEntry(createdOne1()),
            allEntries(haveAtlassianApplicationElement(containsString("com.atlassian.jira")))));
    }

    @Test
    public void assertThatOnlyEntriesFromFisheyeProviderAreInFeedWhenFisheyeProviderIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(fisheye));
        assertThat(feed.getEntries(), allOf(
            hasEntry(projectOneChangeSet2()),
            allEntries(allOf(
                haveAtlassianApplicationElement(containsString("com.atlassian.fisheye")),
                withActivityObjectElement(withType(equalTo(changeset().iri().toASCIIString())))))));
    }

    @Test
    public void assertThatOnlyEntriesFromCrucibleProviderAreInFeedWhenCrucibleProviderIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), provider(crucible));
        assertThat(feed.getEntries(), allOf(
            hasEntry(createdReviewOne1()),
            allEntries(allOf(
                haveAtlassianApplicationElement(containsString("com.atlassian.fisheye")),
                not(withActivityObjectElement(withType(equalTo(changeset().iri().toASCIIString()))))))));
    }

    /* Tests showing that when a specific project key is specified, only entries from the linked project, space or
     * repository are included in feed. */

    @Test
    public void assertThatOnlyEntriesFromJiraProjectOneAreInFeedWhenOneKeyIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(jira));

        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(hasEntry(createdOne1()), not(hasEntry(createdTwo1())))));
    }

    @Test
    public void assertThatOnlyEntriesFromFisheyeRepositoryOneAreInFeedWhenOneKeyIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(fisheye));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(hasEntry(projectOneChangeSet2()), not(hasEntry(changeSet2())))));
    }

    @Test
    public void assertThatOnlyEntriesFromCrucibleProjectCrOneAreInFeedWhenOneKeyIsSpecified()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(IS, "ONE"), provider(crucible));

        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(hasEntry(createdReviewOne1()), not(hasEntry(review1Created())))));
    }

    /* Tests showing that when a specific project key is filtered, no entries from the linked project, space or
     * repository are included in feed. */

    @Test
    public void assertThatNoEntriesFromFisheyeRepositoryOneAreInFeedWhenOneKeyIsFiltered()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(fisheye));
        assertThat(feed.getEntries(), not(hasEntry(projectOneChangeSet2())));
    }

    @Test
    public void assertThatEntriesFromFisheyeRepositoryTstAreInFeedWhenOneKeyIsFiltered()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(fisheye));
        assertThat(feed.getEntries(), hasEntry(changeSet2()));
    }

    @Test
    public void assertThatNoEntriesFromCrucibleProjectCrOneAreInFeedWhenOneKeyIsFiltered()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS), key(NOT, "ONE"), provider(crucible));
        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(not(hasEntry(createdReviewOne1())), hasEntry(review1Created()))));
    }

    @Test
    public void assertThatUsingAdminUserFilterDoesNotReturnBuildEntriesPerformedByRwallace()
    {
        Feed feed = client.get(user(IS, ADMIN_USER), maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntries(withAuthorElement(equalTo("rwallace")))));
    }

    @Test
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromJira()
    {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksJiraFilters());
    }

    private Matcher<? super StreamsConfigRepresentation> hasAppLinksJiraFilters()
    {
        return hasProviderFilter(withProviderKey(startsWith("issues")));
    }

    @Test
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromFisheye()
    {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksFisheyeFilters());
    }

    private Matcher<? super StreamsConfigRepresentation> hasAppLinksFisheyeFilters()
    {
        return hasProviderFilter(withProviderKey(startsWith("source")));
    }

    @Test
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromCrucible()
    {
        assertThat(restTester.getStreamsConfigRepresentation(), hasAppLinksCrucibleFilters());
    }

    protected final Matcher<? super StreamsConfigRepresentation> hasSelfLinkedCrucibleFilters()
    {
        return new TypeSafeDiagnosingMatcher<StreamsConfigRepresentation>()
        {
            @Override
            protected boolean matchesSafely(StreamsConfigRepresentation rep, Description mismatchDescription)
            {
                StreamsConfigRepresentation config = restTester.getStreamsConfigRepresentation();
                Iterable<ProviderFilterRepresentation> reviewProviders =
                    filter(config.getFilters(), new Predicate<ProviderFilterRepresentation>()
                    {
                        public boolean apply(ProviderFilterRepresentation filter)
                        {
                            return filter.getKey().startsWith("reviews");
                        }
                    });

                //the local provider exists
                if (size(reviewProviders) == 1)
                {
                    mismatchDescription.appendText("review provider filter count was " + size(reviewProviders));
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("self linked crucible filter exists");
            }
        };
    }

    protected final Matcher<? super StreamsConfigRepresentation> hasAppLinksCrucibleFilters()
    {
        return hasProviderFilter(withProviderKey(startsWith("reviews")));
    }

    @Test
    public abstract void assertThatFilterProvidersAreSorted();

    protected final void assertThatFilterProvidersAreSortedWithLocalProvidersFirst(String... localProviders)
    {
        assertTrue(ProviderFilterOrdering.prioritizing(localProviders).isOrdered(restTester.getStreamsConfigRepresentation().getFilters()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatUrlProxyResourceIsWhitelistedToAccessJira()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:2990/streams/").getStatus(),
                   anyOf(equalTo(OK.getStatusCode()), equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatUrlProxyResourceIsWhitelistedToAccessFeCru()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:3990/streams/").getStatus(),
                   anyOf(equalTo(OK.getStatusCode()), equalTo(HttpServletResponse.SC_MOVED_TEMPORARILY)));
    }

    /*
     * These protected methods are used by both ConfluenceApplinksTest and JiraApplinksTest.
     */
    protected final Matcher<? super Entry> blogInFrench()
    {
        return withTitle(containsString("le blogue"));
    }

    protected final Matcher<? super Entry> commentedIssueInFrench()
    {
        return withTitle(containsString("comment\u00e9"));
    }

    protected final Matcher<? super Entry> createdIssueInFrench()
    {
        return withTitle(containsString("cr\u00e9\u00e9"));
    }

    protected final Matcher<? super Entry> editedInFrench()
    {
        return withTitle(containsString("ajout\u00e9"));
    }

    protected final Matcher<? super Entry> createdWtfBlog()
    {
        return withTitle(allOf(containsString(ADMIN_USER), containsString("blogged"), containsString("WTF?!?")));
    }

    protected final Matcher<? super Entry> projectOneChangeSet2()
    {
        return withTitle(allOf(
            containsString("rwallace"), containsString("committed"), containsString("cs=2"), containsString("Project ONE")));
    }

    protected final Matcher<? super Entry> projectTst2ChangeSet1()
    {
        return withTitle(allOf(
            containsString("pkaeding"), containsString("committed"), containsString("cs=1"), containsString("TST2")));
    }

    protected final Matcher<? super Entry> createdReviewOne1() {
        return createdReview("CR-ONE-1");
    }

    protected final Matcher<? super Entry> createdReview(final String reviewKey)
    {
        return withTitle(allOf(containsString("created"), containsString("review"), containsString(reviewKey)));
    }

    protected Matcher<? super Entry> addedTutorialPage()
    {
        return withTitle(allOf(containsString("added"), containsString("Tutorial")));
    }

    protected Matcher<? super Entry> projectOneBuildOne()
    {
        return withTitle(allOf(containsString("Project ONE"), containsString("#1"), containsString("ONE-ONE10-JOB1-1")));
    }
}
