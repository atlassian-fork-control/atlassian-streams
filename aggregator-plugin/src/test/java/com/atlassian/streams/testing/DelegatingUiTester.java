package com.atlassian.streams.testing;

import com.atlassian.integrationtesting.ui.UiTester;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import be.roam.hue.doj.Doj;

public abstract class DelegatingUiTester implements UiTester
{
    private final UiTester uiTester;

    public DelegatingUiTester(UiTester uiTester)
    {
        this.uiTester = uiTester;
    }

    public final void clickElementWithId(String id)
    {
        uiTester.clickElementWithId(id);
    }

    public final void closeWindows()
    {
        uiTester.closeWindows();
    }

    public final Doj currentPage()
    {
        return uiTester.currentPage();
    }

    public final void destroy()
    {
        uiTester.destroy();
    }

    public final Doj elementById(String id)
    {
        return uiTester.elementById(id);
    }

    public final String getBaseUrl()
    {
        return uiTester.getBaseUrl();
    }

    public final HtmlElement getElementById(String id)
    {
        return uiTester.getElementById(id);
    }

    public final String getHtmlContents()
    {
        return uiTester.getHtmlContents();
    }

    public final String getLoggedInUser()
    {
        return uiTester.getLoggedInUser();
    }

    public final String getStyleClass(String elementId)
    {
        return uiTester.getStyleClass(elementId);
    }

    public final HtmlPage gotoPage(String relativePath)
    {
        return uiTester.gotoPage(relativePath);
    }

    public final boolean isOnLoginPage()
    {
        return uiTester.isOnLoginPage();
    }

    public final void logInAs(String username)
    {
        uiTester.logInAs(username);
    }

    public final void logout()
    {
        uiTester.logout();
    }

    public final Page refreshPage()
    {
        return uiTester.refreshPage();
    }

    public final void restore(String backup, String user)
    {
        uiTester.restore(backup, user);
    }

    public final void restore(String backup)
    {
        uiTester.restore(backup);
    }

    public final int waitForAsyncEventsThatBeginWithin(long delayMillis)
    {
        return uiTester.waitForAsyncEventsThatBeginWithin(delayMillis);
    }

    public final int waitForAsyncEventsThatBeginWithinDefaultTime()
    {
        return uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public final int waitForAsyncEventsToComplete()
    {
        return uiTester.waitForAsyncEventsToComplete();
    }

    public final int waitForAsyncEventsToComplete(long timeoutMillis)
    {
        return uiTester.waitForAsyncEventsToComplete(timeoutMillis);
    }
    
    public final boolean isJavaScriptEnabled()
    {
        return uiTester.isJavaScriptEnabled();
    }
    
    public final void setJavaScriptEnabled(boolean enabled)
    {
        uiTester.setJavaScriptEnabled(enabled);
    }
}
