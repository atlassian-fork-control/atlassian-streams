package com.atlassian.streams.testing;

import java.io.File;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.ui.Bamboo;
import com.atlassian.integrationtesting.ui.CompositeUiTester.Backup;
import com.atlassian.integrationtesting.ui.CompositeUiTester.Login;
import com.atlassian.integrationtesting.ui.CompositeUiTester.WebSudoLogin;
import com.atlassian.integrationtesting.ui.Confluence;
import com.atlassian.integrationtesting.ui.FeCru;
import com.atlassian.integrationtesting.ui.Jira;
import com.atlassian.integrationtesting.ui.RunningTestGroup;
import com.atlassian.integrationtesting.ui.UiRunListener.OutputDirectory;
import com.atlassian.integrationtesting.ui.UiTestRunner;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.integrationtesting.ui.UiTesterFunctionProvider;
import com.atlassian.integrationtesting.ui.UiTesters;
import com.atlassian.sal.api.ApplicationProperties;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.spi.Message;

import org.junit.runners.model.InitializationError;

import static com.atlassian.integrationtesting.ui.UiTesters.logInByGoingTo;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class StreamsUiTesterRunner extends CompositeTestRunner
{
    public StreamsUiTesterRunner(Class<?> klass) throws InitializationError
    {
        super(klass, compose());
    }

    public static Composer compose()
    {
        Injector injector = Guice.createInjector(new StreamsModule());
        return CompositeTestRunner.compose().
            from(UiTestRunner.compose(injector)).
            afterTestClass(new DestroyClients(injector));
    }

    static final class StreamsModule extends AbstractModule
    {
        @Override
        protected void configure() {}

        @Provides @Singleton @RunningTestGroup String getRunningTestGroup()
        {
            if (isBlank(TestGroupRunner.getRunningTestGroup()))
            {
                throw new ConfigurationException(ImmutableList.of(new Message("No testGroup configured - can't figure out which UiTester to use without a test group in the form {application}-v{version}")));
            }
            return TestGroupRunner.getRunningTestGroup();
        }
        
        @Provides @Singleton ApplicationProperties getApplicationProperties()
        {
            return ApplicationPropertiesImpl.getStandardApplicationProperties();
        }

        @Provides @Singleton @OutputDirectory File outputDirectory(@RunningTestGroup String runningTestGroup)
        {
            return new File("target", runningTestGroup + "-htmlunit");
        }

        @Provides @Singleton RestTester getRestTester(ApplicationProperties applicationProperties)
        {
            return new RestTester(applicationProperties);
        }
        
        @Provides @Singleton UiTester getUiTester(ApplicationProperties applicationProperties,
                @RunningTestGroup String runningTestGroup)
        {
            return Testers.valueOf(runningTestGroup.toUpperCase()).get(applicationProperties);
        }
        
        @Provides @Singleton StreamsUiTester getStreamsUiTester(UiTester uiTester)
        {
            return new StreamsUiTester(uiTester);
        }

        @Provides @Singleton ThirdPartyClient getThirdPartyClient(ApplicationProperties applicationProperties)
        {
            return new ThirdPartyClient(applicationProperties);
        }
        
        enum Testers
        {
            BAMBOO(Bamboo.v5_7),
            CONFLUENCE(Confluence.v5_6),
            FECRU(FeCru.v3_6),
            JIRA(Jira.v6_3);
            
            private final UiTesterFunctionProvider provider;

            Testers(UiTesterFunctionProvider provider)
            {
                this.provider = provider;
            }
            
            public UiTester get(ApplicationProperties applicationProperties)
            {
                return UiTesters.newUiTester(applicationProperties, provider);
            }
        }
    }
    
    /**
     * Because we have a hacked up version of Fisheye and the dashboard is broken we need a customized login 
     * that will redirect to somewhere other than the dashboard, which currently returns a 500.
     */
    private enum FeCruWithCustomizedLogIn implements UiTesterFunctionProvider
    {
        v2_3(FeCru.v2_3);
        
        private final UiTesterFunctionProvider delegate;

        FeCruWithCustomizedLogIn(UiTesterFunctionProvider delegate)
        {
            this.delegate = delegate;
        }
        
        public Function<UiTester, String> getLoggedInUser()
        {
            return delegate.getLoggedInUser();
        }

        public Function<UiTester, Boolean> isOnLogInPage()
        {
            return delegate.isOnLogInPage();
        }

        public Function<Login, HtmlPage> logIn()
        {
            return logInByGoingTo("login?origUrl=/streams/browse").formName("loginform").userInputName("username").passwordInputName("password").build();
        }

        public Function<UiTester, Void> logout()
        {
            return delegate.logout();
        }

        public Function<Backup, Void> restore()
        {
            return delegate.restore();
        }

        public Function<WebSudoLogin, HtmlPage> webSudoLogIn()
        {
            return delegate.webSudoLogIn();
        }
    }
    
    private static final class DestroyClients implements Function<AfterTestClass, Void>
    {
        private final Injector injector;

        public DestroyClients(Injector injector)
        {
            this.injector = injector;
        }

        public Void apply(AfterTestClass from)
        {
            injector.getInstance(FeedClient.class).destroy();
            injector.getInstance(RestTester.class).destroy();
            return null;
        }
    }    
}
