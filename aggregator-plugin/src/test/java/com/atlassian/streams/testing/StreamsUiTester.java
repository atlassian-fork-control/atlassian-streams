package com.atlassian.streams.testing;

import com.atlassian.integrationtesting.ui.UiTester;

public class StreamsUiTester extends DelegatingUiTester
{
    public StreamsUiTester(UiTester uiTester)
    {
        super(uiTester);
    }
}
