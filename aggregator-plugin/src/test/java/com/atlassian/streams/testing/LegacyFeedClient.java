package com.atlassian.streams.testing;

import com.atlassian.sal.api.ApplicationProperties;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.joda.time.DateTime;

@Singleton
public class LegacyFeedClient extends AbstractFeedClient
{
    @Inject
    public LegacyFeedClient(ApplicationProperties applicationProperties)
    {
        super(applicationProperties);
    }

    @Override
    protected String getPath()
    {
        return "/plugins/servlet/streams";
    }

    public static Parameter key(String value)
    {
        return param("key", value);
    }

    public static Parameter itemKey(String value)
    {
        return param("itemKey", value);
    }

    public static Parameter filter(String value)
    {
        return param("filter", value);
    }

    public static Parameter user(String value)
    {
        return param("filterUser", value);
    }

    public static Parameter minDate(DateTime dateTime)
    {
        return minDate(dateTime.getMillis());
    }

    public static Parameter minDate(long min)
    {
        return param("minDate", Long.toString(min));
    }

    public static Parameter maxDate(DateTime dateTime)
    {
        return maxDate(dateTime.getMillis());
    }

    public static Parameter maxDate(long max)
    {
        return param("maxDate", Long.toString(max));
    }
}
