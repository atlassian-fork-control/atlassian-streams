package com.atlassian.streams.testing;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.thirdparty.rest.representations.ActivityCollectionRepresentation;
import com.atlassian.streams.thirdparty.rest.representations.ActivityRepresentation;

import com.google.common.collect.ImmutableList;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.uri.UriBuilderImpl;

import static com.atlassian.streams.thirdparty.rest.MediaTypes.STREAMS_THIRDPARTY_JSON;

/**
 * Simple client for the third-party activity REST API.
 * <p>
 * This currently uses generic JSON objects rather than the API-specific representation classes from
 * streams-thirdparty-plugin.
 */
public class ThirdPartyClient
{
    private static final String DEFAULT_USERNAME = "admin";
    private static final String DEFAULT_PASSWORD = "admin";
    
    private final Client client;
    private final ApplicationProperties applicationProperties;

    public ThirdPartyClient(ApplicationProperties applicationProperties)
    {
        this(applicationProperties, true);
    }

    public ThirdPartyClient(ApplicationProperties applicationProperties, boolean login)
    {
        this(applicationProperties, login, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public ThirdPartyClient(ApplicationProperties applicationProperties, String username, String password)
    {
        this(applicationProperties, true, username, password);
    }

    private ThirdPartyClient(ApplicationProperties applicationProperties, boolean login, String username, String password)
    {
        this.applicationProperties = applicationProperties;
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(com.atlassian.streams.thirdparty.rest.representations.JsonProvider.class);
        client = Client.create(config);
        if (login)
        {
            client.addFilter(new HTTPBasicAuthFilter(username, password));
        }
        client.setFollowRedirects(false);
    }

    public ThirdPartyClient as(String username, String password)
    {
        return new ThirdPartyClient(applicationProperties, username, password);
    }

    public void destroy()
    {
        client.destroy();
    }

    public ClientResponse postActivity(ActivityRepresentation rep)
    {
        WebResource resource = client.resource(newBaseUriBuilder().build());
        return resource.type(STREAMS_THIRDPARTY_JSON).post(ClientResponse.class, rep);
    }

    public Iterable<URI> getActivityUris()
    {
        WebResource resource = client.resource(newBaseUriBuilder().build());
        ActivityCollectionRepresentation response = resource.accept(STREAMS_THIRDPARTY_JSON).get(ActivityCollectionRepresentation.class);
        ImmutableList.Builder<URI> ret = ImmutableList.builder();
        for (ActivityRepresentation item: response.getItems())
        {
            ret.add(item.getLinks().get("self"));
        }
        return ret.build();
    }
    
    public ClientResponse deleteActivity(URI activityUri)
    {
        return client.resource(newActivityUriBuilder(activityUri).build()).delete(ClientResponse.class);
    }
    
    public ClientResponse deleteAllActivities()
    {
        return client.resource(newBaseUriBuilder().build()).delete(ClientResponse.class);
    }
    
    protected UriBuilder newBaseUriBuilder()
    {
        return new UriBuilderImpl().replacePath(applicationProperties.getBaseUrl()).path("/rest/activities/1.0/");
    }

    protected UriBuilder newActivityUriBuilder(URI activityUri)
    {
        String basePath = URI.create(applicationProperties.getBaseUrl()).getPath();
        return UriBuilderImpl.fromUri(applicationProperties.getBaseUrl()).path(activityUri.toASCIIString().substring(basePath.length()));
    }
}
