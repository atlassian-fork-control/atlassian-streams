package it.com.atlassian.streams.confluence;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.matchesRegEx;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/user-timezone.zip")
public class ConfluenceUserTimeZoneTest
{
    @Inject static RestTester restTester;
    @Inject static ApplicationProperties applicationProperties;

    @Test
    public void assertThatAdminUserTimeZoneIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), is(equalTo("-0200")));
    }

    @Test
    public void assertThatUserTimeZoneIsCorrectWhenUsingADifferentUser()
    {
        RestTester userRestTester = new RestTester(applicationProperties, "barney", "barney");
        ConfigPreferencesRepresentation prefs = userRestTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), is(equalTo("+0800")));
    }

    @Test
    public void assertThatNotNullUserTimeZoneIsReturnedWhenThereIsNoLoggedInUser()
    {
        RestTester anonRestTester = new RestTester(applicationProperties, false);
        ConfigPreferencesRepresentation prefs = anonRestTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeZone(), matchesRegEx("[+-]\\d{4}"));
    }
}
