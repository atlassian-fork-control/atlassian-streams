package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withEmptyContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/edit-description-field.xml")
public class EditDescriptionFieldTest
{
    @Inject static FeedClient client;

    public static Matcher<? super Entry> issueOne2Created()
    {
        return withTitle(allOf(containsString("ONE-2"),
                containsString("I like creating bugs"),
                containsString("created")));
    }

    public static Matcher<? super Entry> issueOne2Updated()
    {
        return withTitle(allOf(containsString("ONE-2"),
                containsString("I like creating bugs"),
                containsString("updated the Description")));
    }

    public static Matcher<? super Entry> issueOne3Created()
    {
        return withTitle(allOf(containsString("ONE-3"),
                containsString("Now this is a cool bug"),
                containsString("created")));
    }

    public static Matcher<? super Entry> issueOne3Updated()
    {
        return withTitle(allOf(containsString("ONE-3"),
                containsString("Now this is a cool bug"),
                containsString("updated the Description")));
    }

    @Test
    public void assertThatUpdatingTheDescriptionRendersTheDescriptionInContent()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(feed.getEntries(), hasEntry(allOf(issueOne3Updated(),
                withContent(allOf(containsString("I just updated the cool bug description"))))));
    }

    @Test
    public void assertThatUpdatingTheDescriptionWithCommentRendersBothTheDescriptionAndCommentInContent()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(feed.getEntries(), hasEntry(allOf(issueOne3Updated(),
                withContent(allOf(containsString("blockquote"),
                    containsString("oops forgot to add the description"),
                    containsString("this is the description of the cool bug"))))));
    }
 
    @Test
    public void assertThatCreatedIssueDoesNotShowDescriptionIfCreatedWithoutDescription()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-3"));

        assertThat(feed.getEntries(), hasEntry(allOf(issueOne3Created(), withEmptyContent())));
    }

    @Test
    public void assertThatUpdatingTheDescriptionDoesNotChangeTheDescriptionWhenCreated()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-2"));

        assertThat(feed.getEntries(), hasEntry(allOf(issueOne2Created(),
                withContent(not(containsString("I have updated my description"))))));
    }

    @Test
    public void assertThatUpdatingTheDescriptionDoesNotOverEncodeTheDescription()
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-2"));

        assertThat(feed.getEntries(), hasEntry(allOf(issueOne2Updated(),
                withContent(not(containsString("<script>alert('1')</script> "))))));
    }

}
