package it.com.atlassian.streams.thirdparty;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.atlassian.streams.testing.ThirdPartyTests;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorElement;
import static com.atlassian.streams.testing.matchers.Matchers.withAuthorLink;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(CONFLUENCE)
@RestoreOnce("confluence/backups/base.zip")
public class ConfluenceThirdPartyTest extends ThirdPartyTests
{
    private static final String ANON_AVATAR_URL = "/streams/images/icons/profilepics/anonymous.png";

    @Override
    public String getValidProjectKey()
    {
        return "ds";
    }
    
    @Override
    public String getInvalidProjectKey()
    {
        return "bad";
    }

    @SuppressWarnings("unchecked")
    @Test
    public void activityWithUnrecognizedUserAndNoAvatarSpecifiedInRequestHasAnonymousAvatar()
    {
        postActivityAndAssertSuccess(activity(actorWithId(UNKNOWN_USER_NAME), generator()));

        Feed feed = client.getAs("admin", thirdPartyModule());

        assertThat(feed.getEntries(), hasEntry(allOf(withAuthorElement(equalTo(UNKNOWN_USER_NAME)),
                                                     withAuthorLink(whereHref(containsString(ANON_AVATAR_URL))))));
    }
}
