package it.com.atlassian.streams;

import be.roam.hue.doj.Doj;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.base.Function;
import com.google.inject.internal.ImmutableList;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

import java.io.IOException;

import static com.google.common.collect.Iterables.transform;
import static it.com.atlassian.streams.StreamsQUnitJsTest.Result.PASS;

@RunWith(StreamsQUnitJsTest.JsQUnitRunner.class)
public class StreamsQUnitJsTest
{
    private static final String BASE_URL = System.getProperty("baseurl", "http://localhost:3990/streams/");
    protected static RunNotifier runNotifier;

    private static final WebClient webClient;

    static
    {
        webClient = new WebClient(BrowserVersion.FIREFOX_38);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
    }

    @Test
    public void runQUnitJsTests() throws IOException
    {
        HtmlPage page = webClient.getPage(ensureTrailingSlash(BASE_URL) + "plugins/servlet/jstests/all-tests");
        webClient.waitForBackgroundJavaScript(1000);

        notifyTestResults(transform(getHtmlResultElements(page), ToQUnitResults.INSTANCE));
    }

    private void notifyTestResults(Iterable<QUnitResult> results)
    {
        for (QUnitResult result : results)
        {
            Description test = Description.createTestDescription(StreamsQUnitJsTest.class, result.testName());
            runNotifier.fireTestStarted(test);
            if (result.result() == PASS)
            {
                pass(test);
            }
            else
            {
                fail(test, result.testMessage());
            }
        }
    }

    private Iterable<HtmlElement> getHtmlResultElements(HtmlPage page)
    {
        // get element with id "qunit-tests" then get all the child result denoted by "li" element. The get("strong")
        // and parent() was placed so we can make sure that only the parent "li" is returned and will not include
        // all the child "li" of the one we need.
        return ImmutableList.of(Doj.on(page).getById("qunit-tests").get("li").get("strong").parent().allElements());
    }

    /**
     * Finishing current QUnit testcase as passed.
     */
    private static void pass(Description test)
    {
        runNotifier.fireTestFinished(test);
    }

    /**
     * Finishing current QUnit testcase as failed.
     */
    private static void fail(Description test, String testMessage)
    {
        Failure failure = new Failure(test, new RuntimeException(testMessage));
        runNotifier.fireTestFailure(failure);
    }

    private String ensureTrailingSlash(String url)
    {
        return url.endsWith("/") ? url : url + "/";
    }

    /**
     * Override the default Junit4 runner class to get the {RunNotifier} before testing.
     */
    public static class JsQUnitRunner extends BlockJUnit4ClassRunner
    {
        public JsQUnitRunner(Class<?> klass) throws InitializationError
        {
            super(klass);
        }

        @Override
        public void run(RunNotifier notifier)
        {
            runNotifier = notifier;
            super.run(notifier);
        }
    }

    private enum ToQUnitResults implements Function<HtmlElement, QUnitResult>
    {
        INSTANCE;

        public QUnitResult apply(HtmlElement element)
        {
            Doj dojElement = Doj.on(element);
            String className = dojElement.classValue();
            String testName = dojElement.get(".test-name").text();
            String testMessage = dojElement.get(".test-message").text();

            return new QUnitResult(testName, Result.valueOf(className.toUpperCase()), testMessage);
        }
    }

    private static final class QUnitResult
    {
        private final String testName;
        private final Result result;
        private final String testMessage;

        QUnitResult(String testName, Result result, String testMessage)
        {
            this.testName = testName;
            this.result = result;
            this.testMessage = testMessage;
        }

        String testName()
        {
            return testName;
        }

        Result result()
        {
            return result;
        }

        String testMessage()
        {
            return testMessage;
        }
    }

    enum Result
    {
        PASS,
        FAIL
    }
}