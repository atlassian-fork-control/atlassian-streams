package it.com.atlassian.streams.applinks;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.streams.testing.AppLinksTestRunner;
import com.atlassian.streams.testing.AppLinksTests;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

import javax.servlet.http.HttpServletResponse;

import static com.atlassian.streams.testing.Crucible.Data.review1Created;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.setUserLanguage;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_OAUTH;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;


@RunWith(JiraApplinksTest.Runner.class)
@TestGroups({APPLINKS, APPLINKS_OAUTH})
public class JiraApplinksTest extends AppLinksTests
{
    @Inject private static UiTester uiTester;

    public static final class Runner extends AppLinksTestRunner
    {
        public Runner(Class<?> klass) throws InitializationError
        {
            super(klass, "jira");
        }
    }

    @Override
    @Ignore("JIRA won't be applinked to itself")
    public void assertThatJiraActivityIsInFeed() {}

    @Override
    @Ignore("JIRA won't have applinks filters for itself")
    public void assertThatFilterResourceHasAppLinksProviderFiltersFromJira() {}

    @Test
    public void assertThatRetrievingFeedFromDisabledConfluenceUserDoesNotReturnPrivateConfluenceEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(addedTutorialPage())), hasEntry(createdOne1())));
    }

    @Ignore("Confluence returns a 401 for disabled users")
    @Test
    public void assertThatRetrievingFeedFromDisabledConfluenceUserDoesReturnPublicConfluenceEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(createdWtfBlog()));
    }

    @Test
    public void assertThatRetrievingFeedFromDisabledFeCruUserDoesNotReturnPrivateCrucibleEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(createdReviewOne1())), hasEntry(createdOne1())));
    }

    @Test
    public void assertThatRetrievingFeedFromDisabledFeCruUserDoesReturnPublicCrucibleEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(review1Created()));
    }

    @Test
    public void assertThatRetrievingFeedFromDisabledFeCruUserDoesNotReturnPrivateFishEyeEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(not(hasEntry(projectTst2ChangeSet1())), hasEntry(createdOne1())));
    }

    @Test
    public void assertThatRetrievingFeedFromDisabledFeCruUserDoesReturnPublicFishEyeEntry()
    {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(projectOneChangeSet2()));
    }

    @Test
    public void assertThatRetrievingFeedFromFeCruDoesntIncludeXssVulnerabilities()
    {
        Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), hasEntry(withTitle(containsString("&lt;script&gt;alert('fecru xss');&lt;/script&gt;"))));
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void assertThatBlogCreatedEntryIsValidInFrench()
    {
        uiTester.logInAs(ADMIN_USER);
        try
        {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(blogInFrench()));
        }
        finally
        {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void assertThatCommentedIssueEntryIsValidInFrench()
    {
        uiTester.logInAs(ADMIN_USER);
        try
        {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(commentedIssueInFrench()));
        }
        finally
        {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void assertThatCreatedIssueEntryIsValidInFrench()
    {
        uiTester.logInAs(ADMIN_USER);
        try
        {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(createdIssueInFrench()));
        }
        finally
        {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void assertThatPageEditedEntryIsValidInFrench()
    {
        uiTester.logInAs(ADMIN_USER);
        try
        {
            setUserLanguage(uiTester, ADMIN_USER, "fr_FR");
            Feed feed = client.getAs(ADMIN_USER, maxResults(MAX_RESULTS));

            assertThat(feed.getEntries(), hasEntry(editedInFrench()));
        }
        finally
        {
            // Restore user preference to site-default language
            setUserLanguage(uiTester, ADMIN_USER, "-1");
            uiTester.logout();
        }
    }

    @Ignore("STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126")
    @Test
    public void assertThatJiraIgnoresAcceptLanguageHeader()
    {
        Feed feed = client.getAs(ADMIN_USER, "fr-FR, fr;q=0.9", maxResults(MAX_RESULTS));

        assertThat(feed.getEntries(), allOf(ImmutableList.<Matcher<? super Iterable<? super Entry>>>of(
                not(hasEntry(blogInFrench())),
                not(hasEntry(commentedIssueInFrench())),
                not(hasEntry(createdIssueInFrench())),
                not(hasEntry(editedInFrench())))));
    }

    @Test @Override
    public void assertThatFilterProvidersAreSorted()
    {
        assertThatFilterProvidersAreSortedWithLocalProvidersFirst("Jira", "Third Parties");
    }

    @Test @Override @Ignore
    public void assertThatUrlProxyResourceIsWhitelistedToAccessJira()
    {
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessJira()
    {
        assertThat(restTester.getProxiedResponse("http://localhost:2990/streams/").getStatus(),
                   equalTo(HttpServletResponse.SC_FORBIDDEN));
    }

    // STRM-2339; call JIRA with a user that exists in in JIRA but not in FeCru - should omit content that's anonymously accessible in FeCru
    @Test
    public void shouldNotExposeNonLocalAnonymousContentToNonUser() {
        Feed feed = client.getAs("jirauser", maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdReview("CR-1"))));
        assertThat(feed.getEntries(), hasEntry(withTitle(containsString("ONE-2"))));
    }

    // STRM-2339; call JIRA with an anonymous user - should omit content that's anonymously accessible in FeCru
    @Test
    public void shouldNotExposeNonRemoteAnonymousContentToAnonymous() {
        Feed feed = client.get(maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), not(hasEntry(createdReview("CR-1"))));
    }

    // STRM-2339: valid case, user exists on both, should see item
    @Test
    public void shouldShowRemoteContentToValidUser() {
        Feed feed = client.getAs(USER_USER, maxResults(MAX_RESULTS));
        assertThat(feed.getEntries(), hasEntry(createdReview("CR-1")));
    }
}

