package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.FeedClient.user;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.Jira.issueType;
import static com.atlassian.streams.testing.Jira.Data.ONE1_COMMENTED;
import static com.atlassian.streams.testing.Jira.Data.ONE1_CREATED;
import static com.atlassian.streams.testing.Jira.Data.ONE2_CREATED;
import static com.atlassian.streams.testing.Jira.Data.TWO1_CREATED;
import static com.atlassian.streams.testing.Jira.Data.commentedOne1;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.Jira.Data.createdOne2;
import static com.atlassian.streams.testing.Jira.Data.createdTwo1;
import static com.atlassian.streams.testing.Jira.IssueType.NEW_FEATURE;
import static com.atlassian.streams.testing.Jira.projectCategory;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasNoEntries;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/base.zip")
public class JiraFiltersActivityStreamTest
{
    @Inject static FeedClient client;

    @Test
    public void assertThatFeedWithKeyParameterOnlyContainsEntriesWithThatKey() throws Exception
    {
        Feed feed = client.getAs("admin", key(IS, "ONE"));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithNotKeyParameterOnlyContainsEntriesWithThatKey() throws Exception
    {
        Feed feed = client.getAs("admin", key(NOT, "ONE"));
        assertThat(feed.getEntries(), not(hasEntries(createdOne2(), commentedOne1(), createdOne1())));
    }

    @Test
    public void assertThatFeedWithItemKeyParameterOnlyContainsEntriesWithThatItemKey() throws Exception
    {
        Feed feed = client.getAs("admin", issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntries(commentedOne1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithNotItemKeyParameterOnlyContainsEntriesWithThatItemKey() throws Exception
    {
        Feed feed = client.getAs("admin", issueKey(NOT, "ONE-1"));
        assertThat(feed.getEntries(), not(hasEntries(commentedOne1(), createdOne1())));
    }

    @Test
    public void assertThatItemKeyParameterIsCaseInsensitive() throws Exception
    {
        Feed feed = client.getAs("admin", issueKey(IS, "oNe-1"));
        assertThat(feed.getEntries(), hasEntries(commentedOne1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithFilterIsUserParameterOnlyContainsIssuesMatchingThatUser() throws Exception
    {
        Feed feed = client.getAs("admin", user(IS, "user"));
        assertThat(feed.getEntries(), hasEntries(createdOne2()));
    }

    @Test
    public void assertThatFeedWithFilterNotUserParameterOnlyContainsIssuesNotMatchingThatUser() throws Exception
    {
        Feed feed = client.getAs("admin", user(NOT, "user"));
        assertThat(feed.getEntries(), not(hasEntries(createdOne2())));
    }

    @Test
    public void assertThatFeedWithTwoFilterUserParametersContainsIssuesMatchingEitherUser() throws Exception
    {
        Feed feed = client.getAs("admin", user(IS, "user", "admin"));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1(), createdTwo1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithFilterIsIssueAddedActivityOnlyContainsIssuesMatchingThatActivity() throws Exception
    {
        Feed feed = client.getAs("admin", activities(IS, pair(issue(), post())));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), createdTwo1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithFilterNotIssueAddedActivityOnlyContainsIssuesNotMatchingThatActivity() throws Exception
    {
        Feed feed = client.getAs("admin", activities(NOT, pair(issue(), post())));
        assertThat(feed.getEntries(), not(hasEntries(createdOne2(), createdTwo1(), createdOne1())));
    }

    @Test
    public void assertThatFeedWithFilterIsIssueAddedAndIssueStartedActivityContainsEntriesMatchingEitherActivity() throws Exception
    {
        Feed feed = client.getAs("admin", activities(IS, pair(issue(), post()), pair(comment(), post())));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1(), createdTwo1(), createdOne1()));
    }

    @Test
    public void assertThatFeedInDescendingOrderWithUpdateDateBeforeMostRecentEntryAndMaxResultsOfOneOnlyContainsLastEntry() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, ONE2_CREATED.plusSeconds(1)), maxResults(1));
        assertThat(feed.getEntries(), hasEntries(createdOne2()));
    }

    @Test
    public void assertThatFeedInDescendingOrderWithUpdateDateBeforeThirdMostRecentEntryAndMaxResultsOfOneOnlyContainsThirdEntry() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, ONE1_COMMENTED.plusSeconds(1)), maxResults(1));
        assertThat(feed.getEntries(), hasEntries(commentedOne1()));
    }

    @Test
    public void assertThatFeedInDescendingOrderWithUpdateDateBeforeSecondMostRecentEntryAndMaxResultsOfOneOnlyContainsSecondEntry() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, TWO1_CREATED.plusSeconds(1)), maxResults(1));
        assertThat(feed.getEntries(), hasEntries(createdTwo1()));
    }

    @Test
    public void assertThatFeedInDescendingOrderWithUpdateDateBeforeOldestEntryAndMaxResultsOfOneOnlyContainsOldestEntry() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, ONE1_CREATED.plusSeconds(1)), maxResults(1));
        assertThat(feed.getEntries(), hasEntries(createdOne1()));
    }

    @Test
    public void assertThatFeedInDescendingOrderWithUpdateDateBeforeOldestEntryAndMaxResultsOfOneContainsNoEntries() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, new DateTime(0)), maxResults(1));
        assertThat(feed.getEntries(), hasNoEntries());
    }

    @Test
    public void assertThatFeedWithUpdateDateAfterFilterOnlyContainsEntriesAfterDate() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(AFTER, ONE1_COMMENTED.minusMinutes(1)));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1()));
    }

    @Test
    public void assertThatFeedWithUpdateDateBeforeOnlyContainsEntriesBeforeDate() throws Exception
    {
        Feed feed = client.getAs("admin", updateDate(BEFORE, TWO1_CREATED.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(createdTwo1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithUpdateDateBetweenDatesOnlyContainsEntriesBetweenThoseDates() throws Exception
    {
        Feed feed = client.getAs("admin",
            updateDate(BETWEEN, ONE1_COMMENTED.minusSeconds(1), ONE1_COMMENTED.plusSeconds(1)));
        assertThat(feed.getEntries(), hasEntries(commentedOne1()));
    }

    @Test
    public void assertThatFeedWithMaxResultsParameterContainsNoMoreThanMaxResults() throws Exception
    {
        Feed feed = client.getAs("admin", maxResults(2));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1()));
    }

    @Test
    public void assertThatFeedWithIssueTypeIsNewFeatureFilterOnlyContainsNewFeatureIssues()
    {
        Feed feed = client.getAs("admin", issueType(IS, NEW_FEATURE));
        assertThat(feed.getEntries(), hasEntries(createdOne2()));
    }

    @Test
    public void assertThatFeedWithIssueTypeIsNotNewFeatureFilterOnlyContainsNewFeatureIssues()
    {
        Feed feed = client.getAs("admin", issueType(NOT, NEW_FEATURE));
        assertThat(feed.getEntries(), not(hasEntries(createdOne2())));
    }

    @Test
    public void assertThatFeedWithProjectCategoryIsContainsCorrectIssues()
    {
        Feed feed = client.getAs("admin", projectCategory(IS, "10000"));
        assertThat(feed.getEntries(), hasEntries(createdOne2(), commentedOne1(), createdOne1()));
    }

    @Test
    public void assertThatFeedWithProjectCategoryIsNotExcludesIssue()
    {
        Feed feed = client.getAs("admin", projectCategory(NOT, "10000"));
        assertThat(feed.getEntries(), hasEntries(createdTwo1()));
        assertThat(feed.getEntries(), not(hasEntries(createdOne1(), createdOne2(), commentedOne1())));
    }
}
