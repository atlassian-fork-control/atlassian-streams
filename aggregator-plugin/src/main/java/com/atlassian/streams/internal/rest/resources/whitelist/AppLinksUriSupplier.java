package com.atlassian.streams.internal.rest.resources.whitelist;

import java.net.URI;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;

class AppLinksUriSupplier implements Supplier<Iterable<URI>>
{
    private final ApplicationLinkService appLinkService;

    public AppLinksUriSupplier(ApplicationLinkService appLinkService)
    {
        this.appLinkService = checkNotNull(appLinkService, "appLinkService");
    }

    public Iterable<URI> get()
    {
        ImmutableList.Builder<URI> uris = ImmutableList.<URI>builder();

        for (ApplicationLink appLink : appLinkService.getApplicationLinks())
        {
            uris.add(appLink.getRpcUrl());
        }

        return uris.build();
    }
}
