package com.atlassian.streams.internal;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.api.common.uri.Uris;
import com.atlassian.streams.spi.StreamsCommentHandler;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.sal.api.xsrf.XsrfHeaderValidator.TOKEN_HEADER;
import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.internal.ActivityProviders.module;
import static com.atlassian.streams.spi.ServletPath.COMMENTS;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.FORBIDDEN;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.REMOTE_POST_REPLY_ERROR;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.isEmpty;

public class PostReplyHandler
{
    private static final Logger log = LoggerFactory.getLogger(PostReplyHandler.class);

    /**
     * Cross-product value of X-Atlassian-Token to indicate that we want to skip XSRF checking.
     * @deprecated the method to setup header will be moved to somewhere in SAL in platform 3. This constant will soon be
     * removed.
     */
    @Deprecated
    public static final String NO_CHECK = "no-check";

    private final ActivityProviders activityProviders;
    private final ApplicationProperties applicationProperties;

    public PostReplyHandler(ActivityProviders activityProviders,
                            ApplicationProperties applicationProperties)
    {
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    public Either<StreamsCommentHandler.PostReplyError, URI> postReply(String pathInfo,
                                                                       String comment,
                                                                       String replyTo)
    {
        final URI baseUri = URI.create(applicationProperties.getBaseUrl());
        if (replyTo != null)
        {
            // See if the replyTo is for this server
            final String thisPath = baseUri.toASCIIString() + COMMENTS.getPath();
            if (replyTo.equals(thisPath) || replyTo.startsWith(thisPath + "/"))
            {
                return routeCommentToLocalProvider(baseUri, replyTo.substring(thisPath.length()), comment);
            }
            else
            {
                return forwardCommentRequest(replyTo, comment);
            }
        }
        else
        {
            return routeCommentToLocalProvider(baseUri, pathInfo, comment);
        }
    }

    private Either<StreamsCommentHandler.PostReplyError, URI> routeCommentToLocalProvider(URI baseUri,
                                                                                          String pathInfo,
                                                                                          String comment)
    {
        List<String> pathElements;
        String moduleKey;
        if (pathInfo != null && pathInfo.length() > 0)
        {
            final String[] parts = pathInfo.split("/");
            moduleKey = Uris.decode(parts[1]);
            pathElements = new ArrayList<String>(parts.length - 1);
            for (int i = 2; i < parts.length; i++)
            {
                pathElements.add(parts[i]);
            }
        }
        else
        {
            throw new MissingModuleKeyException();
        }

        try
        {
            LocalActivityProvider provider = (LocalActivityProvider) getOnlyElement(activityProviders.get(module(moduleKey)));
            return provider.postReply(baseUri, pathElements, comment);
        }
        catch (NoSuchElementException e)
        {
            throw new NoSuchModuleException(moduleKey);
        }
    }


    private Either<StreamsCommentHandler.PostReplyError, URI> forwardCommentRequest(final String replyTo, final String comment)
    {
        // Check that replyTo looks like a comments URL, don't want to be making arbitrary trusted apps calls
        // on remote servers
        Iterable<String> errors = validateReplyTo(replyTo);
        if (isEmpty(errors))
        {
            Option<AppLinksActivityProvider> provider = activityProviders.getRemoteProviderForUri(Uri.parse(replyTo));
            if (!provider.isDefined())
            {
                log.warn("no remote activity provider found for comment URL " + replyTo);
                return Either.left(new StreamsCommentHandler.PostReplyError(FORBIDDEN));
            }
            else
            {
                try
                {
                    Either<StreamsCommentHandler.PostReplyError, URI> response = executeRequest(provider.get().createRequest(replyTo, MethodType.POST), comment);

                    //if your oauth token on the remote server is revoked, you may get a 401 without having a CredentialsRequiredException thrown
                    if (response.isLeft() && HttpServletResponse.SC_UNAUTHORIZED == response.left().get().getType().getStatusCode())
                    {
                        return retryFeedAsAnonymous(provider, replyTo, comment);
                    }

                    return response;
                }
                catch (CredentialsRequiredException e)
                {
                    return retryFeedAsAnonymous(provider, replyTo, comment);
                }
            }
        }
        else
        {
            throw new RemotePostValidationException(errors);
        }
    }

    private Either<StreamsCommentHandler.PostReplyError, URI> retryFeedAsAnonymous(Option<AppLinksActivityProvider> provider, String replyTo, String comment)
    {
        //an authenticated attempt failed. let's try again as an anonymous user
        try
        {
            return executeRequest(provider.get().createAnonymousRequest(replyTo, MethodType.POST), comment);
        }
        catch (CredentialsRequiredException e2)
        {
            //anonymous users are also unpermitted to add comments
            return left(new StreamsCommentHandler.PostReplyError(UNAUTHORIZED));
        }
    }

    /**
     * Execute the given request with the given comment.
     *
     * @param request the request
     * @param comment the comment
     * @return the URI of the successful comment submission or the unsuccessful error type
     */
    @VisibleForTesting
    Either<StreamsCommentHandler.PostReplyError, URI> executeRequest(Request<?, Response> request, String comment)
    {
        try
        {
            // A forward comment should tell the target product to skip XSRF token checking, the check is already taken place in the current product.
            request.setHeader(TOKEN_HEADER, NO_CHECK);
            request.addRequestParameters("comment", comment);
            return request.executeAndReturn(new AddCommentHandler());
        }
        catch (ResponseException e)
        {
            return left(new StreamsCommentHandler.PostReplyError(REMOTE_POST_REPLY_ERROR));
        }
    }

    private Iterable<String> validateReplyTo(final String replyTo)
    {
        // Check that it contains the streams comments servlet path
        if (!replyTo.contains(COMMENTS.getPath()))
        {
            return ImmutableList.of("Will not forward comment request to URL that does not contain streams servlet comments path");
        }
        // Check that it doesn't contain a query string
        if (replyTo.contains("?"))
        {
            return ImmutableList.of("Will not forward comment request to URL that contains a query string");
        }
        // Check that the URL looks like a valid http or https server
        if (!replyTo.matches("https?://.*"))
        {
            return ImmutableList.of("Will not forward comment request to URL that doesn't look like an HTTP url");
        }
        return ImmutableList.of();
    }
    
    private final class AddCommentHandler implements ReturningResponseHandler<Response, Either<StreamsCommentHandler.PostReplyError, URI>>
    {
        public Either<StreamsCommentHandler.PostReplyError, URI> handle(final Response response) throws ResponseException
        {
            switch (response.getStatusCode())
            {
                case HttpServletResponse.SC_CREATED:
                    return right(URI.create(response.getHeader("Location")));
                case HttpServletResponse.SC_UNAUTHORIZED:
                    return left(new StreamsCommentHandler.PostReplyError(UNAUTHORIZED));
                default:
                    return left(new StreamsCommentHandler.PostReplyError(REMOTE_POST_REPLY_ERROR));
            }
        }
    }
}
