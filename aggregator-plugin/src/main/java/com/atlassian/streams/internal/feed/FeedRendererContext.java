package com.atlassian.streams.internal.feed;

import java.net.URI;

import com.atlassian.streams.api.common.Option;

/**
 * Provides data that a {@link FeedRenderer} may need that is independent of the output format.
 */
public interface FeedRendererContext
{
    /**
     * Returns the user name that should be attached to feed entries that have no author.
     */
    String getAnonymousUserName();

    /**
     * Returns the author name to use for aggregated feeds, if the output format requires one.
     */
    String getDefaultFeedAuthor();

    /**
     * Returns the title to use for aggregated feeds, if the output format requires one.
     */
    String getDefaultFeedTitle();

    /**
     * Returns the (square) pixel sizes of the user profile icons that should be shown for
     * each entry.
     */
    Iterable<Integer> getDefaultUserPictureSizes();
    
    /**
     * Returns a user picture URI for a specific image size.
     * @param baseUri  the user picture URI, if available, without any size parameters; the
     *   method may return a some() value even if contextUri is none(), because there may be a
     *   default image
     * @param size  the desired pixel width and height
     * @param application  the current application ID
     * @return  some(URI) if a picture is available, none() if not
     */
    Option<URI> getUserPictureUri(Option<URI> baseUri, int size, String application);
    
    /**
     * Returns true if the global developer mode system property is set.
     */
    boolean isDeveloperMode();
}
