package com.atlassian.streams.internal.feed.builder;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.internal.ActivityProvider;
import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsI18nResolver;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.atlassian.streams.internal.ActivityProvider.Error.timeout;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;

public class FeedFetcher implements TransactionCallback<Either<ActivityProvider.Error, FeedModel>> {
    private final ActivityRequest request;
    private final CancellableTask<StreamsFeed> task;
    private final ActivityProvider activityProvider;
    private final StreamsI18nResolver i18nResolver;
    private static final Logger logger = LoggerFactory.getLogger(FeedFetcher.class);

    public FeedFetcher(StreamsI18nResolver i18nResolver, ActivityRequest request, CancellableTask<StreamsFeed> task, final ActivityProvider activityProvider) {
        this.i18nResolver = i18nResolver;
        this.request = request;
        this.task = task;
        this.activityProvider = activityProvider;
    }

    public Either<ActivityProvider.Error, FeedModel> doInTransaction() {
        try {
            i18nResolver.setRequestLanguages(request.getRequestLanguages());
            StreamsFeed stream = task.call();
            Iterable<StreamsEntry> entries = stream.getEntries();
            FeedModel.Builder builder = FeedModel.builder(request.getUri());
            builder = builder.title(option(stream.getTitle()));
            builder = builder.subtitle(stream.getSubtitle());
            Option<DateTime> some = some(getUpdatedDate(request, entries));
            builder = builder.updated(some);
            builder.addEntries(transform(entries, FeedEntry.fromStreamsEntry()));
            return right(builder.build());
        } catch (CancelledException e) {
            return left(timeout(activityProvider));
        } catch (Exception e) {
            // the transaction template seems to lose some exceptions and errors
            logger.error("Exception building feed", e);
            return left(other(activityProvider));
        } finally {
            i18nResolver.setRequestLanguages(null);
        }
    }

    private DateTime getUpdatedDate(ActivityRequest request, Iterable<StreamsEntry> entries) {
        if (isEmpty(entries)) {
            return new DateTime();
        } else {
            return new DateTime(get(entries, 0).getPostedDate().getMillis());
        }
    }
}
