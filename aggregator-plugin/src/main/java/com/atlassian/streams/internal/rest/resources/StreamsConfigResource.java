package com.atlassian.streams.internal.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.streams.internal.ConfigRepresentationBuilder;

import static com.atlassian.streams.internal.rest.MediaTypes.STREAMS_JSON;
import static com.google.common.base.Preconditions.checkNotNull;

@Path("/config")
@AnonymousAllowed
public class StreamsConfigResource
{
    private static final CacheControl NO_CACHE = new CacheControl();

    static
    {
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }

    private final ConfigRepresentationBuilder representationBuilder;

    public StreamsConfigResource(ConfigRepresentationBuilder representationBuilder)
    {
        this.representationBuilder = checkNotNull(representationBuilder, "representationBuilder");
    }

    @GET
    @Produces(STREAMS_JSON)
    public Response getFilters(@QueryParam("local") boolean local)
    {
        return Response.ok(representationBuilder.getConfigRepresentation(local))
            .type(STREAMS_JSON)
            .cacheControl(NO_CACHE)
            .build();
    }
}
