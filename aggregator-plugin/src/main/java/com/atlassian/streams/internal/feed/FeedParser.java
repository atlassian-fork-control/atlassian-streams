package com.atlassian.streams.internal.feed;

import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

/**
 * An object that can parse input to produce a {@link FeedModel}.
 * <p>
 * This is used to read data from remote feeds which will be aggregated.  If the
 * output format is known to be the same as the input format (i.e. the parser and
 * {@link FeedRenderer} are implemented by the same class), then parsing can be
 * extremely lazy so that the renderer can just output the original encoded entry
 * unchanged; the only feed entry property that needs to be accessible is the
 * entry date.  If the output format may be different than the input format, then
 * the parser will need to produce feed entries that are fully convertible back
 * into StreamsEntries.
 */
public interface FeedParser
{
    /**
     * Parses feed data from a Reader.
     */
    FeedModel readFeed(Reader reader) throws IOException, ParseException;
}
