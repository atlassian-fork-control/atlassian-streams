package com.atlassian.streams.internal;

import java.util.concurrent.Callable;

/**
 * A {@link java.util.concurrent.Callable} that contains the information about which {@link ActivityProvider} it came from
 */
public interface ActivityProviderCallable<V> extends Callable<V>
{
    public ActivityProvider getActivityProvider();
}
