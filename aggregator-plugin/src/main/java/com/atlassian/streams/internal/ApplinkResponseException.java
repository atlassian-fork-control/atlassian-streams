package com.atlassian.streams.internal;

import com.atlassian.sal.api.net.ResponseException;

public class ApplinkResponseException extends ResponseException {

    private final int statusCode;

    public ApplinkResponseException(final int statusCode) {
        super();
        this.statusCode = statusCode;
    }

    public ApplinkResponseException(final String message, final Throwable cause, final int statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public ApplinkResponseException(final String message, final int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public ApplinkResponseException(final Throwable cause, final int statusCode) {
        super(cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
