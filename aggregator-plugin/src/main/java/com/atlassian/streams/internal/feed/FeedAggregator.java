package com.atlassian.streams.internal.feed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.api.common.uri.UriBuilder;
import com.atlassian.streams.internal.ActivityProvider.Error;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

import org.joda.time.DateTime;

import static com.atlassian.streams.api.common.Either.getLefts;
import static com.atlassian.streams.api.common.Either.getRights;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_TIMEZONE_OFFSET;
import static com.atlassian.streams.internal.atom.abdera.StreamsAbdera.AtomParsedFeedHeader;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.transform;

public class FeedAggregator
{
    /**
     * Aggregate the given list of feeds into one feed. If provided a single feed whose
     * URI is identical to the aggregate feed URI, that feed will be returned with no
     * changes.  Otherwise, the title, subtitle, and self link will be set, and the
     * entries will be concatenated.
     *
     * @param feedResponses The feeds to aggregate, including errored feed responses
     * @param selfUri The URI for the aggregated feed's self link
     * @param maxResults The maximum number of entries to appear in the output feed
     * @param title The title of the feed
     * @return The aggregated feed
     */
    public FeedModel aggregate(final Iterable<Either<Error, FeedModel>> feedResponses, final Uri selfUri, final int maxResults, Option<String> title)
    {
        if (sizeEquals(feedResponses, 1))
        {
            Either<Error, FeedModel> onlyResponse = getOnlyElement(feedResponses);
            if (onlyResponse.isRight())
            {
                FeedModel feed = onlyResponse.right().get();
                if (feed.getUri().equals(selfUri))
                {
                    return feed;
                }
            }
        }

        //filter out all non-timeout errors as we do not want to handle those here
        Iterable<Either<Error, FeedModel>> feeds = filter(feedResponses, successesAndTimeoutErrors);

        FeedModel.Builder builder = FeedModel.builder(selfUri);
        builder.title(title);

        final List<FeedEntry> entries = new ArrayList<FeedEntry>();

        for (final Error error : getLefts(feeds))
        {
            if (error.getType().equals(Error.Type.TIMEOUT))
            {
                if (error.getApplicationLinkName().isDefined())
                {
                    builder.addHeaders(Lists.<FeedHeader>newArrayList(new ActivitySourceTimeOutFeedHeader(error.getApplicationLinkName().get())));
                }
            }
            if (error.getType().equals(Error.Type.BANNED))
            {
                if (error.getApplicationLinkName().isDefined())
                {
                    builder.addHeaders(Lists.<FeedHeader>newArrayList(new ActivitySourceBannedFeedHeader(error.getApplicationLinkName().get())));
                }
            }
        }
        for (final FeedModel feed : getRights(feeds))
        {
            builder.addHeaders(filter(feed.getHeaders(), not(timezoneOffset))); // The local timezone offset is added to the feed when writing it out, here we filter remote offsets because they aren't relevant
            Iterables.addAll(entries, transform(feed.getEntries(), fromFeedSource(feed)));
        }

        try
        {
            builder.updated(some(Ordering.natural().max(catOptions(transform(getRights(feeds), toUpdated)))));
        }
        catch (NoSuchElementException nsee)
        {
            // It's possible that none of the feeds will have updated dates. If so, don't set updated
        }

        Collections.sort(entries, REVERSE_CHRONOLOGICAL);
        builder.addEntries(take(maxResults, entries));

        return builder.build();
    }

    private Function<FeedModel,Option<DateTime>> toUpdated = new Function<FeedModel, Option<DateTime>>()
    {
        public Option<DateTime> apply(FeedModel feedModel)
        {
            return feedModel.getUpdated();
        }
    };


    private Predicate<FeedHeader> timezoneOffset = new Predicate<FeedHeader>()
    {
        @Override
        public boolean apply(FeedHeader header)
        {
            if (header instanceof AtomParsedFeedHeader)
            {
                return ((AtomParsedFeedHeader)header).getElement().getQName().equals(ATLASSIAN_TIMEZONE_OFFSET);
            }
            return false;
        }
    };

    private Function<FeedEntry, FeedEntry> fromFeedSource(final FeedModel feed)
    {
        return new Function<FeedEntry, FeedEntry>() {

            private final Option<FeedModel> sourceFeed = some(feed);

            public FeedEntry apply(FeedEntry from)
            {
                return from.toAggregatedEntry(sourceFeed);
            }
        };
    }
    private Predicate<Either<Error, FeedModel>> successesAndTimeoutErrors = new Predicate<Either<Error, FeedModel>>()
    {
        public boolean apply(Either<Error, FeedModel> either)
        {
            return either.isRight() || either.left().get().getType().equals(Error.Type.TIMEOUT) || either.left().get().getType().equals(Error.Type.BANNED);
        }
    };

    /**
     * We use this auxilary method to calculate the size so we don't have to wait for the whole iterable to be
     * available.  The contents of the iterable may be a remote fetch running in a background thread, and we don't
     * want to wait for all of them to complete before being able to continue.
     */
    private boolean sizeEquals(final Iterable<?> iterable, final int size)
    {
        final Iterator<?> it = iterable.iterator();
        int i = 0;
        while (it.hasNext() && (i < size))
        {
            it.next();
            i++;
        }
        return !it.hasNext() && (i == size);
    }

    // Sort the entries in reverse chronological order
    private static final Comparator<FeedEntry> REVERSE_CHRONOLOGICAL = new Comparator<FeedEntry>()
    {
        public int compare(final FeedEntry entry1, final FeedEntry entry2)
        {
            return entry2.getEntryDate().compareTo(entry1.getEntryDate());
        }
    };
}
