package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;

import com.google.common.collect.ImmutableList;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class StreamsConfigRepresentation
{
    @JsonProperty final Collection<ProviderFilterRepresentation> filters;

    @JsonCreator
    public StreamsConfigRepresentation(@JsonProperty("filters") Collection<ProviderFilterRepresentation> filters)
    {
        this.filters = ImmutableList.copyOf(filters);
    }

    public Collection<ProviderFilterRepresentation> getFilters()
    {
        return filters;
    }
}
