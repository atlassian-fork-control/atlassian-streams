package com.atlassian.streams.internal;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;

/**
 * Provides aggregate access to {@link ActivityProvider}s.  Utility methods are provided for aggregating feeds and
 * filtering options from providers and for doing validation.
 */
public class ActivityProviders
{
    private static final Logger log = LoggerFactory.getLogger(ActivityProviders.class);
    
    private Iterable<Supplier<Iterable<ActivityProvider>>> suppliers;

    ActivityProviders(LocalActivityProviders localProviders, AppLinksActivityProviders applinksProviders)
    {
        this.suppliers = ImmutableList.of(localProviders, applinksProviders);
    }

    Iterable<ActivityProvider> get(Predicate<ActivityProvider> predicate)
    {
        return get(ImmutableList.of(predicate));
    }

    Iterable<ActivityProvider> get()
    {
        return get(ImmutableList.<Predicate<ActivityProvider>>of());
    }

    Iterable<ActivityProvider> get(Predicate<ActivityProvider> p1, Predicate<ActivityProvider> p2)
    {
        return get(ImmutableList.of(p1, p2));
    }

    Iterable<ActivityProvider> get(Iterable<Predicate<ActivityProvider>> predicates)
    {
        return filter(concat(transform(suppliers, toProviders())), and(predicates));
    }

    public Option<ActivityProvider> getProviderForUri(Uri uri)
    {
        Iterable<ActivityProvider> matchingProviders = get(matchesUri(uri));
        if (isEmpty(matchingProviders))
        {
            log.warn("no activity provider found for URI " + uri);
            return none();
        }
        else
        {
            // There could be multiple providers whose base URIs match, e.g. if one is at
            // http://extranet.atlassian.com/ and the other is at http://extranet.atlassian.com/jira/.
            // To make sure we match the most specific one, sort by base URL in descending order and
            // take the first one.
            matchingProviders = byBaseUrl().sortedCopy(matchingProviders);
            return some(Iterables.get(matchingProviders, 0));
        }
    }

    public Option<AppLinksActivityProvider> getRemoteProviderForUri(Uri uri)
    {
        return getProviderForUri(uri).flatMap(requireRemoteProvider());
    }
    
    private static Function<Supplier<Iterable<ActivityProvider>>, Iterable<ActivityProvider>> toProviders()
    {
        return SupplierGetterFunction.INSTANCE;
    }

    private enum SupplierGetterFunction implements Function<Supplier<Iterable<ActivityProvider>>, Iterable<ActivityProvider>>
    {
        INSTANCE;

        public Iterable<ActivityProvider> apply(Supplier<Iterable<ActivityProvider>> supplier)
        {
            return supplier.get();
        }
    }

    static Predicate<ActivityProvider> module(String key)
    {
        return new ModulePredicate(checkNotNull(key, "key"));
    }

    private static final class ModulePredicate implements Predicate<ActivityProvider>
    {
        private final String key;

        public ModulePredicate(String key)
        {
            this.key = key;
        }

        public boolean apply(ActivityProvider provider)
        {
            return provider.matches(key);
        }
    }

    static Predicate<ActivityProvider> localOnly(boolean local)
    {
        return local ? local() : Predicates.<ActivityProvider>alwaysTrue();
    }

    static Predicate<ActivityProvider> local()
    {
        return LocalPredicate.INSTANCE;
    }

    private enum LocalPredicate implements Predicate<ActivityProvider>
    {
        INSTANCE;

        public boolean apply(ActivityProvider provider)
        {
            return provider instanceof LocalActivityProvider;
        }
    }

    static Predicate<ActivityProvider> selectedProvider(Iterable<String> selectedProviderKeys)
    {
        return new SelectedProviderPredicate(selectedProviderKeys);
    }

    private static final class SelectedProviderPredicate implements Predicate<ActivityProvider>
    {
        private final Iterable<String> selectedProviderKeys;

        public SelectedProviderPredicate(Iterable<String> selectedProviderKeys)
        {
            this.selectedProviderKeys = selectedProviderKeys;
        }

        public boolean apply(ActivityProvider provider)
        {
            return any(selectedProviderKeys, matches(provider));
        }
    }

    public static Predicate<String> matches(ActivityProvider provider)
    {
        return new ProviderMatches(provider);
    }

    private static final class ProviderMatches implements Predicate<String>
    {
        private final ActivityProvider provider;

        public ProviderMatches(ActivityProvider provider)
        {
            this.provider = provider;
        }

        public boolean apply(String key)
        {
            return provider.matches(key);
        }
    }
    
    private static Predicate<ActivityProvider> matchesUri(Uri uri)
    {
        return new MatchesUriPredicate(uri);
    }
    
    private static final class MatchesUriPredicate implements Predicate<ActivityProvider>
    {
        private final String uri;
        
        public MatchesUriPredicate(Uri uri)
        {
            this.uri = checkNotNull(uri).toString();
        }
        
        public boolean apply(ActivityProvider provider)
        {
            return uri.startsWith(provider.getBaseUrl());
        }
    }
    
    private static Ordering<ActivityProvider> byBaseUrl()
    {
        return byBaseUrl;
    }
    
    private static final Ordering<ActivityProvider> byBaseUrl = new Ordering<ActivityProvider>()
    {
        @Override
        public int compare(ActivityProvider provider1, ActivityProvider provider2)
        {
            return provider2.getBaseUrl().compareTo(provider1.getBaseUrl());
        }  
    };
    
    private static Function<ActivityProvider, Option<AppLinksActivityProvider>> requireRemoteProvider()
    {
        return RequireRemoteProvider.INSTANCE;
    }

    private enum RequireRemoteProvider implements Function<ActivityProvider, Option<AppLinksActivityProvider>>
    {
        INSTANCE;

        public Option<AppLinksActivityProvider> apply(ActivityProvider provider)
        {
            if (provider instanceof AppLinksActivityProvider)
            {
                return some((AppLinksActivityProvider) provider);
            }
            return none();
        }
    }
}
