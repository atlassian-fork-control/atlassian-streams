package com.atlassian.streams.internal;

import com.atlassian.failurecache.failures.ExponentialBackOffFailureCache;
import com.atlassian.failurecache.failures.FailureCache;
import com.atlassian.failurecache.util.date.Clock;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.ActivityProvider.Error;
import com.atlassian.streams.internal.completion.Execution;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import io.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static com.atlassian.streams.api.common.Either.getLefts;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.isEmpty;

/**
 * A service which allows multiple {@link Callable jobs} to be submitted and returns an {@link Iterable} over the return
 * values.  The jobs are run in a {@link ExecutorService pool} in the background.  When iterating over the
 * return values, the first value available will be returned.  If no values are available, the thread will block until
 * a value becomes available.
 */
public final class StreamsCompletionService implements InitializingBean, DisposableBean
{
    private static final Logger logger = LoggerFactory.getLogger(StreamsCompletionService.class);

    // Ignore failure cache results in dev mode with this property set. Used for diagnosis of failing individual streams.
    private static final boolean IGNORE_FAILURE_CACHE = Sys.inDevMode() && Boolean.getBoolean("com.atlassian.streams.aggregator.ignore.failure.cache");

    private final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory;
    private final PluginEventManager pluginEventManager;
    private final ResettableLazyReference<Execution> async = new ResettableLazyReference<Execution>()
    {
        @Override
        protected Execution create() throws Exception
        {
            return new Execution(threadLocalDelegateExecutorFactory, failureCache);
        }
    };
    private final FailureCache<ActivityProvider> failureCache;

    StreamsCompletionService(final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory,
                             final PluginEventManager pluginEventManager, final Clock clock)
    {
        this.threadLocalDelegateExecutorFactory = checkNotNull(threadLocalDelegateExecutorFactory, "threadLocalDelegateExecutorFactory");
        this.pluginEventManager = checkNotNull(pluginEventManager, "pluginEventManager");
        this.failureCache = new ExponentialBackOffFailureCache<ActivityProvider>(clock);
    }

    public Predicate<ActivityProvider> reachable()
    {
        return not(new Predicate<ActivityProvider>()
        {
            public boolean apply(ActivityProvider activityProvider)
            {
                return !IGNORE_FAILURE_CACHE && failureCache.isFailing(activityProvider);
            }
        });
    }

    /**
     * Given some {@link Callable jobs} execute them in parallel and return an Iterable that
     * will block on the first call to {@link Iterator#next()} and return the first available result.
     * <p>
     * Note that the client may block forever if the job becomes stuck (there is no timeout support).
     * Use the overloaded method if timeout support is desired.
     *
     * @param <T> the result type
     * @param callables jobs that return a pair of the original ActivityProvider and the results
     * @return an {@link Iterable} of the results, may block on the first calls to {@link Iterator#next()} to await results becoming available.
     */
    public <T> Iterable<Either<Error, T>> execute(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables)
    {
        if (isEmpty(callables))
        {
            return Collections.emptySet();
        }

        Iterable<Either<Error, T>> results = async.get().invokeAll(callables);

        registerFailures(results);

        return results;
    }

    /**
     * Given some {@link Callable jobs} execute them in parallel and return an Iterable that
     * will block on the first call to {@link Iterator#next()} and return the first available result.
     *
     * @param <T> the result type
     * @param callables jobs that return a pair of the original ActivityProvider and the results
     * @param time the maximum amount of time to allow all {@link Callable}s to complete
     * @param unit the unit of time in which to measure
     * @return an {@link Iterable} of the results, may block on the first calls to {@link Iterator#next()} to await results becoming available.
     */
    public <T> Iterable<Either<Error, T>> execute(final Iterable<? extends ActivityProviderCallable<Either<Error, T>>> callables, final long time, final TimeUnit unit)
    {
        if (isEmpty(callables))
        {
            return Collections.emptySet();
        }

        Iterable<Either<Error, T>> results = async.get().invokeAll(callables, time, unit);

        registerFailures(results);

        return results;
    }

    private <T> void registerFailures(final Iterable<Either<Error, T>> results)
    {
        for (Error error : getLefts(results))
        {
            // Don't log failure cache errors for credentials required or unauthorized - these are per-user errors.
            if (error.getActivityProvider().isDefined() && isNotUserSpecificError(error))
            {
                logger.warn("Registering failure for stream provider {} due to error {}", error.getActivityProvider().get().getName(), error);
                failureCache.registerFailure(error.getActivityProvider().get());
            }
        }
    }

    private boolean isNotUserSpecificError(final Error errors)
    {
        final Error.Type type = errors.getType();
        return !(type.equals(Error.Type.CREDENTIALS_REQUIRED) || type.equals(Error.Type.UNAUTHORIZED));
    }

    private void resetCompletionService()
    {
        if (async.isInitialized())
        {
            async.get().close();
        }
        async.reset();
    }

    public synchronized void afterPropertiesSet()
    {
        resetCompletionService();
        pluginEventManager.register(this);
    }

    public synchronized void destroy()
    {
        resetCompletionService();
        pluginEventManager.unregister(this);
    }

    @PluginEventListener
    public void onShutdown(final PluginFrameworkShutdownEvent event)
    {
        // Because disabled() is only called when the module is disabled by a user, and not when the whole plugin
        // system goes down (on JIRA data import for example), this event listener method is needed to shut the
        // executor down to prevent thread and memory leaks.
        resetCompletionService();
    }
}
