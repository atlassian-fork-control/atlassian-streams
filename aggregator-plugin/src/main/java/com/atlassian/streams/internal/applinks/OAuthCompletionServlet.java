package com.atlassian.streams.internal.applinks;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This is the servlet we will redirect back to after successful completion of the OAuth dance.
 * It stores a per-user timestamp for the applink (maintained by ApplicationLinkServiceExtensionsImpl)
 * since that information is not currently available in the Applinks API, and then outputs some
 * Javascript that will close the OAuth window and notify the caller who opened it.
 * <p>
 * All of the above will eventually be integrated into the existing OAuthApplinksServlet in the
 * Applinks plugin.
 */
@SuppressWarnings("serial")
public class OAuthCompletionServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(OAuthCompletionServlet.class);
    
    public static final String APPLINK_ID_PARAM = "applinkId";
    
    private static final Pattern APPLINK_ID_REGEX = Pattern.compile("[0-9a-z-]+");
    private static final String TEMPLATE = "template/applinks/auth-completion.vm";
    
    private final ApplicationLinkService appLinkService;
    private final ApplicationLinkServiceExtensionsImpl appLinkServiceExtensionsImpl;
    private final TemplateRenderer templateRenderer;
    private final WebResourceManager webResourceManager;
    
    public OAuthCompletionServlet(ApplicationLinkService appLinkService,
                                  ApplicationLinkServiceExtensionsImpl appLinkServiceExtensionsImpl,
                                  TemplateRenderer templateRenderer,
                                  WebResourceManager webResourceManager)
    {
        this.appLinkService = checkNotNull(appLinkService, "appLinkService");
        this.appLinkServiceExtensionsImpl = checkNotNull(appLinkServiceExtensionsImpl, "appLinkServiceExtensionsImpl");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException
    {
        String applinkId = request.getParameter(APPLINK_ID_PARAM);
        if ((applinkId == null) || (!APPLINK_ID_REGEX.matcher(applinkId).matches()))
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        
        ApplicationLink appLink;
        boolean success = false;
        try
        {
            appLink = appLinkService.getApplicationLink(new ApplicationId(applinkId));
            if (appLink != null)
            {
                success = appLinkServiceExtensionsImpl.isAuthorised(appLink);
            }
        }
        catch (TypeNotInstalledException e)
        {
            log.error("Unknown applink type for applink ID '" + applinkId + "'");
            appLink = null;
        }
        
        Map<String, Object> context = ImmutableMap.of("applinkId",
                                                      applinkId,
                                                      "authAdminUri",
                                                      (appLink == null) ? "" : appLinkServiceExtensionsImpl.getUserAdminUri(appLink),
                                                      "success",
                                                      success);
        
        response.setContentType("text/html");
        webResourceManager.requireResource("com.atlassian.auiplugin:ajs");
        templateRenderer.render(TEMPLATE, context, response.getWriter());
    }
}
