package com.atlassian.streams.internal.servlet;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.api.common.uri.UriBuilder;
import com.atlassian.streams.internal.FeedBuilder;
import com.atlassian.streams.internal.HttpParameters;
import com.atlassian.streams.internal.feed.FeedRenderer;
import com.atlassian.streams.internal.feed.FeedRendererContext;
import com.atlassian.streams.internal.feed.FeedModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.internal.LocalActivityProvider.ACCEPT_LANGUAGE_KEY;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

@SuppressWarnings("serial")
public class StreamsActivityServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(StreamsActivityServlet.class);
    private static final String CACHE_BUSTER_PARAM = "_";
    private static final String CHARACTER_ENCODING = "UTF-8";

    private final FeedBuilder feedBuilder;
    private final FeedRenderer feedRenderer;
    private final FeedRendererContext feedRendererContext;
    private final StreamsFeedUriBuilderFactory uriBuilderFactory;
    private final ApplicationProperties applicationProperties;

    public StreamsActivityServlet(FeedBuilder feedBuilder,
                                  FeedRenderer feedRenderer,
                                  FeedRendererContext feedRendererContext,
                                  StreamsFeedUriBuilderFactory uriBuilderFactory,
                                  ApplicationProperties applicationProperties)
    {
        this.uriBuilderFactory = uriBuilderFactory;
        this.feedBuilder = checkNotNull(feedBuilder, "feedBuilder");
        this.feedRenderer = checkNotNull(feedRenderer, "feedRenderer");
        this.feedRendererContext = checkNotNull(feedRendererContext, "feedRendererContext");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
        throws ServletException, IOException
    {
        FeedModel feed;
        final HttpParameters parameters = HttpParameters.parameters(request);
        final String contextPath = request.getContextPath();
        final URI baseUri = parameters.calculateContextUrl(applicationProperties, contextPath);
        try
        {
            feed = feedBuilder.getFeed(getSelf(request), contextPath, parameters,
                    request.getHeader(ACCEPT_LANGUAGE_KEY));
        }
        catch (final RuntimeException e)
        {
            log.error("Error getting activity", e);
            response.sendError(SC_INTERNAL_SERVER_ERROR, "Error occured getting activity: " + e.getMessage());
            return;
        }

        // Render and send to client
        try
        {
            response.setContentType(feedRenderer.getContentType());
            response.setCharacterEncoding(CHARACTER_ENCODING);
            //set no cache headers to ensure the feed wont get cached indefinitely by shindig
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", -1);
            feedRenderer.writeFeed(baseUri, feed, response.getWriter(), feedRendererContext);
        }
        catch (final Exception e)
        {
            log.error("Error sending feed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error occurred sending feed");
        }
    }

    private Uri getSelf(HttpServletRequest request)
    {
        UriBuilder builder = new UriBuilder(request);
        builder.setPath(request.getContextPath() + "/activity");
        builder.removeQueryParameter(CACHE_BUSTER_PARAM);
        builder.removeQueryParameter(HttpParameters.RELATIVE_LINKS_KEY);

        for (Pair<String, String> auth : getAuthParameter())
        {
            List<String> authParams = builder.getQueryParameters(auth.first());
            if (authParams == null || authParams.isEmpty())
            {
                builder.addQueryParameter(auth.first(), auth.second());
            }
        }
        return builder.toUri();
    }

    private Option<Pair<String, String>> getAuthParameter()
    {
        // this is a bit hacky to avoid having to import the UriAuthenticationParameterProvider and use some correct
        // default if nothing is provided, which the StreamsFeedUriBuilder already handles
        URI uriWithAuth = uriBuilderFactory.getStreamsFeedUriBuilder("http://localhost").addAuthenticationParameterIfLoggedIn().getUri();
        for (Map.Entry<String, List<String>> entry : Uri.fromJavaUri(uriWithAuth).getQueryParameters().entrySet())
        {
            return some(pair(entry.getKey(), entry.getValue().get(0)));
        }
        return none();
    }
}
