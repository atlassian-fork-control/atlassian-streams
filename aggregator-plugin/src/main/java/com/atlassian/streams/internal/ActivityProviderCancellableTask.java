package com.atlassian.streams.internal;

import com.atlassian.streams.spi.CancellableTask;

/**
 * ActivityProviderCancellableTask is both an ActivityProviderCallable and a CancellableTask
 */
public interface ActivityProviderCancellableTask<V> extends ActivityProviderCallable<V>, CancellableTask<V>
{
}
