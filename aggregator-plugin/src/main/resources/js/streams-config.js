var ActivityStreams = ActivityStreams || {};

// TODO: STRM-381: This probably needs to be backwards-compatible so that it doesn't break existing streams

/**
 * Creates a dynamic configuration UI where you can add and remove rules for filtering
 * @method smartConfig
 * @param {Object} availableFilters Object representing the available filters
 * @param {Object} originalSettings Object containing the initial configuration settings
 * @param {HTMLElement} container
 * @return {Object} Smart config object
 */
ActivityStreams.config = function(availableFilters, originalSettings, container) {
    var setPref = ActivityStreams.setPref,
        getMsg = ActivityStreams.getMsg,
        smartConfigs = {},
        allProviders = [],
        maxProviderLabelCharacters = ActivityStreams.getPref('maxProviderLabelCharacters');

    /**
     * Gets the internationalized message for a given key
     * @method geti18n
     * @param {String} key i18n key
     * @return {String} Internationalized message
     */
    function geti18n(key) {
        return getMsg('gadget.activity.stream.config.' + key);
    }

    /**
     * Gets the internationalized label for a select option based on the option value and unique "namespace"
     * @method getLabel
     * @param {String} value Value of the option in questions
     * @param {String} namespace To avoid conflicts
     * @return {String} Internationalized label
     */
    function getLabel(value, namespace) {
        var key = namespace ? namespace + '.' + value : value;
        return geti18n(key);
    }

    /**
     * Triggers a 'contentResize' event so that the activity stream will resize itself
     * @method resize
     */
    function resize() {
        container.trigger('contentResize.streams');
    }

    /**
     * Sets all user-modifiable gadget prefs to an empty string
     * @method clearPrefs
     */
    function clearPrefs() {
        setPref('rules', AJS.json.stringify({rules: []}));
    }

    /**
     * Creates and returns a checkbox element
     * @method buildCheckbox
     * @param {Boolean} isChecked Whether the checkbox should be checked by default or not
     * @param {String} name (optional) What to use for the "name" attribute
     * @return {HTMLElement} The dom element for the created checkbox
     */
    function buildCheckbox(isChecked, name) {
        var checkbox = AJS.$('<input type="checkbox" class="checkbox" value="">');
        checkbox.attr('checked', isChecked);
        if (name) {
            checkbox.attr('name', name.replace(/\s/, '-'));
        }
        return checkbox;
    }

    /**
     * Creates and returns an option element for a select
     * @method buildSelectOption
     * @param {String} value The value of the option
     * @param {String} label The text label of the option
     * @param {String} classname (optional) A classname to be applied to the element
     * @return {HTMLElement} The dom element for the created option
     */
    function buildSelectOption(value, label, classname) {
        classname = classname || value;
        return AJS.$('<option value=""></option>')
            .addClass(classname)
            .val(value)
            .text(label);
    }

    /**
     * Creates and returns a select element and corresponding option elements
     * @method buildSelect
     * @param {Array} values An array of values to populate the select with
     * @param {String} selectedValue The value that should be initially selected
     * @param {String} namespace Used to retrieve i18n labels and added as a classname to the select
     * @param {String} elementName (optional) What to use for the "name" attribute
     * @return {HTMLElement} The dom element for the created select
     */
    function buildSelect(values, selectedValue, namespace, elementName) {
        var select = AJS.$('<select class="select"></select>').addClass(namespace);
        if (elementName) {
            select.attr('name', elementName.replace(/\s/, '-'));
        }
        if (AJS.$.isArray(values)) {
            for (var i = 0, len = values.length; i < len; i++) {
                var value = values[i],
                    label,
                    option;
                if (typeof value === 'string') {
                    // value is a string, we'll need to get the i18n label
                    label = getLabel(value, namespace);
                } else {
                    // value should be an object, with 'value' and 'label' properties
                    label = value.name;
                    value = value.key;
                }
                option = buildSelectOption(value, label);
                if (value == selectedValue) {
                    option.attr('selected', 'selected');
                }
                option.appendTo(select);
            }
        } else {
            for (var value in values) {
                var label = values[value],
                    option;
                option = buildSelectOption(value, label);
                if (value == selectedValue || label == selectedValue) {
                    option.attr('selected', 'selected');
                }
                option.appendTo(select);
            }
        }
        return select;
    }

    /**
     * Creates and returns a input element of type "text"
     * @method buildTextField
     * @param {String} startingValue (optional) The initial value of the text field
     * @param {String} name (optional) What to use for the "name" attribute
     * @return {HTMLElement} The dom element for the created text input
     */
    function buildTextField(startingValue, name) {
        buildTextField.input = buildTextField.input || AJS.$('<input class="text" type="text" value="">');
        var input = buildTextField.input.clone();
        if (startingValue) {
            input.val(startingValue);
        }
        if (name) {
            input.attr('name', name.replace(/\s/, '-'));
        }
        return input;
    }

    /**
     * Creates and returns a fieldset with "Apply" and "Cancel" buttons
     * @method buildFormButtons
     * @return {HTMLElement} The dom element for the created fieldset
     */
    function buildFormButtons() {
        var container = AJS.$('<div class="buttons-container"></div>'),
            buttons = AJS.$('<div class="buttons"></div>'),
            button = AJS.$('<button class="submit button" type="submit"></button>')
                .text(geti18n('save'))
                .click(function(e) {
                    e.preventDefault();
                    save();
                });
        button.appendTo(buttons);
        AJS.$('<a href="#" class="streams-cancel cancel"></a>')
            .text(geti18n('cancel'))
            .click(function(e) {
                e.preventDefault();
                clearValidationErrors();
                hideFiltering();
                applySettings(originalSettings);
                applyRules();
            })
            .appendTo(buttons);
        buttons.appendTo(container);
        return container;
    }

    /**
     * Creates and returns a "field group" for configuring the desired number of entries to return
     * @method buildNumOfEntriesFieldGroup
     * @param {String} currentValue The initial "number of entries" value
     * @return {HTMLElement} The dom element for the created field group
     */
    function buildNumOfEntriesFieldGroup(currentValue) {
        var div = AJS.$('<div class="field-group"></div>').attr('id', 'max-results-container'),
            input = buildTextField(currentValue || 10, 'numofentries')
                .attr('maxlength', 2)
                .attr('id', 'numofentries')
                .addClass('numofentries update-on-change');
        div.html(geti18n('numofentries'));
        div.find('span.input-container').append(input);
        return div;
    }

    /**
     * Creates and returns a "field group" for configuring the refresh interval of the streams gadget
     * @method buildRefreshFieldGroup
     * @param {String} selectedValue The initial refresh value
     * @return {HTMLElement} The dom element for the created field group
     */
    function buildRefreshFieldGroup(selectedValue) {
        var div = AJS.$('<div class="field-group refresh-container"></div>'),
            checkboxDiv = AJS.$('<div class="checkbox"></div>'),
            checked =  selectedValue !== 'false',
            id = 'refresh-checkbox';

        if (checked) {
            div.addClass('do-refresh');
        }
        buildCheckbox(checked, 'refresh')
            .attr('id', id)
            .change(function(e) {
                var target = AJS.$(e.target);
                target.closest('div.refresh-container').toggleClass('do-refresh', target.is(':checked'));
            })
            .appendTo(checkboxDiv);

        AJS.$('<label></label>')
            .text(geti18n('refresh'))
            .attr('for', id)
            .appendTo(checkboxDiv);

        checkboxDiv.appendTo(div);

        buildSelect(['15', '30', '60', '120'], selectedValue, 'refresh', 'refresh-interval')
            .attr('id', 'refresh-select')
            .appendTo(div);
        return div;
    }

    /**
     * Creates and returns a legend element with the specified text
     * @method buildLegend
     * @param {Boolean} text The text for the legend
     * @return {HTMLElement} A dom element for the created legend
     */
    function buildLegend(text) {
        return AJS.$('<legend></legend>').text(text);
    }

    /**
     * Hides or shows the smart config for the global filters
     * @method toggleGlobalFilters
     * @param {Event} e The event object
     */
    function toggleGlobalFilters(e) {
        var isEnabled = AJS.$(e.target).attr('checked');
        smartConfigs['streams'].toggle(!isEnabled);
    }

    /**
     * Creates and returns a text description of global filters and a link for adding a new filter
     * @method buildGlobalFiltersDescription
     * @param {Boolean} isSingleProvider Whether or not there's only a single provider
     * @return {HTMLElement} A dom element for the created div
     */
    function buildGlobalFiltersDescription(isSingleProvider) {
        var description = isSingleProvider ? geti18n('global.filters.single.description') : geti18n('global.filters.description'),
            linkText = isSingleProvider ? geti18n('global.filters.single.add') : geti18n('global.filters.add'),
            div = AJS.$('<div class="global-filter-description stream-provider"></div>')
                .text(description + ' ')
                .attr('data-key', 'streams');
        AJS.$('<a href="#" class="add-filter-link" id="add-global-filter"></a>')
            .text(linkText)
            .click(addFilter)
            .appendTo(div);
        return div;
    }

    /**
     * Creates and returns a fieldset for configuring global filters
     * @method buildGlobalFilters
     * @param {Object} filter An object containing information on available filter options
     * @param {Array} settings An array of objects containing information on defined filter rules
     * @param {Boolean} isSingleProvider Whether or not there's only a single provider
     * @return {HTMLElement} A dom element for the created fieldset
     */
    function buildGlobalFilters(filter, settings, isSingleProvider) {
        var fieldset = AJS.$('<fieldset id="streams-global-filters"></fieldset>'),
            options = filter.options,
            rules = settings && settings.rules,
            isChecked = rules && rules.length,
            headerText = isSingleProvider ? geti18n('global.filters.single') : geti18n('global.filters');
        buildLegend(headerText).appendTo(fieldset);
        buildGlobalFiltersDescription(isSingleProvider).appendTo(fieldset);
        options = addIsOperatorToDateOption(options);
        smartConfigs[filter.key] = AJS.smartConfig(options, {
            parent: fieldset,
            allowZeroRules: true,
            hidden: !isChecked,
            addText: geti18n('add'),
            removeText: geti18n('remove'),
            dateRangeSeparatorText: geti18n('date.range.to'),
            rules: rules
        });
        return fieldset;
    }

    /**
     * Enables or disabled a provider
     * @method toggleProvider
     * @param {Event} e The event object
     */
    function toggleProvider(e) {
        var target = AJS.$(e.target),
            isChecked = target.attr('checked'),
            providerElement = target.closest('div.stream-provider'),
            provider = providerElement.attr('data-key'),
            smartConfig = smartConfigs[provider];
        if (isChecked) {
            providerElement.removeClass('provider-disabled');
            target.attr('title', geti18n('stream.disable'));
            smartConfig && smartConfig.show();
        } else {
            providerElement.addClass('provider-disabled');
            target.attr('title', geti18n('stream.enable'));
            smartConfig && smartConfig.hide();
        }
    }

    /**
     * After a specified delay, fades an element out and then removes it from the dom
     * @method fadeOutAndRemove
     * @param {HTMLElement} element The element to remove
     * @param {Number} delay (optional) The time, in ms, to delay before fading out the element
     */
    function fadeOutAndRemove(element, delay) {
        delay = delay || 2500;
        setTimeout(function() {
            element.fadeOut(250, function() {
                element.remove();
            });
        }, delay);
    }

    /**
     * Creates and inserts an html element with the specified message triggered by an "add filter" link
     * @method displayAddFilterMessage
     * @param {String} message The message to display
     * @param {HTMLElement} addLink The DOM element that triggered this message
     * @param {String} className (optional) Class name to add to the created element
     */
    function displayAddFilterMessage(message, addLink, className) {
        var existing = addLink.closest('div.stream-provider').find('span.add-filter-message'),
            msg = AJS.$('<span class="add-filter-message"></span>')
            .text(message)
            .addClass(className);
        if (existing.length) {
            existing.replaceWith(msg);
        } else {
            msg.insertAfter(addLink);
        }
        fadeOutAndRemove(msg);
    }

    /**
     * Adds a filter to the appropriate provider's smart config; triggered by the "add filter" link
     * @method addFilter
     * @param {Event} e The event object
     */
    function addFilter(e) {
        e.preventDefault();
        var target = AJS.$(e.target),
            key = target.closest('div.stream-provider').attr('data-key');
        try {
            smartConfigs[key].add();
        } catch(e) {
            displayAddFilterMessage(geti18n(e.key), target, 'filter-error');
        }
    }

    /**
     * Creates and returns a text description for available streams
     * @method buildAvailableStreamsDescription
     * @return {HTMLElement} A dom element for the created div
     */
    function buildAvailableStreamsDescription() {
        return AJS.$('<div class="available-streams-description"></div>')
            .text(geti18n('available.streams.description'));
    }

    /**
     * Creates and returns an element for identifying and enabling/disabling a provider
     * @method buildProviderHeader
     * @param {Object} provider An object containing information for the provider in question
     * @param {Boolean} isEnabled Whether of not the provider is currently enabled
     * @return {HTMLElement} A dom element for the created div
     */
    function buildProviderHeader(provider, isEnabled) {
        var div = AJS.$('<div class="stream-provider-header checkbox"></div>'),
            inputName = provider.key + '-enable';
        buildCheckbox(isEnabled, inputName)
            .attr({
                  'id': inputName,
                  'title': isEnabled ? geti18n('stream.disable') : geti18n('stream.enable')
              })
            .addClass('provider-enable')
            .change(toggleProvider)
            .appendTo(div);
        truncateCharacters(AJS.$('<label></label>')
            .attr('for', inputName)
            .attr('title', provider.title || '')
            .text(provider.name)
            .appendTo(div));
        // only show the "Add filter" link if there are filter options to add
        if (provider.options && provider.options.length) {
            AJS.$('<a href="#" class="stream-provider-customize-link"></a>')
                .text('(' + geti18n('add.filter') + ')')
                .click(addFilter)
                .appendTo(div);
        }
        return div;
    }

    /**
     * Creates and returns a an element for configuring a provider
     * @method buildProviderFilters
     * @param {Object} filter An object containing information on available filter options
     * @param {Array} settings An array of objects containing information on defined filter rules
     * @return {HTMLElement} A dom element for the created div
     */
    function buildProviderFilters(filter, settings) {
        var key = filter.key,
            rules = settings ? settings.rules : [],
            isEnabled = !(settings && settings.disabled),
            div = AJS.$('<div class="stream-provider"></div>').attr('data-key', key).toggleClass('provider-disabled', !isEnabled);
        buildProviderHeader(filter, isEnabled).appendTo(div);
        // only create a smart config if there are available options for this filter
        if (filter.options && filter.options.length) {
            smartConfigs[key] = AJS.smartConfig(filter.options, {
                parent: div,
                allowZeroRules: true,
                hidden: !(rules && rules.length),
                addText: geti18n('add'),
                removeText: geti18n('remove'),
                dateRangeSeparatorText: geti18n('date.range.to'),
                rules: rules
            });
        }
        
        return div;
    }

    /**
     * Given a provider filter, finds the filter option (if present) that should be displayed in a provider-like fashion
     * @method getAliasOptionFromFilter
     * @param {Object} filter An object containing information on available filter options
     * @return {Object} The option object containing the provider aliases
     */
    function getAliasOptionFromFilter(filter) {
        var aliasOptionKey = filter.providerAliasOptionKey,
            options = filter.options;
        for (var i = 0, len = options.length; i < len; i++) {
            var option = options[i];
            if (option.key === aliasOptionKey) {
                return option;
            }
        }
        return null;
    }

    /**
     * Builds "provider-style" checkboxes for a specified set of option values.  Used to display 3rd party generators as
     * individual checkboxes instead of as a multi-select
     * @method buildProviderAliasCheckboxes
     * @param {Object} filter An object containing information on available filter options
     * @param {Object} settings An object containing defined filter rules
     * @return {HTMLElement} A dom element for the containing div
     */
    function buildProviderAliasCheckboxes(filter, settings) {
        var key = filter.key,
            name = filter.name,
            applinkName = filter.applinkName,
            alias = getAliasOptionFromFilter(filter),
            enabledCheckboxes = (settings && settings.rules && settings.rules.length && settings.rules[0].value) || [],
            div = AJS.$('<div class="aliased-providers"></div>');
        if (alias) {
            div.attr({
                         'data-key': key,
                         'data-option': alias.key,
                         'data-type': alias.type
                     });
            for (var aliasedProviderKey in alias.values) {
                var aliasedProviderName = alias.values[aliasedProviderKey],
                    isEnabled = !settings || ((settings && !settings.disabled) && (!enabledCheckboxes.length || AJS.$.inArray(aliasedProviderKey, enabledCheckboxes) !== -1)),
                    aliasedProviderDiv = AJS.$('<div class="stream-provider"></div>');
                aliasedProviderName = applinkName ? AJS.format(getMsg('gadget.activity.stream.config.option.name'), aliasedProviderName, applinkName) : aliasedProviderName;
                aliasedProviderDiv.attr({
                                         'data-key': aliasedProviderKey,
                                         'data-name': aliasedProviderName
                                     });
                
                // Construct a link title from the generator ID
                var title = aliasedProviderKey.replace(/@[^@]+$/, "");
                buildProviderHeader({
                                        key: key + '-' + aliasedProviderKey,
                                        name: aliasedProviderName,
                                        title: title
                                    }, isEnabled).appendTo(aliasedProviderDiv);
                aliasedProviderDiv.appendTo(div);
            }
        }

        return div;
    }

    /**
     * Creates and returns a fieldset containing a set of form elements for each available provider
     * @method buildAvailableStreams
     * @param {Array} filters An array of objects containing information on available filter options
     * @param {Array} settings An array of objects containing information on defined filter rules
     * @return {HTMLElement} A dom element for the created fieldset
     */
    function buildAvailableStreams(filters, settings) {
        var fieldset = AJS.$('<fieldset id="streams-available-streams"></fieldset>');
        buildLegend(geti18n('available.streams')).appendTo(fieldset);
        buildAvailableStreamsDescription().appendTo(fieldset);
        for (var i = 0, len = filters.length; i < len; i++) {
            var filter = filters[i],
                key = filter.key;
            if (filter.providerAliasOptionKey) {
                buildProviderAliasCheckboxes(filter, settings[key]).appendTo(fieldset);
            } else {
                buildProviderFilters(filter, settings[key]).appendTo(fieldset);
            }
            allProviders.push(key);
        }
        return fieldset;
    }

    /**
     * Creates and returns a fieldset containing inputs for defining the max results and refresh options
     * @method buildDisplayOptions
     * @param {Number} max An array of objects containing provider and filter information
     * @param {Number|String} refresh An array of objects containing provider and filter information
     * @return {HTMLElement} A dom element for the created fieldset
     */
    function buildDisplayOptions(max, refresh) {
        var fieldset = AJS.$('<fieldset id="streams-display-options"></fieldset>');
        buildLegend(geti18n('display.options')).appendTo(fieldset);
        buildNumOfEntriesFieldGroup(max).appendTo(fieldset);
        buildRefreshFieldGroup(refresh).appendTo(fieldset);
        return fieldset;
    }

    /**
     * Adds the "IS" operator to "update_date", since it's not returned by the filters resource but we support it on the front-end
     * @method addIsOperatorToDateOption
     * @param {Array} options An array containing filter options
     * @return {Object} The same object, with the operator added as necessary
     */
    function addIsOperatorToDateOption(options) {
        for (var i = 0, len = options.length; i < len; i++) {
            if (options[i].type === 'date') {
                // insert the new operator at the beginning of the array
                options[i].operators.splice(0, 0, {
                    key: 'is',
                    name: geti18n('is.operator')
                });
                return options;
            }
        }
        return options;
    }

    /**
     * Given an array of filter providers, returns an array of all the corresponding filters options
     * @method collapseOptions
     * @param {Array} filters An array of filter provider objects
     * @return {Array} An array of filter option objects
     */
    function collapseOptions(filters) {
        var collapsed = [];
        // we have an array of filters, we have to get options from all of them
        for (var i = 0, len = filters.length; i < len; i++) {
            if (filters[i].key === 'streams') {
                filters[i].options = addIsOperatorToDateOption(filters[i].options);
            }
            collapsed = collapsed.concat(filters[i].options);
        }
        return collapsed;
    }

    /**
     * Creates and appends a set of global filters and a set of filters for each available provider
     * @method buildFiltersForMultipleProviders
     * @param {Array} availableFilters An array of filter provider objects
     * @param {Object} definedFilters An object containing information on the defined filters for each provider
     * @param {HTMLElement} container The parent element for the created elements
     */
    function buildFiltersForMultipleProviders(availableFilters, definedFilters, container) {
        buildGlobalFilters(availableFilters[0], definedFilters['streams']).appendTo(container);
        availableFilters.splice(0, 1);
        delete definedFilters['streams'];
        buildAvailableStreams(availableFilters, definedFilters).appendTo(container);
    }

    /**
     * Creates and appends a single set of filters containing both global options and options for the local provider
     * @method buildFiltersForSingleProvider
     * @param {Array} availableFilters An array of filter provider objects
     * @param {Object} definedFilters An object containing information on the defined filters
     * @param {HTMLElement} container The parent element for the created elements
     */
    function buildFiltersForSingleProvider(availableFilters, definedFilters, container) {
        var available = {
                key: 'streams',
                name: '',
                options: []
            },
            defined = {
                provider: 'streams',
                rules: []
            };
        for (var i = 0, ii = availableFilters.length; i < ii; i++) {
            var provider = availableFilters[i],
                providerKey = provider.key,
                options = provider.options;
            for (var j = 0, jj = options.length; j < jj; j++) {
                var option = options[j];
                option.key = providerKey + ' ' + option.key;
                available.options.push(option);
            }
        }
        AJS.$.each(definedFilters, function(provider, definedFilter) {
            var rules = definedFilter.rules;
            for (var i = 0, len = rules.length; i < len; i++) {
                rules[i].rule = provider + ' ' + rules[i].rule;
            }
            defined.rules = defined.rules.concat(rules);
        });
        buildGlobalFilters(available, defined, true).appendTo(container);
    }

    /**
     * Deletes any provider objects that are aliased but have no available aliases
     * @method removeEmptyAliasedProviders
     * @param {Array} filters An array of filter provider objects
     * @return {Array} The resulting array (but note that the original array _will_ be altered)
     */
    function removeEmptyAliasedProviders(filters) {
        for (var i = 0; i < filters.length; i++) {
            var filter = filters[i],
                aliasOptionKey = filter.providerAliasOptionKey;
            if (aliasOptionKey) {
                for (var j = 0, jj = filter.options.length; j < jj; j++) {
                    var option = filter.options[j];
                    if (option.key === aliasOptionKey && (!option.values || AJS.$.isEmptyObject(option.values))) {
                        filters.splice(i--, 1);
                    }
                }
            }
        }
        return filters;
    }

    /**
     * Creates and returns a form with dynamic configuration rows
     * @method buildConfigForm
     * @param {Object} filters Object containing information on what rules, operators, and values are available
     * @param {Array} rules Object containing information on what rules, operators, and values are available
     * @return {HTMLElement} The dom element for the created form
     */
    function buildConfigForm(filters, rules) {
        var form = AJS.$('<form action="" id="config-form" class="aui top-label"></form>'),
            rulesMap = {};
        rules = rules || [];
        for (var i = 0, len = rules.length; i < len; i++) {
            var rule = rules[i];
            rulesMap[rule.provider] = rule;
        }
        // if we have an aliased provider with no aliases, get rid of it so it won't mess up our filters count
        removeEmptyAliasedProviders(filters);
        if (filters.length > 2) {
            buildFiltersForMultipleProviders(filters, rulesMap, form);
        } else {
            buildFiltersForSingleProvider(filters, rulesMap, form);
        }
        buildDisplayOptions(originalSettings['numofentries'], originalSettings['refresh']).appendTo(form);
        buildFormButtons().appendTo(form);
        return form;
    }

    /**
     * Saves the specified Stream title to gadget prefs.  Triggered when the title edit field is blurred
     * @method updateTitle
     * @param {Event} e The event object
     */
    function updateTitle(e) {
        var target = e && e.target ? AJS.$(e.target) : AJS.$('#stream-title-edit'),
            newTitle = target.val();
        AJS.$('#stream-title').text(newTitle);
    }

    /**
     * Creates an input field for the stream title and inserts it into the dom next to the read-only title element
     * @method addEditableTitleField
     * @return {HTMLElement} The title input field that has been inserted into the dom
     */
    function addEditableTitleField(title) {
        var titleElement = AJS.$('#stream-title'),
            input = AJS.$('<input id="stream-title-edit" value="">');
        title = title || AJS.$('#stream-title').text();
        input
            .val(title)
            .insertAfter(titleElement);
        return input;
    }

    /**
     * Hides the entire configuration UI
     * @method hideFiltering
     */
    function hideFiltering() {
        // TODO: STRM-403: Shouldn't have to know how activity-stream.js shows and hides the filtering
        AJS.$('div.activity-stream-container').removeClass('filter-view');
        resize();
    }

    /**
     * Removes defined filters from any disabled providers
     * @method clearDisabledProviders
     * @param {Array} providers An array of provider objects
     */
    function clearDisabledProviders(providers) {
        for (var i = 0, len = providers.length; i < len; i++) {
            var provider = providers[i],
                smartConfig;
            if (provider.disabled) {
                smartConfig = smartConfigs[provider.provider];
                smartConfig && smartConfig.clear();
            }
        }
    }

    /**
     * Takes a string and returns the same string escaped for a jQuery selector (escaped periods and octothorpes)
     * @method escapeForSelector
     * @param {String} text The text to escape
     * @return {String} The text with any '.' or '#' escaped
     */
    function escapeForSelector(text) {
        return text ? text.replace(/\./g, '\\.').replace(/#/g, '\\#').replace(/:/g, '\\:').replace(/@/g, '\\@') : text;
    }

    /**
     * Determines if a given provider is currently enabled
     * @method isProviderEnabled
     * @param {String} key Identifying key for the provider in question
     * @return {Boolean} Whether or not the provider is currently enabled
     */
    function isProviderEnabled(key) {
        if (key === 'streams') {
            return smartConfigs['streams'].getElement().find('div.config-row').length > 0;
        } else {
            return AJS.$('#' + escapeForSelector(key) + '-enable').attr('checked');
        }
    }

    /**
     * Parses and returns the rules from aliased providers (eg the third party generators)
     * @method getRulesFromAliasedProviders
     * @return {Array} An array of rule objects
     */
    function getRulesFromAliasedProviders() {
        var aliasedProviders = AJS.$('div.aliased-providers', '#config-form'),
            rules = [];
        for (var i = 0, len = aliasedProviders.length; i < len; i++) {
            var provider = AJS.$(aliasedProviders[i]),
                checkboxes = provider.find('input.checkbox'),
                values = [],
                providerObj = {
                    provider: provider.attr('data-key'),
                    rules: []
                };
            checkboxes.each(function(index, input) {
                input = AJS.$(input);
                var container;
                if (input.attr('checked')) {
                    values.push(input.closest('div.stream-provider').attr('data-key'));
                }
            });
            if (checkboxes.length && !values.length) {
                // if none of the checkboxes are checked, consider the provider "disabled"
                providerObj.disabled = true;
            } else if (checkboxes.length !== values.length) {
                // if all of the checkboxes are checked we don't need to return any rules, that's covered by the default
                providerObj.rules.push({
                                           rule: provider.attr('data-option'),
                                           operator: 'IS',
                                           value: values,
                                           type: provider.attr('data-type')
                                       });
            }
            rules.push(providerObj);
        }
        return rules;
    }

    /**
     * Gets provider and filter information for all defined smart configs
     * @method getRulesFromSmartConfigs
     * @return {Array} An array of objects containing provider and filter information
     */
    function getRulesFromSmartConfigs() {
        var rules = [],
            collapsedRules,
            providers = {};
        if (allProviders.length) {
            // has remote providers
            AJS.$.each(smartConfigs, function(index, smartConfig) {
                var isEnabled = isProviderEnabled(index),
                    providerObj = {
                        provider: index,
                        rules: isEnabled ? smartConfig.getRules() : []
                    };
                if (!isEnabled) {
                    providerObj.disabled = true;
                }
                rules.push(providerObj);
            });
            rules = rules.concat(getRulesFromAliasedProviders());
        } else {
            // In the case of a single provider, global rules and local provider rules are rolled up together.
            // We need to "unroll" them into separate providers.
            collapsedRules = smartConfigs['streams'].getRules() || [];
            for (var i = 0, len = collapsedRules.length; i < len; i++) {
                var rule = collapsedRules[i],
                    info = rule.rule.split(' '),
                    providerKey = info[0];
                rule.rule = info[1];
                providers[providerKey] = providers[providerKey] || [];
                providers[providerKey].push(rule);
            }
            AJS.$.each(providers, function(providerKey, providerRules) {
                var isEnabled = isProviderEnabled('streams'),
                    providerObj = {
                        provider: providerKey,
                        rules: isEnabled ? providerRules : []
                    };
                if (!isEnabled) {
                    providerObj.disabled = true;
                }
                rules.push(providerObj);
            });
        }
        return rules;
    }

    /**
     * Removes a given item from the given array, directly altering the original array
     * @method removeItemFromArray
     * @param {String} item The item to remove
     * @param {Array} arr The array to remove from
     */
    function removeItemFromArray(item, arr) {
        var index = AJS.$.inArray(item, arr);
        if (index != -1) {
            arr.splice(index, 1);
        }
    }

    /**
     * Transforms an array of rules objects into an object that can be properly parametrized
     * @method getParams
     * @param {Array} providers (optional) Array of provider objects
     * @return {Object} Object suitable for use in AJS.$.param()
     */
    function getParams(providers) {
        var params = {
                maxResults: AJS.$('#numofentries').val()
            },
            map = {},
            // create a copy of the array so we can leave the original in tact
            enabledProviders = allProviders.concat([]);
        providers = providers || getRulesFromSmartConfigs();
        for (var i = 0, ii = providers.length; i < ii; i++) {
            var providerObj = providers[i],
                provider = providerObj.provider,
                rules = providerObj.rules || [];
            if (providerObj.disabled) {
                removeItemFromArray(provider, enabledProviders);
            } else {
                for (var j = 0, jj = rules.length; j < jj; j++) {
                    var ruleObj = rules[j],
                        rule = ruleObj.rule,
                        value = ruleObj.value,
                        operator,
                        type = ruleObj.type,
                        modifiedRuleObj,
                        idString,
                        ruleString,
                        index;
                    if (value) {
                        value = AJS.$.isArray(value) ? value : AJS.$.trim(value);
                        if (value.length) {
                            modifiedRuleObj = ActivityStreams.handleSpecialCases(ruleObj);
                            value = modifiedRuleObj.value;
                            operator = modifiedRuleObj.operator;
                            idString = provider + ' ' + rule + ' ' + operator;
                            ruleString = [rule, operator.toUpperCase(), value].join(' ');
                            index = map[idString];
                            if (index) {
                                // if instance of rule for provider already exists, just append to it
                                params[provider][index - 1] = params[provider][index - 1] + ' ' + value;
                            } else {
                                params[provider] = params[provider] || [];
                                params[provider].push(ruleString);
                                // store 1-base index of rule so we can find it later (1-base so it's easier to test for existence)
                                map[idString] = params[provider].length;
                            }
                        }
                    }
                }
            }
        }
        if (enabledProviders.length < allProviders.length) {
            params['providers'] = enabledProviders.join(' ');
        }
        return params;
    }

    /**
     * Clear any existing validation errors from the UI.
     * @method clearValidationErrors
     */
    function clearValidationErrors() {
        // clear any previous validation errors
        AJS.$('div.activity-stream-validation-error', '#config-form').remove();
    }

    /**
     * Creates and returns a dom element with the specified error message
     * @method buildValidationErrorMessage
     * @param {String} key Key for internationalization
     * @return {HTMLElement} The created error message element
     */
    function buildValidationErrorMessage(key) {
        var msg = getMsg('gadget.activity.stream.error.' + key);
        // Any additional function arguments will be passed to AJS.format
        if (arguments.length > 1) {
            msg = AJS.format(msg, Array.prototype.slice.call(arguments, 1));
        }
        return AJS.$('<div class="activity-stream-validation-error"></div>')
            .text(msg);
    }

    /**
     * Checks that the given value for max results is a number greater than zero
     * @method validateMaxResults
     * @param {Number} maxResults
     * @return {Boolean} Whether or not max results is valid
     */
    function validateMaxResults(maxResults) {
        maxResults = parseInt(maxResults);
        if (isNaN(maxResults) || maxResults < 1) {
            buildValidationErrorMessage('pref.numofentries.number').appendTo(AJS.$('#max-results-container'));
            return false;
        }
        return true;
    }

    /**
     * Checks that any entered date values parse into valid dates
     * @method validateDates
     * @return {Boolean} Whether or not all dates are valid
     */
    function validateDates() {
        var isValid = true,
            dateFormat= ActivityStreams.getDatepickerDefaults().dateFormat;
        AJS.$('input.hasDatePicker:visible', '#config-form').each(function(index, element) {
            element = AJS.$(element);
            var val = element.attr('data-parsed-time');
            if (val && val.toString() === 'NaN') {
                buildValidationErrorMessage('invalid.date', AJS.$.datepicker.formatDate(dateFormat, new Date()))
                    .appendTo(element.closest('div.config-row'));
                isValid = false;
                return false;
            }
        });
        return isValid;
    }

    /**
     * Validates parameters that are about to be saved.  Each validation error is displayed next to its input control.
     * @method validate
     * @param {Object} params Object containing parameters to be validated
     * @returns {Boolean} {@code false} if there is a validation error; otherwise {@code true}
     */
    function validate(params) {
        var isValid = true;
        clearValidationErrors();
        isValid = isValid && validateDates() && validateMaxResults(params.numofentries || params.maxResults);
        resize();
        return isValid;
    }

    /**
     * Called when inputs are changed, gets the current config params and validates them
     * @method validateChanges
     * @param {Event} e The event object
     */
    function validateChanges(e) {
        validate(getParams());
    }

    /**
     * Saves the currently applied rules to the gadget prefs
     * @method save
     */
    function save() {
        var providers = getRulesFromSmartConfigs(),
            maxResults = AJS.$('#numofentries').val(),
            title = AJS.$('#stream-title-edit').val(),
            params = {
                numofentries: maxResults
            };
        if (providers.length) {
            AJS.$.extend(params, getParams(providers));
            setPref('rules', AJS.json.stringify({providers:providers}));
        } else {
            clearPrefs();
        }
        // validate extracted parameters before applying and saving
        if (!applyRules()) {
            return;
        }
        // save the prefs not encapsulated in the rules pref
        setPref('refresh', AJS.$('#refresh-checkbox').is(':checked') ? AJS.$('#refresh-select').val() : 'false');
        setPref('numofentries', maxResults);
        setPref('title', title);
        updateTitle();
        // these two are necessary for Confluence, which has an all-or-nothing approach to saving prefs
        setPref('isConfigured', true);
        setPref('isReallyConfigured', true);
        originalSettings = AJS.$.extend(originalSettings, {
            title: title,
            numofentries: maxResults,
            rules: providers
        });
        hideFiltering();
        clearDisabledProviders(providers);
        AJS.$('#config-form').trigger('saveFilter.streams');
    }

    /**
     * Updates the contents of the stream with the current filtering rules
     * @method applyRules
     * @return {Boolean} Whether or not the update was successful
     */
    function applyRules() {
        var params = getParams();
        // validate parameters before applying
        if (!validate(params)) {
            return false;
        }
        container.trigger('contentUpdate.streams', params);
        resize();
        return true;
    }

    /**
     * Given an array of both local and global rule objects, collapses them into a single rule object
     * @method collapseGlobalAndLocalRules
     * @param {Array} ruleObjs
     * @return {Array}
     */
    function collapseGlobalAndLocalRules(ruleObjs) {
        var rules = [];
        for (var n = 0; n < ruleObjs.length; n++) {
            rules = rules.concat(ruleObjs[n].rules);
        }
        return [{
            provider: 'streams',
            rules: rules
        }];
    }

    /**
     *
     * @method applySettings
     * @param {Object} settings
     */
    function applySettings(settings) {
        var refresh = settings.refresh,
            title = settings.title,
            ruleObjs = settings.rules || {};
        if (!allProviders.length && ruleObjs.length > 1) {
            // no remote providers, but rules for both the local and global provider
            ruleObjs = collapseGlobalAndLocalRules(ruleObjs);
        }
        for (var i = 0, len = ruleObjs.length; i < len; i++) {
            var ruleObj = ruleObjs[i],
                provider = ruleObj.provider,
                smartConfig = smartConfigs[provider];
            smartConfig.apply(ruleObj);
            AJS.$('#' + escapeForSelector(provider) + '-enable').attr('checked', !ruleObj.disabled).trigger('change');
            if (ruleObj.rules && ruleObj.rules.length) {
                smartConfig.show();
            }
        }
        AJS.$('#stream-title').text(title);
        AJS.$('#stream-title-edit').val(title);
        AJS.$('#numofentries').val(settings.numofentries);
        AJS.$('#refresh-checkbox').attr('checked', refresh !== 'false').trigger('change');
        AJS.$('#refresh-select').val(refresh);
    }

    /**
     * Binds event handlers and runs anything else that needs to wait until after the config is attached to the dom
     * @method registerHandlers
     */
    function registerHandlers() {
        // all dom initializations have to take place after the stream is done loading and after the filtering resource
        // request has returned.  so this function shouldn't run the first time it's called.
        if (registerHandlers.shouldRun) {
            AJS.$(':input.update-on-change', '#config-form')
                .on('input', validateChanges);
            AJS.$(document).unbind('contentLoaded.streams');
        } else {
            registerHandlers.shouldRun = true;
        }
    }

    /**
     * Truncate an element's text with elipses if it has more characters than a specified length
     * @method truncateCharacters
     * @param {HTMLElement} $elem The jQuery collection to truncate
     * @param {Number} maxCharacters The maximum characters allowed in the element
     * @return {HTMLElement} The element passed in
     */
    function truncateCharacters($elem, maxCharacters) {
        var text = $elem.text();
        maxCharacters = maxCharacters || maxProviderLabelCharacters || 50;

        // If the total length is greater than allowed length...
        if(text.length > maxCharacters
                // ...and if there is a "word" (no spaces or dashes) longer than our allowed length. Otherwise it will
                // be broken into multiple lines by browser word wrap and there won't be an issue.
                && new RegExp('[^-\\s]{' + maxCharacters + ',}').test(text)) {
            $elem.html(text.substring(0, maxCharacters) + '&#133').attr('title', text);
        }
        return $elem;
    }

    /**
     * Get this party started
     * @method init
     */
    function init() {
        addEditableTitleField(originalSettings.title);
        buildConfigForm(availableFilters, originalSettings.rules).appendTo(container);
        registerHandlers();
        applyRules();
    }

    init();

    AJS.$(document).bind('contentLoaded.streams', registerHandlers);

    return {

    };
};
