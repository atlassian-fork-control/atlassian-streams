/*
 * UX support for authenticating/authorizing OAuth application links.
 * This is a modified copy of the "applinks.public.js" code in UAL 3.6, and
 * will be retained here until Streams can rely on UAL 3.6 being present
 * in all products.  The only changes from that code are as follows:
 * - added link to OAuth Access Tokens page which isn't currently in UAL
 * - renamed ApplinksUtils to ActivityStreamsApplinks
 * - use ActivityStreams.getMsg instead of AJS.I18n.getText
 * - renamed i18n keys
 * - removed some functions that aren't used by Streams
 */
var ActivityStreamsApplinks = ActivityStreamsApplinks || (function() {

    var pendingRequests = {},
        pendingConfirmations = {};
    
    // Provide a well-defined name for the authentication window/tab we create with
    // window.open() - used in integration testing and may be useful in debugging
    var authWindowName = "com_atlassian_applinks_authentication";

    // Copied from AJS - remove when all products are using at least AJS 3.5
    var ESCAPE_HTML_SPECIAL_CHARS = /[&"'<>]/g;

    /**
     * Helper function copied from AJS because it is currently unavailable in some product versions
     */
    function escapeHtml_replacement(specialChar) {
        switch (specialChar) {
            case "<": return "&lt;";
            case ">": return "&gt;";
            case '&': return "&amp;";
            case "'": return "&#39;";
            default : return "&quot;";
        }
    }

    /**
     * Helper function copied from AJS because it is currently unavailable in some product versions
     */
    function escapeHtml(str) {
        return str.replace(ESCAPE_HTML_SPECIAL_CHARS, escapeHtml_replacement);
    }

    /**
     * Event handler that is called by the applinks authorization completion servlet.  It triggers
     * the completion function for any pending authorization request that matches the given applink
     * ID, and also redispatches the event to any other iframes in the current window.
     * @param {Object} eventObject  JQuery event object
     * @param {string} applinkId  application link ID
     * @param {boolean} success  true if the request was approved
     * @param {string} authAdminUri  URI of the "OAuth Access Tokens" page (will be displayed in the
     *   confirmation message)
     * @param {boolean} wasRedispatched  true if the event has been retriggered from another frame
     */
    function onAuthCompletion(eventObject, applinkId, success, authAdminUri, wasRedispatched) {
        if (applinkId in pendingRequests) {
            var request = pendingRequests[applinkId];
            if (success) {
                request.authAdminUri = authAdminUri;
                delete pendingRequests[applinkId];
            }
            completeAuthRequest(request, success);
        }
        if (!wasRedispatched && parent && (parent !== window)) {
            var myWindow = window;
            AJS.$('iframe', parent.document.body).each(function(index, frame) {
                var scope = frame.contentWindow;
                if (scope !== myWindow) {
                    if (scope.AJS && scope.AJS.$) {
                        scope.AJS.$(scope.document).trigger('applinks.auth.completion',
                            [applinkId, success, authAdminUri, true]);
                    }
                }
            });
        }
    }
        
    /**
     * Initialization function to be called once at document ready time.
     */
    function setup() {
        // If we're in an iframe, set up an object in the parent window that we can use to
        // keep track of state even if the iframe is refreshed.
        if (parent && !(parent === window)) {
            parent.ActivityStreamsApplinks = parent.ActivityStreamsApplinks || {};
        }
        
        AJS.$(document).bind('applinks.auth.completion', onAuthCompletion);
        
        checkForPendingConfirmations();
    }

    /**
     * Helper function to get the DOM object of the current iframe.
     * @return {Object} a DOM object, or null if we are not in an iframe
     */
    function getCurrentIframe() {
        if (window === parent.window) {
            return null;
        }
        var ret = null,
            myFrameWindow = window;
        AJS.$('iframe', parent.document.body).each(function(index) {
            if (this.contentWindow.window === myFrameWindow) {
                ret = this;
            }
        });
        return ret;
    }
    
    /**
     * Fires the appropriate event when the authorization flow has completed.  On approval, reloads
     * the window/frame unless an event handler calls {@code preventDefault()} on the event.
     * @param {Object} applinkProperties  has the same properties passed to {@link createAuthRequestBanner}
     * @param {boolean} approved  true if the request was approved
     */
    function completeAuthRequest(applinkProperties, approved) {
        var $scope = AJS.$(document);
        if (approved) {
            // Temporarily bind an event handler so our handler runs after any other handlers that
            // may exist.
            var defaultApprovalHandler = function (eventObject) {
                if (eventObject.isDefaultPrevented()) {
                    // Don't reload, just show the confirmation message
                    showAuthConfirmationBanner(applinkProperties);
                } else {
                    // Reload, but first save a reminder to make us show a confirmation message
                    // after we've reloaded.
                    registerPendingConfirmation(applinkProperties);
                    document.location.reload(true);
                }
            };
            $scope.bind('applinks.auth.approved', defaultApprovalHandler);
            $scope.trigger('applinks.auth.approved', applinkProperties);
            $scope.unbind('applinks.auth.approved', defaultApprovalHandler);
        } else {
            // There's no default behavior for a request that was denied, but fire an event in case
            // anyone is interested.
            $scope.trigger('applinks.auth.denied', applinkProperties);
        }
    }
    
    /**
     * Used internally to make the applink support code aware of a "please authenticate" message
     * element that has been displayed, by wiring the appropriate event handlers and adding the
     * applink's properties to an internal list of authentication requests.
     * @param $element {Object}  a JQuery object
     * @param applinkProperties {Object}  has the same properties passed to {@link createAuthRequestBanner}
     */
    function initAuthRequest($element, applinkProperties) {
        var $authLink = $element.find("a.applink-authenticate");
        
        if ($element.hasClass('aui-message')) {
            // Workaround for incomplete AJS availability in some products
            ActivityStreams.makeMessageCloseable($element);
        }
        
        $authLink.click(function(e) {
            window.open(applinkProperties.authUri, authWindowName);
            e.preventDefault();
        });
        
        pendingRequests[applinkProperties.id] = applinkProperties;
        
        return $element;
    }
    
    /**
     * Builds a "please authenticate" banner (in a standard AUI message box) containing a link that
     * that will start authorization for an application link that needs credentials.
     * <p>
     * On completion of the authorization flow, a JQuery event will be triggered on the document,
     * with the event type "applinks.auth.approved" or "applinks.auth.denied", and an additional
     * parameter equal to the {@code applinkProperties} parameter that was passed here.
     * <p>
     * If authorization is granted (event "applinks.auth.approved"), the default behavior is for the
     * window or frame to be reloaded; also, a confirmation banner will be displayed either within
     * a &lt;div&gt; element of class "applinks-auth-confirmation-container" if one exists, or at the top of
     * the document otherwise.  Reloading of the window/frame can be disabled by having an event
     * handler call {@code preventDefault()} on the event.
     *
     * @param {Object} applinkProperties contains the following application link properties:
     *   {@code id}: the application link identifier;
     *   {@code appName}: the name of the remote application;
     *   {@code appUri}: the base URI of the remote application;
     *   {@code authUri}: the URI for starting the authorization flow
     */
    function createAuthRequestBanner(applinkProperties) {
        var $banner = AJS.$('<li></li>');
        // Note that we can't just use the AJS.messages.warning() function, because it will put a
        // standard warning icon in the message box and we want a custom icon.
        $banner.append(AJS.format(ActivityStreams.getMsg("gadget.activity.stream.auth.request.message"),
                escapeHtml(applinkProperties.authUri),
                escapeHtml(applinkProperties.appUri),
                escapeHtml(applinkProperties.appName)));
        initAuthRequest($banner, applinkProperties);
        return $banner;
    }

    /**
     * Used internally to remember the fact that we have just completed authorizing an
     * applink and are about to refresh the iframe associated with it, so that we can
     * display a confirmation message after the iframe is refreshed.
     */
    function registerPendingConfirmation(applinkProperties) {
        var frame = getCurrentIframe();
        if ((!frame) || (!frame.id)) {
            return;
        }
        if (! parent.ActivityStreamsApplinks.pendingConfirmations) {
            parent.ActivityStreamsApplinks.pendingConfirmations = { };
        }
        if (!(frame.id in parent.ActivityStreamsApplinks.pendingConfirmations)) {
            parent.ActivityStreamsApplinks.pendingConfirmations[frame.id] = [];
        }
        parent.ActivityStreamsApplinks.pendingConfirmations[frame.id].push(applinkProperties);
        return;
    }

    /**
     * Called after a page load, to see if we've been refreshed due to a successful authorization.
     * If we're in an iframe, a variable will have been set on the parent window to tell us that
     * this happened.  If so, insert a confirmation banner at the top of the iframe.
     */
    function checkForPendingConfirmations() {
        if (parent && parent.ActivityStreamsApplinks && parent.ActivityStreamsApplinks.pendingConfirmations) {
            var myFrame = getCurrentIframe();
            if (myFrame) {
                if (myFrame.id in parent.ActivityStreamsApplinks.pendingConfirmations) {
                    var pendingConfirmations = parent.ActivityStreamsApplinks.pendingConfirmations[myFrame.id];
                    delete parent.ActivityStreamsApplinks.pendingConfirmations[myFrame.id];
                    for (var i = 0, n = pendingConfirmations.length; i < n; i++) {
                        showAuthConfirmationBanner(pendingConfirmations[i]);
                    }
                }
            }
        }
    }

    /**
     * Displays a confirmation banner.  If an element exists with the class
     * "applinks-auth-confirmation-contianer", it is inserted there, otherwise at the top of the
     * document.
     */
    function showAuthConfirmationBanner(applinkProperties) {
        var scope = AJS.$(document),
            banner = AJS.$('<div class="aui-message aui-message-success closeable shadowed applinks-auth-confirmation"><span class="aui-icon icon-applinks-key-success"></span></div>'),
            container = scope.find('div.applinks-auth-confirmation-container');
        if (!container.length) {
            container = scope.find('body');
        }
        banner.append(AJS.format(ActivityStreams.getMsg("gadget.activity.stream.auth.confirmation"),
                escapeHtml(applinkProperties.appUri),
                escapeHtml(applinkProperties.appName),
                escapeHtml(applinkProperties.authAdminUri)));
        ActivityStreams.makeMessageCloseable(banner);
        container.prepend(banner);
        setTimeout(function() {
            banner.fadeOut(1000, function() {
                AJS.$(this).remove();
            });
        }, 5000);
    }

    AJS.$(document).ready(setup);
    
    return {      
        createAuthRequestBanner: createAuthRequestBanner
    };
})();
