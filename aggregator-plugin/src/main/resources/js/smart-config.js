var AJS = AJS || {};

/**
 * Creates a dynamic configuration UI where you can add and remove rules for filtering
 * @method smartConfig
 * @param {Array} rules Object containing available rule/operator/value filtering options
 * @param {Object} originalSettings
 * @return {Object} Smart config object
 */
AJS.smartConfig = function(rules, originalSettings) {
    var ruleTypeExclusions = [],
        rulesMap = {},
        container,
        defaultSettings = {
            parent: AJS.$(document),
            addText: 'Add',
            removeText: 'Remove',
            dateRangeSeparatorText: 'to',
            allowZeroRules: false,
            hidden: false
        },
        chosenConfig = {
            defaultText: ActivityStreams.getMsg('gadget.activity.stream.config.chosen.default.placeholder'),
            noResultsText: ActivityStreams.getMsg('gadget.activity.stream.config.chosen.default.noresults')
        };

    originalSettings = AJS.$.extend(defaultSettings, originalSettings);

    /**
     * Returns the first rule in the list of available rule types
     * @method getDefaultRule
     * @return {String} The name of the "default" rule
     */
    function getDefaultRule() {
        var index = 0,
            rule = rules[index];
        // don't return a hidden/disabled rule
        while (AJS.$.inArray(rule.key, ruleTypeExclusions) > -1) {
            rule = rules[++index];
        }
        return rule;
    }

    /**
     * Returns the first operator in the list of available operators for a given rule, or for the defaul rule if none is specified
     * @method getDefaultOperator
     * @param {String} rule (optional) The rule to get a operator for
     * @return {String} The name of the "default" operator
     */
    function getDefaultOperator(rule) {
        rule = rule || getDefaultRule();
        if (typeof(rule) === 'string') {
            rule = rulesMap[rule];
        }
        return rule.operators[0];
    }

    /**
     * Takes a string and returns the same string escaped for a jQuery selector (escaped periods and octothorpes)
     * @method escapeForSelector
     * @param {String} text The text to escape
     * @return {String} The text with any '.' or '#' escaped
     */
    function escapeForSelector(text) {
        return text ? escapeForClassname(text).replace(/\./g, '\\.').replace(/#/g, '\\#').replace(/:/g, '\\:') : text;
    }

    /**
     * Takes a string and returns the same string with spaces replaced with underscores, so that it can be used in classnames and the like
     * @method escapeForClassname
     * @param {String} str The string to escape
     * @return {String} The text with any space (\s) characters replaced with undescores(_)
     */
    function escapeForClassname(str) {
        return str.replace(/\s/g, '_');
    }

    /**
     * Hides/disables any non-selected options for a given rule.  Used to ensure that unique rules are only selected once.
     * @method hideUniqueRuleOptions
     * @param {String} rule (optional) The rule to hide options for
     */
    function hideUniqueRuleOptions(rule) {
        AJS.$('select.rule option.' + escapeForSelector(rule), container)
            .not(':selected')
            .addClass('hidden disabled')
            .attr('disabled', true);
    }

    /**
     * Shows/enables options for a given rule.  Used when unique rules are un-selected or removed.
     * @method showRuleOptions
     * @param {String} rule (optional) The rule to show options for
     */
    function showRuleOptions(rule) {
        ruleTypeExclusions.splice(AJS.$.inArray(rule, ruleTypeExclusions), 1);
        AJS.$('select.rule option.' + escapeForSelector(rule), container)
            .removeClass('hidden disabled')
            .removeAttr('disabled');
    }

    /**
     * Checks to see if a rule is specified as unique, and if so adds it to the list of rule exclusions
     * @method checkForUniqueness
     * @param {String} rule The rule to check uniqueness for
     * @param {HTMLElement} select The select element that changed
     */
    function checkForUniqueness(rule, select) {
        rule = rule.key ? rule : rulesMap[rule];
        var key = rule.key;
        if (rule.unique) {
            ruleTypeExclusions.push(key);
            hideUniqueRuleOptions(key);
            select.bind('change.' + escapeForClassname(key), function(e) {
                showRuleOptions(key);
                select.unbind('change.' + escapeForClassname(key));
            });
        }
    }

    /**
     * Creates and returns an option element for a select
     * @method buildSelectOption
     * @param {String} value The value of the option
     * @param {String} label The text label of the option
     * @param {String} classname (optional) A classname to be applied to the element
     * @return {HTMLElement} The dom element for the created option
     */
    function buildSelectOption(value, label, classname) {
        classname = escapeForClassname(classname || value);
        return AJS.$('<option value=""></option>')
            .addClass(classname)
            .val(value)
            .text(label);
    }

    /**
     * Creates and returns a select element and corresponding option elements
     * @method buildSelect
     * @param {Array} values An array of values to populate the select with
     * @param {String} selectedValue The value that should be initially selected
     * @param {String} namespace Used to retrieve i18n labels and added as a classname to the select
     * @param {String} elementName (optional) What to use for the "name" attribute
     * @return {HTMLElement} The dom element for the created select
     */
    function buildSelect(values, selectedValue, namespace, elementName) {
        var select = AJS.$('<select></select>').addClass(namespace);
        selectedValue = selectedValue ? selectedValue.key || selectedValue : selectedValue;
        if (elementName) {
            select.attr('name', elementName.replace(/\s/, '-'));
        }
        if (AJS.$.isArray(values)) {
            for (var i = 0, len = values.length; i < len; i++) {
                var value = values[i],
                    label,
                    option;

                // value should be an object, with 'value' and 'label' properties
                label = value.name;
                value = value.key;

                option = buildSelectOption(value, label);
                if (value == selectedValue) {
                    option.attr('selected', 'selected');
                }
                option.appendTo(select);
            }
        } else {
            for (var value in values) {
                var label = values[value],
                    option;
                option = buildSelectOption(value, label);
                if (value == selectedValue || label == selectedValue) {
                    option.attr('selected', 'selected');
                }
                option.appendTo(select);
            }
        }
        return select;
    }

    /**
     * Creates and returns a input element of type "text"
     * @method buildTextField
     * @param {String} startingValue (optional) The initial value of the text field
     * @param {String} name (optional) What to use for the "name" attribute
     * @param {String} titleText (optional) Additional info to be displayed as a tooltip
     * @return {HTMLElement} The dom element for the created text input
     */
    function buildTextField(startingValue, name, titleText) {
        buildTextField.input = buildTextField.input || AJS.$('<input class="text" type="text" value="">');
        var input = buildTextField.input.clone();
        if (startingValue) {
            input.val(startingValue);
        }
        if (name) {
            input.attr('name', name.replace(/\s/, '-'));
        }
        if (titleText) {
            input.attr('title', titleText);
        }
        return input;
    }

    /**
     * Creates and returns a multiple select element and corresponding option elements
     * @method buildMultipleSelect
     * @param {Array} values An array of values to populate the select with
     * @param {Array} selectedValues The values that should be initially selected. If unspecified, all will be deselected
     * @param {String} namespace Used to retrieve i18n labels and added as a classname to the select
     * @param {String} elementName (optional) What to use for the "name" attribute
     * @param {String} titleText (optional) Additional info to be displayed as a tooltip
     * @return {HTMLElement} The dom element for the created multiple select
     */
    function buildMultipleSelect(values, selectedValues, namespace, elementName, titleText) {
        var select = buildSelect(values, '', namespace, elementName)
            .attr('multiple', 'multiple')
            .addClass('multi-select')
            .attr('rows', 5),
            options = AJS.$('option', select);
        if (selectedValues) {
            if (typeof selectedValues === 'string') {
                selectedValues = [selectedValues];
            }
            options.filter(':first').removeAttr('selected');
            for (var i = 0, len = selectedValues.length; i < len; i++) {
                options.filter('[value="' + selectedValues[i] + '"]').attr('selected', 'selected');
            }
        } else {
            //if no selectedValues are specified then let's deselect everything by default
            for (var i = 0, len = options.length; i < len; i++) {
                options[i].selected = false;
            }
        }
        if (titleText) {
            select.attr('title', titleText);
        }
        return select;
    }

    /**
     * Updates the user input when a corresponding operator or rule is changed
     * @method updateUserInput
     * @param {String} rule Currently selected rule
     * @param {String} operator Currently selected operator
     * @param {HTMLElement} row The configuration row being modified
     * @param {Boolean} persistValue Whether to persist the currently selected value(s) of the input to the new input
     */
    function updateUserInput(rule, operator, row, persistValue) {
        var existing = row.find(':input.user-input'),
            input = buildUserInputs(rule, operator.key ? operator.key : operator, persistValue ? existing.val() : ''),
            dateTime = persistValue && existing.attr('data-parsed-time');

        existing.unchosen().datepicker('destroy');

        if (existing.length > 1) {
            // when we replace date range there will be two existing inputs and some joining text
            existing.not(':first').remove();
            existing.nextAll('span.date-range-joiner').remove();
            existing.filter(':first').replaceWith(input);
        } else {
            existing.replaceWith(input);
        }

        applyChosen(input);

        if (!persistValue) {
            // if we're updating the input but not persisting the value, we need to update the preview and focus the input
            input.trigger('change');
            input.focus();
        } else if (input.val()) {
            if (dateTime) {
                input.attr('data-parsed-time', dateTime);
            }
            // if the input already has a value, update the preview
            input.trigger('change');
        } else {
            // otherwise, focus the input so the user can easily add a value
            input.focus();
        }
    }

    /**
     * Creates and returns a select element populated with operators based on the given rule
     * @method buildOperatorsDropdown
     * @param {String} rule Currently selected rule
     * @param {String} selectedOperator Operator which should be initially selected
     * @return {HTMLElement} The dom element for the created dropdown
     */
    function buildOperatorsDropdown(rule, selectedOperator) {
        if (typeof rule === 'string') {
            rule = rulesMap[rule];
        }
        var operators = rule.operators;
        return buildSelect(operators, selectedOperator, 'operator').change(function(e) {
            var target = AJS.$(e.target),
                row = target.closest('div.config-row'),
                rule = row.find('select.rule').val(),
                operator = target.val();
            updateUserInput(rule, operator, row, true);
        });
    }

    /**
     * Updates the operators dropdown when a corresponding rule is changed
     * @method updateOperators
     * @param {String} rule Currently selected rule
     * @param {HTMLElement} row The configuration row being modified
     */
    function updateOperators(rule, row) {
        row.find('select.operator').replaceWith(buildOperatorsDropdown(rule));
        updateUserInput(rule, getDefaultOperator(rule), row);
    }

    /**
     * Creates and returns a select element populated with the available rule types
     * @method buildRulesDropdown
     * @param {String} selectedRule Rule which should be initially selected
     * @return {HTMLElement} The dom element for the created dropdown
     */
    function buildRulesDropdown(selectedRule) {
        var select = buildSelect(rules, selectedRule, 'rule').change(function(e) {
            var target = AJS.$(e.target),
                rule = target.val(),
                row = target.closest('div.config-row').addClass(rule.key);
                updateOperators(rule, row);
                checkForUniqueness(rule, target);
        });
        for (var i = 0, len = ruleTypeExclusions.length; i < len; i++) {
            AJS.$('option.' + escapeForSelector(ruleTypeExclusions[i]), select).addClass('hidden disabled').attr('disabled', true);
        }
        checkForUniqueness(select.find('option:selected').val(), select);
        return select;
    }

    /**
     * Creates and returns an element with "add" and "remove" buttons for adding and removing config rows
     * @method buildAddRemoveLinks
     * @return {HTMLElement} The dom element for the created buttons
     */
    function buildAddRemoveLinks() {
        var links = AJS.$('<span class="add-remove-buttons"></span>');
        AJS.$('<button class="add" type="button"></button>')
            .attr('title', originalSettings.addText)
            .append(AJS.$('<span></span>').text(originalSettings.addText))
            .click(addRow)
            .appendTo(links);
        AJS.$('<button class="remove" type="button"></button>')
            .attr('title', originalSettings.removeText)
            .append(AJS.$('<span></span>').text(originalSettings.removeText))
            .click(removeRow)
            .appendTo(links);
        return links;
    }

    /**
     * Initializes the given input so that a datepicker will be shown when focused
     * @method initializeDatepicker
     * @param {HTMLElement} input The input element to associate a datepicker with
     * @return {HTMLElement} The same dom element that was passed in
     */
    function initializeDatepicker(input) {
        input.datepicker({
            onSelect: function() {
                getValueFromDateInput(input);
                input.trigger('change');
            }
        });

        // if the user enters a non-standard date string that date.js is capable of parsing, we want
        // to convert it to the standard format for that locale
        input.bind('change', function(e) {
            // don't update the input if it's still focused
            if (e.target !== document.activeElement) {
                formatTimestampIntoDate(AJS.$(e.target));
            }
        });

        // if the user starts typing in the input, hide the datepicker
        input.bind('keyup', function(e) {
            AJS.$(e.target).datepicker('hide');
        });

        return input;
    }

    /**
     * Creates and returns an input element or group of input elements based on the given rule and operator
     * @method buildUserInputs
     * @param {String} rule Currently selected rule
     * @param {String} operator Currently selected operator
     * @param {String|Array|Object} currentValue Initial value or values of the input(s)
     * @return {HTMLElement} The dom element for the created input or group of inputs
     */
    function buildUserInputs(rule, operator, currentValue) {
        var container,
            endDate,
            startValue = currentValue,
            endValue;
        if (operator !== 'between') {
            return buildUserValueInput(rule, operator, currentValue);
        } else {
            container = AJS.$('<span class="date-range-container"></span>');
            if (currentValue.indexOf(' ') != -1) {
                currentValue = currentValue.split(' ');
                startValue = currentValue[0];
                endValue = currentValue[1];
            }
            endDate = buildUserValueInput(rule, operator, endValue).addClass('date-range');
            if (startValue) {
                endDate.datepicker('option', 'defaultDate', startValue);
            }
            buildUserValueInput(rule, operator, startValue)
                .addClass('date-range')
                .datepicker('option', 'onClose', function(dateText, inst) {
                    // using the 'onSelect' event doesn't work because the next datepicker is shown and then immediately
                    // hidden.  so we use onClose, but test to make sure a value was actually selected
                    if (dateText) {
                        endDate.datepicker('option', 'defaultDate', dateText);
                        if (!endDate.val()) {
                            // if the endDate has no current value, pop up its datepicker
                            endDate.datepicker('show');
                        }
                    }
                })
                .appendTo(container);
            AJS.$('<span class="date-range-joiner"></span>').text(originalSettings.dateRangeSeparatorText).appendTo(container);
            endDate.appendTo(container);
            return container;
        }
    }

    /**
     * Formats the stored timestamp for a date input into a localized date string
     * @method formatTimestampIntoDate
     * @param {HTMLElement} input The date input to format
     * @param {Number|String} timestamp (optional) Timestamp to set input to, in millis since epoch
     * @return {HTMLElement} The input element
     */
    function formatTimestampIntoDate(input, timestamp) {
        var date,
            format = ActivityStreams.getDatepickerDefaults().dateFormat;
        if (timestamp) {
            input.attr('data-parsed-time', timestamp);
        } else {
            timestamp = getValueFromDateInput(input);
        }
        timestamp = parseInt(timestamp);
        if (timestamp.toString() != 'NaN') {
            date = AJS.$.datepicker.formatDate(format, new Date(timestamp));
            input.val(date);
        }
        return input;
    }

    /**
     * Creates and returns an input element or multiple select based on the given rule and operator
     * @method buildUserValueInput
     * @param {String} rule Currently selected rule
     * @param {String} operator Currently selected operator
     * @param {String|Array} currentValue Initial value or values of the input
     * @return {HTMLElement} The dom element for the created input or multiple select
     */
    function buildUserValueInput(rule, operator, currentValue) {
        var value,
            input,
            elementName;
        if (typeof(rule) === 'string') {
            rule = rulesMap[rule];
        }
        value = rule.values;
        elementName = rule.key + '-' + operator;
        if (rule.type == 'select') {
            input = buildMultipleSelect(value, currentValue, rule, elementName, rule.helpText).removeClass(rule);
        } else {
            // no value specified, assume it should be user-provided
            input = buildTextField(currentValue, elementName, rule.helpText);
        }
        input.attr('data-rule', rule.key).attr('data-operator', operator);
        if (rule.type === 'date') {
            // if the current value is in millis since epoch, we need to format it into something human-readable
            if (currentValue && typeof(currentValue) === 'number') {
                formatTimestampIntoDate(input, currentValue);
            }
            initializeDatepicker(input);
        }

        return input.addClass('update-on-change user-input');
    }

    /**
     * Creates and returns a "clearing" div for use in clearing floats
     * @method buildClearer
     * @return {HTMLElement} The created div
     */
    function buildClearer() {
        return AJS.$('<div class="clearer"></div>');
    }

    /**
     * Creates and returns a configuration row, with rule dropdown, operator dropdown, user input, and add/remove buttons
     * @method buildConfigRow
     * @param {String} rule Initially selected rule
     * @param {String} operator Initially selected operator
     * @param {String|Array} value Initial value or values of the input
     * @return {HTMLElement} The dom element for the created configuration row
     */
    function buildConfigRow(rule, operator, value) {
        var row = AJS.$('<div class="config-row"></div>'),
            input;
        buildRulesDropdown(rule).appendTo(row);
        buildOperatorsDropdown(rule, operator).appendTo(row);
        input = buildUserInputs(rule, operator.key || operator, value).appendTo(row);
        buildAddRemoveLinks().appendTo(row);
        buildClearer().appendTo(row);
        applyChosen(input);
        return row;
    }

    /**
     * Creates and returns the default configuration row
     * @method buildDefaultRow
     * @return {HTMLElement} The dom element for the created configuration row
     */
    function buildDefaultRow() {
        var rule = getDefaultRule();
        return buildConfigRow(rule, getDefaultOperator(rule), '');
    }

    /**
     * Creates and returns a div with a row for each specified rule, or with a single default row if no rules are specified
     * @method buildSmartConfig
     * @param {Object} settings Object containing information on what rules (if any) to build config rows for
     * @return {HTMLElement} The created dom element
     */
    function buildSmartConfig(settings) {
        var rules = AJS.$.grep(settings.rules || [], function(rule) {
                return rulesMap[rule.rule];
            }),
            div = AJS.$('<div class="smart-config"></div>').toggleClass('hidden', settings.hidden);
        // make sure we're starting without any exclusions
        ruleTypeExclusions = [];
        if (!rules.length && !settings.allowZeroRules) {
            // if no rules are defined, and we can't have zero rules, add a default rule
            div.append(buildDefaultRow());
        } else {
            for (var i = 0, len = rules.length; i < len; i++) {
                var rule = rules[i];
                buildConfigRow(rule.rule, rule.operator, rule.value).appendTo(div);
            }
        }
        return div;
    }

    /**
     * Checks the current number of configuration rows and enables/disables remove buttons as appropriate
     * @method checkConfigRowCount
     */
    function checkConfigRowCount() {
        var rows = container.find('div.config-row'),
            addButtons = rows.find('button.add'),
            removeButtons = rows.find('button.remove'),
            canBeZero = originalSettings.allowZeroRules;
        if (canBeZero && rows.length === 0) {
            container.addClass('hidden');
        } else {
            if (rows.length == 1 && !canBeZero) {
                removeButtons
                    .attr('disabled', 'disabled')
                    .addClass('disabled');
            } else {
                removeButtons
                    .removeAttr('disabled')
                    .removeClass('disabled');
            }
            if (ruleTypeExclusions.length === rules.length) {
                addButtons
                    .attr('disabled', 'disabled')
                    .addClass('disabled');
            } else {
                addButtons
                    .removeAttr('disabled')
                    .removeClass('disabled');
            }
        }
    }

    /**
     * Adds Chosen autocompleter to a multi select, or an element containg a multi select
     * @method applyChosen
     * @param {HTMLElement} element The multi select, or element containing a multi select
     * @param {String} selector The CSS selector to search the element for
     * @return {Boolean} Whether or not chosen was applied to a matched element
     */
    function applyChosen(element, selector) {
        var finder = selector || 'select.multi-select';

        if (!element.is(finder)) {
            element = element.find(finder);
        }
        if (element.length) {
            element
                .chosen(chosenConfig)
                .data('chosen')
                .bind('show', function() {
                    element.parents('.config-row').addClass('chosen-dropdown');
                }).bind('hide', function() {
                    element.parents('.config-row').removeClass('chosen-dropdown');
                });
            return true;
        }
        return false;
    }

    /**
     * Adds a new config row to the smart config fieldset.  Triggered by clicking a config row's "Add" button
     * @method addRow
     */
    function addRow(e) {
        var row = buildDefaultRow();
        if (e && e.preventDefault) {
            e.preventDefault();
            AJS.$(e.target).closest('div.config-row').after(row);
        } else {
            container.append(row);
        }
        checkConfigRowCount();
        row.find('.user-input:first').focus();
        row.trigger('rowAdded.smartConfig');
        showContainer();
        resize();
    }

    /**
     * Removes a config row from the smart config fieldset.  Triggered by clicking a config row's "Remove" button
     * @method addRow
     */
    function removeRow(e) {
        e.preventDefault();
        var row = AJS.$(e.target).closest('div.config-row'),
            select = row.find('select.rule'),
            rule = select.find('option:selected').val(),
            container = row.closest('div.smart-config');
        container.unchosen();
        select.trigger('change.' + escapeForClassname(rule));
        row.remove();
        checkConfigRowCount();
        container.trigger('rowRemoved.smartConfig');
        resize();
    }

    /**
     * Removes all removeable config rows; if any row cannot be removed, the input value is cleared
     * @method clearRows
     */
    function clearRows() {
        AJS.$('div.config-row', container).each(function(index, el) {
            var row = AJS.$(el),
                removeButton = row.find('button.remove'),
                input = row.find('.user-input');
            if (!removeButton.hasClass('disabled')) {
                removeButton.click();
            } else {
                input.val('');
            }
        });
    }

    /**
     * Given a date input element, returns the entered date in millis since epoch format
     * @method getValueFromDateInput
     * @param {HTMLElement} input The input element in question
     * @return {Number} The date in millis since epoch
     */
    function getValueFromDateInput(input) {
        var value = input.val(),
            prevTime = input.attr('data-parsed-time'),
            date;
        if (value) {
            date = Date.parse(value);
            value = (date && date.getTime) ? date.getTime() : NaN;
            if (prevTime != value) {
                input.attr('data-parsed-time', value);
            }
        } else {
            input.removeAttr('data-parsed-time');
        }
        return value;
    }

    /**
     * Returns the currently configured rules as an array of objects
     * @method getCurrentRulesArray
     */
    function getCurrentRulesArray() {
        return AJS.$.map( AJS.$("div.config-row", container), function(row, n) {
            var inputs = AJS.$(row).find(':input.user-input'),
                el = inputs.filter(':first'),
                rule = el.attr('data-rule'),
                operator = el.attr('data-operator'),
                value = el.val(),
                type;
            type = rulesMap[rule].type;
            if (type === 'date') {
                value = getValueFromDateInput(el);
            }
            if (inputs.length > 1) {
                value = [];
                for (var i = 0; i < inputs.length; i++) {
                    var input = inputs.filter(':nth(' + i + ')'),
                        inputVal = (type === 'date') ? getValueFromDateInput(input) : input.val();
                    value.push(inputVal);
                }
                value = value.join(' ');
            }
            if (!value) {
                return;
            }
            return {
                rule: rule,
                operator: operator,
                value: value,
                type: type
            };
        } );
    }

    /**
     * Updates the smart config to represent the specified options
     * @method applyOptions
     * @param {Object} options The options to update the config with
     */
    function applyOptions(options) {
        var newElement;
        options = AJS.$.extend(originalSettings, options);
        newElement = buildSmartConfig(options);
        container.replaceWith(newElement);
        container = newElement;
        checkConfigRowCount();
    }

    /**
     * Toggles the visibility of the smart config
     * @method toggleContainer
     * @param {Boolean} hide (optional) Whether the container should be hidden or shown
     */
    function toggleContainer(hide) {
        container.toggleClass('hidden', hide);
    }

    /**
     * Hides the smart config
     * @method hideContainer
     */
    function hideContainer() {
        toggleContainer(true);
    }

    /**
     * Shows the smart config
     * @method showContainer
     */
    function showContainer() {
        // don't show the container if there's no defined rules
        if (AJS.$('div.config-row', container).length) {
            toggleContainer(false);
        }
    }

    /**
     * Triggers an event declaring that the content has been resized
     * @method resize
     */
    function resize() {
        container.trigger('contentResize.smartConfig');
    }

    /**
     * Throws an object that provides an i18n key for error handling
     * and will bubble up the specified message to the console if not caught
     * @method throwError
     * @param {String} key A key identifying the specific error than can be used to generate i18n text
     * @param {String} message A message that will be printed to the console if the exception is not caught
     */
    function throwError(key, message) {
        throw {
            key: key,
            message: message,
            toString: function() {
                return 'Smart Config: ' + message;
            }
        };
    }

    /**
     * Parses the data as necessary and invokes the building of dom elements
     * @method init
     */
    function init() {
        var originalRules = rules.slice(0); // create a copy of the array
        for (var j = 0, jj = originalRules.length; j < jj; j++) {
            var rule = originalRules[j];
            if (rule.type === 'select' && (!rule.values || AJS.$.isEmptyObject(rule.values))) {
                // if a rule is a select but has no valid values, remove it from the rules array
                rules.splice(j, 1);
            } else {
                rulesMap[rule.key] = rule;
            }
        }
        container = buildSmartConfig(originalSettings).appendTo(originalSettings.parent);
        checkConfigRowCount();
    }

    init();

    // if we load in a new date localization file, we need to update any existing date pickers with the correct localization
    AJS.$(document).bind('dateLocalizationLoaded.streams', function() {
        AJS.$('input.hasDatepicker', '#config-form').each(function(index, input) {
            input = AJS.$(input);
            formatTimestampIntoDate(input, input.attr('data-parsed-time'));
        });
    });

    return {
        apply: applyOptions,
        getRules: getCurrentRulesArray,
        getElement: function() {
            return container;
        },
        hide: hideContainer,
        show: showContainer,
        toggle: toggleContainer,
        add: function() {
            if (ruleTypeExclusions.length != rules.length) {
                addRow();
            } else {
                throwError('smart.config.max.rules.reached', 'The maximum number of rules have already been added.');
            }
        },
        clear: clearRows
    };
};
