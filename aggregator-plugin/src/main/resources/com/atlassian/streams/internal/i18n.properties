streams.and=and
streams.authors.unknown=Anonymous User
streams.authors.and=and
streams.authors.unknown.lowercase=an anonymous user
streams.authors.unknown.capitalized=An anonymous user

streams.title.commented={0} commented
streams.title.commented.on={0} commented on {1}

streams.activity.summary.read.more=Read more

streams.activity.object.with.summary={0} - {1}

streams.feed.title.default=Activity Streams
streams.feed.author.default=Activity Streams

portlet.activityfeed.name=Activity Stream
portlet.activityfeed.description=Lists recent activity in a single project, or in all projects.
project.panel.activityfeed.description=List recent activity for the project in Jira, Confluence, FishEye, and Crucible.
activityfeed.project-tab.name=Activity Stream

user.profile.stream.panel.title=Activity Stream

portlet.loadingFeed=Loading...

activityfeed.wiki-project-tab.name=Wiki
activityfeed.wiki-project-tab.description=Shows updates to this project from the <a href="{0}">Wiki</a>
activityfeed.wiki-project-tab.title=Related Wiki Activity

activityfeed.wiki-issue-tab.name=Wiki
activityfeed.wiki-issue-tab.description=Shows updates from the <a href="{0}">Wiki</a> on pages that refer to this issue
activityfeed.wiki-issue-tab.title=Related Wiki Activity

portlet.activityfeed.title=<h2>Activity Stream: <span class="streamFilterName">{0}</span></h2>
portlet.activityfeed.feedicon.title=Atom Feed
activityfeed.configicon.title=Configure
activityfeed.show.more=Show more
portlet.activityfeed.timestamp={0,date,MMMM d - ha}

portlet.activityfeed.field.projectid.name=Project
portlet.activityfeed.field.projectid.description=Select a project to display activity for
portlet.activityfeed.field.numofentries.name=Number of Entries
portlet.activityfeed.field.numofentries.description=The maximum number of entries to display in the activity stream (cannot be larger than 100)

## User Activity Stream

stream.userstream.name=User Activity Stream
stream.userstream.description=Lists recent activity by a single user.
stream.userstream.field.username.name=Username
stream.userstream.field.username.description=The username of the person to display activity for
stream.userstream.field.username.blank=No username was configured.  Showing all users.
stream.userstream.allusers=All Users

# 0=default number of entries
portlet.activityfeed.warning.nonumofentries=The configured number of entries is missing.  Displaying {0} entries.
# 0=max entries
portlet.activityfeed.warning.maxentries=The configured number of entries is too high.  Displaying no more than {0} entries.


# Activity Streams contain a list of Activity Items

# Activity Item titles have a person, an action and an affected object
# args: 0=people, 1=action, 2=object
stream.item.title={0} {1} {2}


# args: 0=first person's name, 1=second person's name
stream.item.title.people.two={0} and {1}
# args: 0=first person's name, 2=# of other people 
stream.item.title.people.multiple={0} and {1} others

#
# Errors
#
stream.error.unexpected.error=An unexpected error occurred.  Please refer to the logs for more information.
stream.error.unexpected.rendering.error=Sorry, we cannot load this activity due to an error.
stream.item.error.title=There {0,choice,1#was an error|1<were {0} errors} gathering activity:
stream.item.error.author=Atlassian Activity Stream
portlet.activityfeed.error.deletedProject=Project has been deleted

# Gadget
gadget.activity.stream.title=Activity Stream
gadget.activity.stream.description=Lists recent activity in a single project, or in all projects.
gadget.activity.stream.pref.title=Title
gadget.activity.stream.pref.title.description=Title for this activity stream.
gadget.activity.stream.pref.username=Username
gadget.activity.stream.pref.username.description=A space-separated list of usernames of people to display activity for. Leave blank to display activity for all users.
gadget.activity.stream.pref.numofentries=Number of Entries
gadget.activity.stream.pref.numofentries.description=The maximum number of entries to display in the activity stream (cannot be larger than 100)
gadget.activity.stream.pref.keys=Projects
gadget.activity.stream.pref.keys.description=Select which projects to display activity for

gadget.activity.stream.error.pref.keys=Invalid project key specified
gadget.activity.stream.error.pref.title=Title is a required field
gadget.activity.stream.error.pref.numofentries.required=Item limit is a required field
gadget.activity.stream.error.pref.numofentries.number=Item limit must be a number between 1 and 99

gadget.activity.stream.error.loading.filters=An error occurred while loading the available filters.
gadget.activity.stream.error.loading.feed=An error occurred while trying to retrieve recent activity.
gadget.activity.stream.error.timeout=The activity source for '{1}' was slow to respond and isn't included.
gadget.activity.stream.error.timeout.plural={0} activity sources ({1}) were slow to respond and aren't included.
gadget.activity.stream.error.banned=The activity source for '{1}' has previously been slow to respond and wasn't queried.
gadget.activity.stream.error.banned.plural={0} activity sources ({1}) have previously been slow to respond and weren't queried.
gadget.activity.stream.error.invalid.date=The date entered is invalid. The preferred format is "{0}".

gadget.activity.stream.feed=Feed
gadget.activity.stream.add=Add
gadget.activity.stream.cancel=Cancel
gadget.activity.stream.no.activity.found=No activity was found
gadget.activity.stream.hide=Hide
gadget.activity.stream.add.comment=Add comment
gadget.activity.stream.show.more=Show more...
gadget.activity.stream.full.view=Full view
gadget.activity.stream.list.view=List view
gadget.activity.stream.filter=Filter

# Filter Options
streams.filter.option.title=Title
streams.filter.option.summary=Summary
streams.filter.option.update.date=Update Date
streams.filter.option.author=Username
streams.filter.option.help.author=A space-separated list of usernames.
streams.filter.option.activity=Activity
streams.filter.option.help.activity=Select one or more types of activity.
streams.filter.option.project.jira=Project
streams.filter.option.help.project.jira=Select one or more projects.
streams.filter.option.project.confluence=Space
streams.filter.option.help.project.confluence=Select one or more spaces.
streams.filter.option.project.bamboo=Plan
streams.filter.option.help.project.bamboo=Select one or more plans.
streams.filter.option.project.fisheye=Project/Repository
streams.filter.option.help.project.fisheye=Select one or more projects/repositories.
streams.filter.option.issueKey=Jira Issue Key
streams.filter.option.help.issueKey=A space-separated list of issue keys.
streams.filter.option.entry.text=Entry Text
streams.filter.operator.is=is
streams.filter.operator.not=is not
streams.filter.operator.contains=contains
streams.filter.operator.does.not.contain=does not contain
streams.filter.operator.before=is before
streams.filter.operator.after=is after
streams.filter.operator.between=is between

# Filtering UI
gadget.activity.stream.config.global.filters=Global Filters
gadget.activity.stream.config.global.filters.single=Filters
gadget.activity.stream.config.available.streams=Available Streams
gadget.activity.stream.config.available.streams.description=Each stream can be toggled and customised individually.
gadget.activity.stream.config.display.options=Display Options
gadget.activity.stream.config.global.filters.description=Global filters can be used to customise activity for all enabled streams.
gadget.activity.stream.config.global.filters.add=Add a global filter.
gadget.activity.stream.config.global.filters.single.description=Filters can be used to customise the activity in your stream.
gadget.activity.stream.config.global.filters.single.add=Add a filter.
gadget.activity.stream.config.add.filter=add filter
gadget.activity.stream.config.stream.enable=Enable stream
gadget.activity.stream.config.stream.disable=Disable stream

gadget.activity.stream.config.cancel=Cancel
gadget.activity.stream.config.save=Save
gadget.activity.stream.config.apply=Apply
gadget.activity.stream.config.add=Add
gadget.activity.stream.config.remove=Remove

gadget.activity.stream.config.option.name={0} ({1})

gadget.activity.stream.config.customize.all=All Activity
gadget.activity.stream.config.customize.customize=Customize Activity

gadget.activity.stream.config.rule.keys=Project
gadget.activity.stream.config.rule.username=User
gadget.activity.stream.config.rule.date=Date

gadget.activity.stream.config.specifier.is=is
gadget.activity.stream.config.specifier.not=is not
gadget.activity.stream.config.specifier.before=is before
gadget.activity.stream.config.specifier.after=is after
gadget.activity.stream.config.specifier.range=is in the range

gadget.activity.stream.config.date.range.to=and
gadget.activity.stream.config.is.operator=is

gadget.activity.stream.config.chosen.default.placeholder=Start typing to select options
gadget.activity.stream.config.chosen.default.noresults=No results matching "{0}"

gadget.activity.stream.config.numofentries=Limit to <span class="input-container"></span> items
gadget.activity.stream.config.refresh=Automatically refresh this activity stream
gadget.activity.stream.config.refresh.false=Never
gadget.activity.stream.config.refresh.15=every 15 minutes
gadget.activity.stream.config.refresh.30=every 30 minutes
gadget.activity.stream.config.refresh.60=every 1 hour
gadget.activity.stream.config.refresh.120=every 2 hours

# Date picker
gadget.activity.stream.config.datepicker.today=Today
gadget.activity.stream.config.datepicker.next=Next
gadget.activity.stream.config.datepicker.previous=Previous
gadget.activity.stream.config.datepicker.close=Close

# Verbs
streams.activity.verb.comment=Comment
streams.activity.verb.create=Create
streams.activity.verb.delete=Delete
streams.activity.verb.fail=Fail
streams.activity.verb.pass=Pass
streams.activity.verb.attach=Attach
streams.activity.verb.edit=Edit
streams.activity.verb.abandon=Abandon
streams.activity.verb.close=Close
streams.activity.verb.complete=Complete
streams.activity.verb.raise_defect=Raise Defect
streams.activity.verb.reopen=Reopen
streams.activity.verb.start=Start
streams.activity.verb.summarize=Summarise
streams.activity.verb.uncomplete=Uncomplete
streams.activity.verb.commit=Commit
streams.activity.verb.push=Push
streams.activity.verb.resolve=Resolve
streams.activity.verb.stop=Stop
streams.activity.verb.transition=Transition
streams.activity.verb.vote=Vote

# Relative dates
gadget.activity.stream.time.today=Today
gadget.activity.stream.time.past.moment=Moments ago
gadget.activity.stream.time.past.minute=1 minute ago
gadget.activity.stream.time.past.minutes={0} minutes ago
gadget.activity.stream.time.past.hour=1 hour ago
gadget.activity.stream.time.past.hours={0} hours ago
gadget.activity.stream.time.past.day=Yesterday
gadget.activity.stream.time.past.dayWithTime=Yesterday at {0}
gadget.activity.stream.time.past.days={0} days ago
gadget.activity.stream.time.past.weeks={0} weeks ago
gadget.activity.stream.time.past.year=More than a year ago

gadget.activity.stream.time.datetime={0} at {1}

# smart config errors
gadget.activity.stream.config.smart.config.max.rules.reached=All available filters have already been added.

# Authorisation messages (see applinks.js)
gadget.activity.stream.auth.request.header=Additional information may be available, please authenticate for more information:
gadget.activity.stream.auth.request.message=<a class="applink-authenticate" href="{0}">Authenticate</a> with <a href="{1}">{2}</a>.
gadget.activity.stream.auth.confirmation=This application is now using <a href="{0}">{1}</a>''s credentials. Revoke access at any time by going to <a href="{2}">OAuth access tokens</a>.
