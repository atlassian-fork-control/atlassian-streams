#!/bin/sh

set -e

product=fecru

# $0 <filename>
filename=$1

if [ $# -eq 0 ]
  then
    echo -e "Usage:\n  ./upgrade-$product.sh <zip-file>"
    exit 1
fi

# check if file exists, if not, exit immediately
if [ ! -f $filename ]
  then
    echo -e "[ERROR] File does not exist: $filename"
    exit 1
fi

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
idog_dir=/opt/j2ee/domains/atlassian.com/idog

date=`date +%s`
backup_dir=$script_dir/backup/$product-$date

echo -e "\n----------------------------\nUpgrading $product\n----------------------------\n"

# Create backup directory
echo -e "Creating backup directory $backup_dir..."
mkdir -p $backup_dir

# Backup current product
echo -e "Copying $idog_dir/$product directory..."
cp -R $idog_dir/$product $backup_dir

# Delete old product
echo -e "\nDeleting old $product product..."
rm -rf $idog_dir/$product

# Extract zip to idog_dir
echo -e "\nExtracting $filename to $idog_dir...\n"
unzip $filename -d $idog_dir

# Rename fecru-xxx directory to fecru
folder_name=`ls $idog_dir | grep fecru`
echo -e "\nRenaming $idog_dir/$folder_name to $idog_dir/fecru\n"
mv $idog_dir/$folder_name $idog_dir/fecru

# Copy data from old fecru
echo -e "\nCopying data from $backup_dir/fecru to $idog_dir/fecru\n"
cp $backup_dir/fecru/bin/fisheyectl.sh $idog_dir/fecru/bin

# Set file permissions
chown -R j2ee_idog.atlassian.com $idog_dir/$product* && chgrp -R j2ee_idog.atlassian.com $idog_dir/$product*

# Done
echo -e "\n----------------------------\nDone upgrading $product\nStart your server and make sure $product is properly configured\n----------------------------\n"

