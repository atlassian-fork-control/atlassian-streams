#!/bin/sh

#./deploy_confluence <milestone> <artifact_dir>

milestone=$1
artifact_dir=$2

app=Confluence
script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts
conf_bund_zip=/opt/j2ee/domains/atlassian.com/idog/tomcat/webapps/confluence/WEB-INF/classes/com/atlassian/confluence/setup/atlassian-bundled-plugins.zip
temp_dir=$script_dir/temp/conf_bund

bundled_plugins="atlassian-util-concurrent:2.3.0-beta2              \
                 atlassian-universal-plugin-manager-plugin:1.6-m2   \
                 activeobjects-plugin:0.15.9                        \
                 activeobjects-confluence-spi:0.15.9"

plugins="streams-aggregator-plugin             \
         streams-api                           \
         streams-core-plugin                   \
         streams-jira-inline-actions-plugin    \
         streams-bamboo-inline-actions-plugin  \
         streams-confluence-inline-actions-plugin \
         streams-inline-actions-plugin         \
         streams-fecru-inline-actions-plugin   \
         streams-confluence-plugin             \
         streams-spi                           \
         streams-thirdparty-plugin"

echo "-------------"

echo -e "Unzipping atlassian-bundled-plugins.zip.. to $temp_dir"

unzip $conf_bund_zip -d $temp_dir

echo -e "\nRemoving old version of STRMs"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $temp_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
         then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying STRM $milestone to $app bundled plugins"
for plugin in $plugins
do
  plugin_file=$artifact_dir/$plugin-$milestone.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $temp_dir"
      cp $plugin_file $temp_dir
  fi
done

echo -e "\nRemoving old version of required bundled plugins in $app"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      old_plugin=`find $temp_dir -type f -name "$plugin*"`
      if [ $old_plugin ]
        then
          echo -e "removing $(echo $old_plugin | awk 'BEGIN{FS="/"}{print $NF}')..."
          rm $old_plugin
      fi
  fi
done

echo -e "\nCopying required bundled plugins to $app"
for details in $bundled_plugins
do
  plugin=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  plugin_file=$artifact_dir/$plugin-$version.jar
  if [ -f $plugin_file ]
    then
      echo "copying $plugin_file to $temp_dir"
      cp $plugin_file $temp_dir
  fi
done


echo -e "\nZipping Confluence bundled plugins"

# Set file permissions
chown -R j2ee_idog.atlassian.com $temp_dir* && chgrp -R j2ee_idog.atlassian.com $temp_dir*

old_dir=`pwd`

cd $temp_dir
zip atlassian-bundled-plugins.zip *
cp atlassian-bundled-plugins.zip $conf_bund_zip

# Set file permissions
chown -R j2ee_idog.atlassian.com $conf_bund_zip* && chgrp -R j2ee_idog.atlassian.com $conf_bund_zip*

cd $old_dir

rm -rf $temp_dir

echo -e "\nDone deploying STRM $milestone to Confluence"
echo -e "-------------\n"
