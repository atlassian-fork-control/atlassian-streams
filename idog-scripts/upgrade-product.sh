#!/bin/sh

# $0 <product>
# $1 <version>
product=$1
version=$2

if [ $# -eq 0 ] 
  then 
    echo -e "Atlassian Product (bamboo, confluence, jira, fecru): \c"
    read product
    echo -e "Product Version: \c"
    read version
else
  if [ $# -eq 1 ]
    then
      echo -e "Product Version: \c"
      read version
  fi
fi

script_dir=/opt/j2ee/domains/atlassian.com/idog/scripts

atlassian_products[1]="com.atlassian.bamboo:atlassian-bamboo-web-app:war"
atlassian_products[2]="com.atlassian.confluence:confluence-webapp:war"
atlassian_products[3]="com.atlassian.crucible:atlassian-crucible:zip"
atlassian_products[4]="com.atlassian.jira:atlassian-jira-webapp:war"

# get product index based on supplied params
case $product in
"bamboo")
  p=1
  ;;
"confluence")
  p=2
  ;;
"fecru")
  p=3
  ;;
"jira")
  p=4
  ;;
*)
  echo -e "\n[ERROR] Invalid product (please select from one of the following - case-sensitive):"
  echo -e "  bamboo\n  confluence\n  fecru\n  jira\n"
  exit 1
  ;;
esac

#parse product string
path=`echo ${atlassian_products[$p]} | awk '{ print substr($0, 0, index($0, ":")-1 ) }' | sed 's/\./\//g'`
artifact=`echo ${atlassian_products[$p]} | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, 0, index($0, ":")-1) }'`
ext=`echo ${atlassian_products[$p]} | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`

# create temp directory
temp_dir=$script_dir/temp/$product/$version
mkdir -p $temp_dir

# Download product artifact
$script_dir/get-product.sh $p $version

# move downloaded files to created directory
mv $artifact-$version.$ext $temp_dir
product_artifact=$temp_dir/$artifact-$version.$ext

# make sure to backup old product before upgrading
# backupProduct

# perform product upgrade
$script_dir/upgrade-$product.sh $product_artifact

# remove temp directory
rm -rf $temp_dir


