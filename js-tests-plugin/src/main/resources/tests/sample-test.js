/* You can run the QUnit tests by starting the Aggregator plugin and going to
     http://localhost:3990/streams/plugins/servlet/jstests/all-tests (see atlassian-plugin.xml) */

module("Streams Sample QUnit Test");

test("sample test 1", function() {
    equals(2*2, 4, "2x2 = 4?");
});

test("sample test with two asserts", function() {
    equals(2-2, 0, "2-2 = 0?");
    equals(4-4, 0, "4-4 = 0 again?");
});
