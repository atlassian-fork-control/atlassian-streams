package com.atlassian.streams.jira;

import java.net.URI;

import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.common.uri.Uris;

import static com.google.common.base.Preconditions.checkNotNull;

public class UriProvider
{
    private static final String ISSUE_URI = "/browse/";

    private final WebResourceManager webResourceManager;

    UriProvider(WebResourceManager webResourceManager)
    {
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    public URI getIssueUri(URI baseUri, JiraActivityItem activityItem)
    {
        return getIssueUri(baseUri, activityItem.getIssue());
    }

    public URI getIssueUri(URI baseUri, Issue issue)
    {
        return getIssueUri(baseUri, issue.getKey());
    }

    public URI getIssueUri(URI baseUri, String issueKey)
    {
        return URI.create(getIssueUriStr(baseUri, issueKey));
    }

    private String getIssueUriStr(URI baseUri, String issueKey)
    {
        return baseUri + ISSUE_URI + issueKey;
    }

    public URI getIssueCommentUri(URI baseUri, Comment comment)
    {
        return URI.create(getIssueUriStr(baseUri, comment.getIssue().getKey()) +
            "?focusedCommentId=" + comment.getId() +
            "&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-" + comment.getId());
    }

    public URI getAttachmentUri(URI baseUri, Attachment attachment)
    {
        return URI.create(baseUri.toASCIIString() + "/secure/attachment/" +
            attachment.getId() + "/" + Uris.encode(attachment.getFilename()));
    }

    public URI getThumbnailUri(URI baseUri, Thumbnail thumbnail)
    {
        return URI.create(baseUri.toASCIIString() + "/secure/thumbnail/" +
            thumbnail.getAttachmentId() + "/" + Uris.encode(thumbnail.getFilename()));
    }

    public URI getComponentUri(URI baseUri, Issue issue, ProjectComponent component)
    {
        return getComponentUri(baseUri, issue.getProjectObject(), component);
    }

    public URI getComponentUri(URI baseUri, Project project, ProjectComponent component)
    {
        return getComponentUri(baseUri, project.getKey(), component.getId());
    }

    public URI getComponentUri(URI baseUri, String projectKey, Long componentId)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/" + projectKey +
                "/component/" + componentId.toString());
    }

    public URI getFixForVersionUri(URI baseUri, Version version)
    {
        return getFixForVersionUri(baseUri, version.getProjectObject().getKey(), version.getId());
    }

    public URI getFixForVersionUri(URI baseUri, String projectKey, Long versionId)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/" + projectKey +
                "/fixforversion/" + versionId.toString());
    }

    public URI getWikiRendererCssUri()
    {
        return URI.create(webResourceManager.getStaticPluginResource("jira.webresources:global-static",
                "wiki-renderer.css", com.atlassian.plugin.webresource.UrlMode.ABSOLUTE)).normalize();
    }

    public URI getBrokenThumbnailUri(URI baseUri)
    {
        return URI.create(baseUri.toASCIIString() + "/images/broken_thumbnail.png");
    }
}
