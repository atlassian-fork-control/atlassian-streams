package com.atlassian.streams.jira.changehistory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.google.common.collect.ImmutableCollection;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This class holds an issue and its change history.
 */
public final class IssueHistory
{
    private final Issue issue;
    private final List<ChangeHistory> changeHistories;

    IssueHistory(Issue issue, List<ChangeHistory> changeHistories)
    {
        this.issue = checkNotNull(issue, "issue");
        this.changeHistories = checkNotNull(changeHistories, "histories");
    }

    /**
     * Returns the issue.
     *
     * @return an Issue
     */
    public Issue issue()
    {
        return issue;
    }

    /**
     * Returns all ChangeHistory items for {@code issue}.
     *
     * @return an immutable collection of ChangeHistory
     */
    public List<ChangeHistory> changeHistories()
    {
        return changeHistories;
    }
}
