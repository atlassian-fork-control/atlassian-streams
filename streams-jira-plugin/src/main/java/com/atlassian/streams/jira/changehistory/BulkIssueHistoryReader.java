package com.atlassian.streams.jira.changehistory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.singleton;
import static java.util.Objects.requireNonNull;

/**
 * Issue history reader that bulk-fetches change histories for all the specified issues in one shot. Compatible with
 * JIRA 5.1 and above.

 * @since v5.1.2
 */
class BulkIssueHistoryReader implements IssueHistoryReader
{
    /**
     * The JIRA ChangeHistoryManager implementation.
     */
    private final ChangeHistoryManager changeHistoryManager;

    /**
     * Creates a new BulkIssueHistoryReader backed by the given ChangeHistoryManager.
     *
     * @param changeHistoryManager a ChangeHistoryManager
     */
    public BulkIssueHistoryReader(ChangeHistoryManager changeHistoryManager) throws NoSuchMethodException
    {
        this.changeHistoryManager = changeHistoryManager;
    }

    @Override
    public Iterable<IssueHistory> getChangeHistoriesForUser(Iterable<Issue> issues, ApplicationUser remoteUser, int limit) {
        return new Iterable<IssueHistory>() {
            @Override
            public Iterator<IssueHistory> iterator() {
                try {
                    return new BulkIterator(requireNonNull(issues, "issues"), remoteUser, limit);
                } catch (Exception e) {
                    throw new RuntimeException("Error creating BulkIterator", e);
                }
            }
        };
    }

    @Override
    public Iterable<IssueHistory> getChangeHistoriesForUser(final Iterable<Issue> issues, final ApplicationUser remoteUser)
    {
        return new Iterable<IssueHistory>()
        {
            @Override
            public Iterator<IssueHistory> iterator()
            {
                try
                {
                    return new BulkIterator(requireNonNull(issues, "issues"), remoteUser);
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Error creating BulkIterator", e);
                }
            }
        };
    }

    private Multimap<Issue, ChangeHistory> doGetChangeHistoriesForUser(Iterable<Issue> issues, ApplicationUser remoteUser, Integer limit)
            throws IllegalAccessException, InvocationTargetException
    {
        List<ChangeHistory> histories = new ArrayList<>();
        if (limit == null) {
            for (Issue eachIssue : issues) {
                histories.addAll(changeHistoryManager.getChangeHistoriesForUser(singleton(eachIssue), remoteUser));
            }
        } else {
            for (Issue eachIssue : issues) {
                histories.addAll(changeHistoryManager.getChangeHistoriesForUser(singleton(eachIssue), remoteUser, limit));
            }
        }
        

        // index the change histories by issue
        return Multimaps.index(histories, new ByIssueIndexer());
    }

    /**
     * Iterates through the specified issues, fetching all change histories in one hit at construction time.
     */
    private class BulkIterator implements Iterator<IssueHistory>
    {
        private final Iterator<Issue> issues;
        private final Multimap<Issue, ChangeHistory> changeHistories;

        private BulkIterator(Iterable<Issue> issues, ApplicationUser remoteUser, int limit)
                throws InvocationTargetException, IllegalAccessException
        {
            this.changeHistories = doGetChangeHistoriesForUser(issues, remoteUser, limit);
            this.issues = changeHistories.keySet().iterator();
        }

        private BulkIterator(Iterable<Issue> issues, ApplicationUser remoteUser)
                throws InvocationTargetException, IllegalAccessException {
            this.changeHistories = doGetChangeHistoriesForUser(issues, remoteUser, null);
            this.issues = changeHistories.keySet().iterator();
        }

        @Override
        public boolean hasNext()
        {
            return issues.hasNext();
        }

        @Override
        public IssueHistory next()
        {
            Issue nextIssue = issues.next();
            Collection<ChangeHistory> nextChangeHistories = changeHistories.get(nextIssue);

            return new IssueHistory(nextIssue, ImmutableList.copyOf(nextChangeHistories));
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    private static class ByIssueIndexer implements Function<ChangeHistory, Issue>
    {
        @Override
        public Issue apply(ChangeHistory changeHistory)
        {
            return changeHistory != null ? changeHistory.getIssue() : null;
        }
    }
}
