package com.atlassian.streams.jira;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.Suppliers;
import com.atlassian.streams.jira.changehistory.IssueHistory;
import com.atlassian.streams.jira.changehistory.IssueHistoryReader;
import com.atlassian.streams.jira.search.IssueFinder;
import com.atlassian.streams.jira.util.RenderingUtilities;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsActivityProvider;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Options.isDefined;
import static com.atlassian.streams.spi.Filters.inActivities;
import static com.atlassian.streams.spi.Filters.inDateRange;
import static com.atlassian.streams.spi.Filters.inUsers;
import static java.util.Objects.requireNonNull;

public class JiraStreamsActivityProvider implements StreamsActivityProvider
{
    // Amount of time to check to consider if action and comment is related
    static final long CLOSE_ENOUGH_TIME_LIMIT_MS = 200;
    public static final String PROVIDER_KEY = "issues";
    public static final String ISSUE_VOTE_REL = "http://streams.atlassian.com/syndication/issue-vote";

    private final JiraAuthenticationContext authenticationContext;
    private final CommentManager commentManager;
    private final JiraHelper helper;
    private final JiraActivityItemAggregator jiraActivityItemAggregator;
    private final JiraEntryFactory jiraEntryFactory;
    private final com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties;

    private final IssueFinder issueFinder;
    private final IssueHistoryReader issueHistoryReader;
    private static final Logger log = LoggerFactory.getLogger(JiraStreamsActivityProvider.class);

    public JiraStreamsActivityProvider(final JiraAuthenticationContext authenticationContext,
                                       final CommentManager commentManager,
                                       final JiraHelper helper,
                                       JiraActivityItemAggregator jiraActivityItemAggregator,
                                       final JiraEntryFactory jiraSyndEntryFactory,
                                       final ApplicationProperties jiraApplicationProperties,
                                       final IssueFinder issueFinder,
                                       final IssueHistoryReader issueHistoryReader)
    {
        this.authenticationContext = authenticationContext;
        this.commentManager = commentManager;
        this.helper = requireNonNull(helper, "helper");
        this.jiraActivityItemAggregator = requireNonNull(jiraActivityItemAggregator, "jiraActivityItemAggregator");
        this.jiraEntryFactory = jiraSyndEntryFactory;
        this.jiraApplicationProperties = jiraApplicationProperties;
        this.issueFinder = requireNonNull(issueFinder, "issueFinder");
        this.issueHistoryReader = requireNonNull(issueHistoryReader, "issueHistoryReader");
    }

    /**
     * Get the activity feed for the given request
     *
     * @param request The request
     * @return The ATOM feed
     */
    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest request) throws StreamsException
    {
        return new CancellableTask<StreamsFeed>()
        {
            final AtomicBoolean cancelled = new AtomicBoolean(false);

            @Override
            public StreamsFeed call() throws Exception
            {
                final List<Issue> issues = sortedIssues(request);

                List<JiraActivityItem> elements = extractActivity(issues, request, Suppliers.forAtomicBoolean(cancelled), request.getMaxResults());

                Iterable<JiraActivityItem> activityItems = orderByDate.sortedCopy(elements);

                Iterable<AggregatedJiraActivityItem> activities = jiraActivityItemAggregator.aggregate(activityItems);

                final Iterable<StreamsEntry> entries = jiraEntryFactory.getEntries(activities, request);

                final String title = RenderingUtilities.htmlEncode(
                        jiraApplicationProperties.getDefaultBackedString(APKeys.JIRA_TITLE) + " - "
                                + authenticationContext.getI18nHelper().getText("portlet.activityfeed.name"));

                int maxResults = request.getMaxResults();
                return new StreamsFeed(title, take(maxResults, entries), none(String.class));
            }

            @Override
            public Result cancel()
            {
                cancelled.set(true);
                return Result.CANCELLED;
            }

            private List<Issue> sortedIssues(ActivityRequest request) {
                final List<Issue> result = new ArrayList<>();
                result.addAll(issueFinder.find(request));
                Collections.sort(result, new UpdateComparator());
                return result;
            }

            class UpdateComparator implements Comparator<Issue> {
                @Override
                public int compare(Issue o1, Issue o2) {
                    Timestamp ts1 = o1.getUpdated();
                    Timestamp ts2 = o2.getUpdated();
                    return ts1.compareTo(ts2);
                }
            }
        };
    }

    /**
     * Extracts activity from the given list of issues
     */
    private List<JiraActivityItem> extractActivity(final Iterable<Issue> issues, final ActivityRequest request, Supplier<Boolean> cancelled, int limit)
    {
        final ApplicationUser user = authenticationContext.getUser();
        Predicate<String> inUsers = inUsers(request);
        Predicate<Date> containsDate = inDateRange(request);
        Predicate<Option<Pair<ActivityObjectType, ActivityVerb>>> hasActivity = isDefined();
        Predicate<Pair<ActivityObjectType, ActivityVerb>> inJiraActivities = inActivities(request);
        ImmutableList.Builder<JiraActivityItem> builder = ImmutableList.builder();

        // map the change histories by issue so we can look them up from inside the loop
        ChangeHistoryQuery changeHistories = new ChangeHistoryQuery(issueHistoryReader, issues, user);

        IssueActivityExtractor issueActivityExtractor = new IssueActivityExtractor(user, commentManager,
                helper, builder, changeHistories, inUsers, containsDate, inJiraActivities);

        for (final Issue issue : issues)
        {
            // Check for cancellation each time through this loop, since we may be pulling
            // quite a bit of change history for each issue

            if (cancelled.get())
            {
                throw new CancelledException();
            }

            issueActivityExtractor.extract(issue);
        }
        return builder.build();
    }


    private static final Ordering<JiraActivityItem> orderByDate = new Ordering<JiraActivityItem>()
    {
        @Override
        public int compare(JiraActivityItem item1, JiraActivityItem item2)
        {
            return item2.getDate().compareTo(item1.getDate());
        }
    };


    /**
     * This class is used to store and query the {@code IssueHistoryReader} results.
     */
    static class ChangeHistoryQuery
    {
        public static final int HISTORY_DEPTH = 100;
        private ImmutableMap<Issue, IssueHistory> queryResults;

        /**
         * Creates a new ChangeHistoryQuery by calling {@link IssueHistoryReader#getChangeHistoriesForUser(Iterable,
         * ApplicationUser)} and storing its results.
         */
        private ChangeHistoryQuery(IssueHistoryReader issueHistoryReader, Iterable<Issue> issues, ApplicationUser user)
        {
            ImmutableMap.Builder<Issue, IssueHistory> builder = ImmutableMap.builder();
            for (IssueHistory issueHistory : issueHistoryReader.getChangeHistoriesForUser(issues, user, HISTORY_DEPTH))
            {
                builder.put(issueHistory.issue(), issueHistory);
            }

            queryResults = builder.build();
        }

        private List<ChangeHistory> changeHistories(Issue issue)
        {
            if (!queryResults.containsKey(issue))
            {
                return ImmutableList.of();
            }

            return queryResults.get(issue).changeHistories();
        }

        List<ChangeHistory> sortedChangeHistories(Issue issue)
        {
            return changeHistories(issue);
        }
    }
}
