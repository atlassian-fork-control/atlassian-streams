package com.atlassian.streams.jira.renderer;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl2;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.ActivityVerbs;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.jira.JiraActivityItem;
import com.atlassian.streams.jira.JiraActivityObjectTypes;
import com.atlassian.streams.jira.JiraHelper;
import com.atlassian.streams.jira.UriProvider;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import static com.atlassian.streams.api.common.Pair.pair;
import static org.junit.Assert.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.Mockito.mock;

public class IssueUpdateRendererFactoryTest
{
    private static final String WORKLOG_COMMENT = "the worklog comment";
    private static final long WORKLOG_ID = 1L;
    private static final ApplicationUser ADMIN = new MockApplicationUser("admin");
    private static final ApplicationUser FRED = new MockApplicationUser("fred");
    private static final Pair<ActivityObjectType, ActivityVerb> ISSUE_UPDATE = pair(JiraActivityObjectTypes.issue(), ActivityVerbs.update());

    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraHelper jiraHelper;
    @Mock
    private WorklogService worklogService;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private StreamsEntryRendererFactory streamsEntryRendererFactory;
    private IssueUpdateRendererFactory subject;

    @Before
    public void setUp() {
        when(jiraHelper.renderComment()).thenReturn(c -> Html.html(""));
        when(jiraHelper.renderCommentString(any(Issue.class))).thenReturn(Html::html);
        when(worklogService.getById(eq(new JiraServiceContextImpl(ADMIN)), eq(WORKLOG_ID))).thenReturn(worklogWithComment(WORKLOG_COMMENT));
        when(worklogService.getById(eq(new JiraServiceContextImpl(FRED)), anyLong())).thenReturn(null);
        subject = new IssueUpdateRendererFactory(
                jiraHelper,
                mock(AttachmentRendererFactory.class),
                streamsEntryRendererFactory,
                mock(IssueActivityObjectRendererFactory.class),
                mock(I18nResolver.class),
                authenticationContext,
                mock(OutlookDateManager.class),
                mock(ProjectComponentManager.class),
                mock(VersionService.class),
                authenticationContext,
                worklogService,
                mock(UriProvider.class),
                mock(TemplateRenderer.class),
                mock(UserKeyService.class)
        );
    }

    @Test
    public void getI18nKeyReturnsWorklogNormally()
    {
        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn("timespent");
        when(changeItem.getString("oldstring")).thenReturn("0");
        when(changeItem.getString("newstring")).thenReturn("1");

        assertEquals("streams.item.jira.updated.list.single.worklog",
                subject.getI18nKey(changeItem, true));
    }

    @Test
    public void getI18nKeyShowsReducedTime()
    {
        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn("timespent");
        when(changeItem.getString("oldstring")).thenReturn("1");
        when(changeItem.getString("newstring")).thenReturn("0");

        assertEquals("streams.item.jira.updated.list.single.worklog.reduced",
                subject.getI18nKey(changeItem, true));
    }

    @Test
    public void testRenderWorklogComment() throws URISyntaxException {
        setCurrentUser(ADMIN);
        final GenericValue timespentChange = changeItem("timespent");
        final GenericValue worklogChange = changeItem("worklogid", String.valueOf(WORKLOG_ID));
        final GenericValue mainChange = parentChangeItem("worklog", ImmutableList.of(timespentChange, worklogChange));
        ChangeHistory changeHistory = new ChangeHistory(mainChange, mock(IssueManager.class), mock(UserManager.class));
        final JiraActivityItem activityItem = new JiraActivityItem(new MockIssue(), "summary", ISSUE_UPDATE, changeHistory);

        subject.newRenderer(
                activityItem,
                new URI(""),
                ImmutableList.of(timespentChange)
        );

        verify(streamsEntryRendererFactory).newCommentRenderer(any(), eq(Html.html(WORKLOG_COMMENT)));
    }

    @Test
    public void testRenderRestrictedWorklogComment() throws URISyntaxException {
        setCurrentUser(FRED);

        final GenericValue timespentChange = changeItem("timespent");
        final GenericValue worklogChange = changeItem("worklogid", String.valueOf(WORKLOG_ID));
        final GenericValue mainChange = parentChangeItem("worklog", ImmutableList.of(timespentChange, worklogChange));
        ChangeHistory changeHistory = new ChangeHistory(mainChange, mock(IssueManager.class), mock(UserManager.class));
        final JiraActivityItem activityItem = new JiraActivityItem(new MockIssue(), "summary", ISSUE_UPDATE, changeHistory);

        subject.newRenderer(
                activityItem,
                new URI(""),
                ImmutableList.of(timespentChange)
        );

        verify(streamsEntryRendererFactory).newCommentRenderer(any(), eq(Html.html("")));
    }

    private void setCurrentUser(ApplicationUser user) {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
    }

    private static GenericValue changeItem(String field) {
        return new MockGenericValue("changeitem", ImmutableMap.of("field", field));
    }

    private static GenericValue changeItem(String field, String oldValue) {
        return new MockGenericValue("changeitem", ImmutableMap.of("field", field, "oldvalue", oldValue));
    }

    private static GenericValue parentChangeItem(String field, Collection<GenericValue> children) {
        final MockGenericValue changeItem = new MockGenericValue("changeitem", ImmutableMap.of("field", field));
        changeItem.setRelated("ChildChangeItem", ImmutableList.copyOf(children));
        return changeItem;
    }

    private static Worklog worklogWithComment(String comment) {
        return new WorklogImpl2(null, null, null, comment, null, null, null, 0L, null);
    }
}
