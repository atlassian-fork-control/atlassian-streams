package com.atlassian.streams.jira;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.jira.JiraTesting.mockUserProfile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JiraUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    private static final URI BASE_URI = URI.create("http://localhost/streams");

    @Mock
    private EmailFormatter emailFormatter;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private UserManager userManager;
    @Mock
    private AvatarManager avatarManager;
    @Mock
    private StreamsI18nResolver i18nResolver;
    @Mock
    private UserUtil userUtil;
    @Mock
    private ApplicationUser user1;
    @Mock
    private ApplicationUser user3;

    @Override
    public String getProfilePathTemplate()
    {
        return "/secure/ViewProfile.jspa?name={username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return "/secure/useravatar?avatarId={profilePictureParameter}";
    }

    @Override
    protected String getProfilePicParameter(String username)
    {
        return "0";
    }

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new JiraUserProfileAccessor(
                userUtil, getApplicationProperties(), emailFormatter, authenticationContext, userManager,
                avatarManager, i18nResolver);
    }

    @Before
    public void prepareUserUtil()
    {
        when(emailFormatter.emailVisible(any())).thenReturn(true);

        when(user1.getDisplayName()).thenReturn("User");
        when(user1.getEmailAddress()).thenReturn("u@c.com");
        when(userUtil.getUserByName("user")).thenReturn(user1);
        when(userManager.getUserProfile("user")).thenReturn(mockUserProfile("user"));

        when(user3.getDisplayName()).thenReturn("User <3&'>");
        when(user3.getEmailAddress()).thenReturn("u3@c.com");
        when(userUtil.getUserByName("user3")).thenReturn(user3);
        when(userManager.getUserProfile("user3")).thenReturn(mockUserProfile("user3"));
    }

    @Test
    public void assertThatEmailIsOnlyAvailableIfItIsVisibleToTheLoggedInUser()
    {
        when(emailFormatter.emailVisible(any())).thenReturn(false);
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, "user").getEmail(), is(equalTo(none(String.class))));
    }

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfileUri()
    {
    }

    @Test
    @Override
    @Ignore("SAL is doing this for us in JIRA, so there's no reason to test it ourselves")
    public void assertThatUsernameIsUriEncodedInProfilePictureUri()
    {
    }

    @Test
    public void profilePictureUriIsTakenFromSal() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("http://example.test/profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "user").getProfilePictureUri();

        assertEquals(Option.some(profileUri), uri);
    }

    @Test
    public void profilePictureUriIsTreatedRelativeToBaseUrl() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "user").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://localhost/streams/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsNormalizedAfterConstructionFromSal() throws Exception
    {
        UserProfile profile = mock(UserProfile.class);
        URI profileUri = new URI("/path-absolute-profile-picture-provided-by-sal");

        when(profile.getProfilePageUri()).thenReturn(new URI(""));
        when(profile.getProfilePictureUri(anyInt(), anyInt())).thenReturn(profileUri);

        when(userManager.getUserProfile("user")).thenReturn(profile);

        final URI baseUri = URI.create("http://example.test/streams/..");
        Option<URI> uri = userProfileAccessor.getUserProfile(baseUri, "user").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://example.test/path-absolute-profile-picture-provided-by-sal")), uri);
    }

    @Test
    public void profilePictureUriIsConstructedWhenSalReturnsNull() throws Exception
    {
        when(avatarManager.getAnonymousAvatarId()).thenReturn(1L);

        Option<URI> uri = userProfileAccessor.getUserProfile(BASE_URI, "I don't exist").getProfilePictureUri();

        assertEquals(Option.some(new URI("http://localhost/streams/secure/useravatar?avatarId=1")), uri);
    }
}
