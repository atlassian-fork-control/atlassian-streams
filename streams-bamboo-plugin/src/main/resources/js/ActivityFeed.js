// class ActivityFeed
function ActivityFeed(feedId, baseUrl, resourceUrl, feedUrl, maxResults)
{
    this.feedId = feedId;
    this.feedContainer = jQuery('#feedContainer-' + feedId);
    this.baseUrl = baseUrl;
    this.resourceUrl = resourceUrl;
    this.feedUrl = feedUrl;

    this.maxResults = parseInt(maxResults);
    var thisFeed = this;
    jQuery("#showMoreLink-" + feedId).click(function(event)
    {
        thisFeed.toggleShowMoreLink(false);
        thisFeed.maxResults += 10;
        thisFeed.populateFeed();
    });

    this.COMMENT_ICON_HTML = '<img class="activity-item-comment-icon" style="visibility:hidden" src="'
            + resourceUrl + '/showcomment" title="Add Comment"/>';
}

ActivityFeed.prototype.setFeedUrl = function(feedUrl)
{
    this.feedUrl = feedUrl;
};

ActivityFeed.prototype.toggleShowMoreLink = function(show)
{
    this.setMoreAndWaitingLinkVisibility(show, !show);
};

ActivityFeed.prototype.hideShowMoreAndWaitingLinks = function()
{
    this.setMoreAndWaitingLinkVisibility(false, false);
};

ActivityFeed.prototype.setMoreAndWaitingLinkVisibility = function(showMoreLink, showWaitingLink)
{
    var moreLink = document.getElementById("showMoreLink-" + this.feedId);
    if (moreLink)
    {
        moreLink.style.display = showMoreLink ? 'inline' : 'none';
    }
    var moreWaiting = document.getElementById("moreWaiting-" + this.feedId);
    if (moreWaiting)
    {
        moreWaiting.style.display = showWaitingLink ? 'inline' : 'none';
    }
};

ActivityFeed.prototype.addItem = function(feedItem, dateFormat)
{
    var dateContainer = this.getOrCreateDateContainer(DateUtils.format(DateUtils.utcStringToDateByHour(feedItem.updated), dateFormat));
    var items = dateContainer.children("ul");
    var item = jQuery(document.createElement('li'));
    item.addClass("activity-item");
    if (feedItem.icon)
    {
        item.css("backgroundImage", "url('" + feedItem.icon + "')");
    }
    item.html(feedItem.title);
    item.append(jQuery("<div class='activity-item-description'></div>").html(feedItem.content));
    if (feedItem.replyTo) {
        this.addCommentForm(feedItem.replyTo, item);
    }
    items.append(item);
};

ActivityFeed.prototype.getOrCreateDateContainer = function(date)
{
    var containerId = "dateContainer-" + this.feedId + "-" + date.replace(/\s/g, "");
    var container = jQuery("#" + containerId);
    if (container.size() == 0)
    {
        var div = jQuery("#feedContainer-" + this.feedId);

        container = jQuery(document.createElement("div"));
        container.attr("id", containerId);
        div.append(container);

        var dateLabel = jQuery(document.createElement("h3"));
        dateLabel.addClass("timestamp");
        dateLabel.html(date);
        container.append(dateLabel);
        var items = jQuery(document.createElement('ul'));
        items.addClass("activityItems");
        container.append(items);
    }

    return container;
};

ActivityFeed.prototype.getDateFormat = function(minDate, maxDate)
{
    if (minDate == null || maxDate == null)
        return "MMMMM dd - hh aa";

    var delta = maxDate.getTime() - minDate.getTime();

    if (delta <= DateUtils.MILLIS_PER_DAY * 2)
        return "MMMMM dd - hh aa";
    else if (delta <= DateUtils.MILLIS_PER_DAY * 30)
        return "MMMMM dd";
    else
        return "MMMMM yyyy";
};

/**
 * Adds comment forms to all feeds that have a replyTo set.  This should be called before aggregation,
 * because the aggregation depends on it.
 * @param replyTo The reply-to link for the item in question
 * @param item The jquery-wrapped dom element to add comment elements to
 * @return replyTo The reply-to link for the item in question
 */
ActivityFeed.prototype.addCommentForm = function(replyTo, item)
{
    var commentForm = AJS.$('<form method="post" class="activity-item-comment-form">' +
                           '<div class="stream-comment-message hidden"></div>' +
                           '<input type="hidden" name="replyTo" value="' + replyTo + '"/>' +
                           '<textarea name="comment" rows="6" cols="40"></textarea>' +
                           '<button type="submit" name="submit">Save</button></form>');
    if (replyTo) {
        function getXsrfTokenFromCookie() {
            return AJS.$.cookie('atl.xsrf.token');
        }
        commentForm.append(AJS.$("<input>", {type: "hidden", name: "atl_token", value: getXsrfTokenFromCookie()}));
        item.prepend(this.COMMENT_ICON_HTML);
        item.append(commentForm);
    }
    return item;
};

ActivityFeed.prototype.populateFeed = function()
{
    var showCommentButton = this.resourceUrl + "/showcomment",
            hideCommentButton = this.resourceUrl + "/hidecomment";

    var thisFeed = this;

    var handleSuccess = function(data, textStatus)
    {
        thisFeed.feedContainer.empty(); // remove the loading graphic
        jQuery('<div class="stream-message-container hidden"></div>').appendTo(thisFeed.feedContainer);

        var feed = new JAtom(data);
        if (!feed || !feed.items || feed.items.length == 0)
        {
            thisFeed.setWarning('No activity was found');
            return;
        }

        thisFeed.setError(null);
        thisFeed.setWarning(null);

        var maxDate, minDate;
        for (var i = 0; i < feed.items.length; i++)
        {
            var timestamp = DateUtils.utcStringToDateByHour(feed.items[i].updated);
            if (i == 0)
            {
                maxDate = timestamp;
                minDate = timestamp;
            }
            else if (timestamp.getTime() > maxDate.getTime())
            {
                maxDate = timestamp;
            }
            else if (timestamp.getTime() < minDate.getTime())
                {
                    minDate = timestamp;
                }
        }

        var dateFormat = thisFeed.getDateFormat(minDate, maxDate);

        for (i = 0; i < feed.items.length; i++)
        {
            thisFeed.addItem(feed.items[i], dateFormat);
        }

        // STRM-124: if we asked for 10 results and got back 5, there's no point displaying the showMore link
        if (feed.items.length < thisFeed.maxResults)
        {
            thisFeed.hideShowMoreAndWaitingLinks();
        }
        else
        {
            thisFeed.toggleShowMoreLink(true);
        }

        // Fade in comment icon when mouse over
        jQuery(".activity-item:has(.activity-item-comment-icon)").hover(function ()
        {
            // Would love to just use fadeIn fadeOut here, but we can't, because they use
            // display: None at the end, but we want the icon to still take up space
            jQuery(this).find("img.activity-item-comment-icon").css("visibility", "visible").fadeIn("fast");
        }, function()
        {
            jQuery(this).find("img.activity-item-comment-icon").fadeOut("fast", function()
            {
                jQuery(this).css("visibility", "hidden").css("display", "inline");
            });
        });

        // Highlight everything when mouse is over it
        jQuery(".activity-item").hover(function()
        {
            jQuery(this).css("background-color", "#FFFFF0");
        }, function()
        {
            jQuery(this).css("background-color", "");
        });

        // Callbacks for clicking on comment icon
        jQuery(".activity-item-comment-icon")
            .click(function () {
                var form = jQuery(this).parent().children("form.activity-item-comment-form");
                form.find('div.stream-comment-message').empty().addClass('hidden');
                form.slideToggle();
                form.find('textarea').focus();
            })
            .toggle(function () {
                jQuery(this).attr("src", hideCommentButton).attr("title", "Hide");
            }, function() {
                jQuery(this).attr("src", showCommentButton).attr("title", "Add Comment");
            });

        jQuery(".activity-item-comment-form")
                .children()
                .focus(function()
        {
            jQuery(this).parent().children("button").attr("accesskey", "s");
        })
                .blur(function()
        {
            jQuery(this).parent().children("button").removeAttr("accesskey");
        });

        jQuery(".activity-item-comment-form").submit(function(event)
        {
            event.preventDefault();
            var form = jQuery(this);

            /* todo make go through the issueRequest abstraction so it will work with gadgets API
             thisFeed.issueRequest("POST", thisFeed.baseUrl + "/plugins/servlet/streamscomments", "comment="+escape(this["comment"]
             +"&replyTo=" + escape(this["replyTo"])), function()
             {
             form.children("textarea").val("").end();
             form.prev(".activity-item-comment-icon").click();
             thisFeed.populateFeed();
             }, null);
             */
            form.children("button").attr("disabled", "true");
            jQuery.ajax({
                type: 'POST',
                url: thisFeed.baseUrl + "/plugins/servlet/streamscomments",
                data: jQuery(this).serialize(),
                dataType: "text",
                success: function() {
                    form.children("textarea").val("").end();
                    form.prev(".activity-item-comment-icon").click();
                    form.children("button").removeAttr("disabled");
                    thisFeed.populateFeed();
                },
                error: function(request) {
                    thisFeed.setError('Error saving comment: ' + request.statusText, form.find('div.stream-comment-message'));
                    form.children("button").removeAttr("disabled");
                }
        });
        });
        thisFeed.feedPopulated();
    };

    var handleFailure = function(request, textStatus, errorThrown)
    {
        thisFeed.setDivText('feedContainer-' + thisFeed.feedId, ''); // remove the loading graphic
        thisFeed.setError('Error communicating with server: ' + request.statusText);
        thisFeed.toggleShowMoreLink(true);
    };

    var sUrl = this.feedUrl;
    if (sUrl.indexOf("?") < 0)
    {
        sUrl += "?";
    }
    else
    {
        sUrl += "&";
    }
    sUrl += "maxResults=" + this.maxResults;

    this.issueRequest("GET", sUrl, null, handleSuccess, handleFailure);

};

ActivityFeed.prototype.issueRequest = function (method, url, data, successCallback, failureCallback)
{
    jQuery.ajax({
        type: method,
        url: url,
        dataType: jQuery.browser.msie ? 'text' : 'xml',
        success: successCallback,
        error: failureCallback
    });
};

ActivityFeed.prototype.feedPopulated = function()
{

};

ActivityFeed.prototype.setWarning = function(text, context) {
    context = context ? jQuery(context) : this.feedContainer.find('div.stream-message-container');
    context.empty();
    if (text) {
        context.removeClass('hidden');
        AJS.messages.error(context, {
            body: text,
            closeable: true,
            messageClose: function() {
                context.addClass('hidden');
            }
        });
    } else {
        context.addClass('hidden');
    }
};

ActivityFeed.prototype.setError = function(text, context) {
    context = context ? jQuery(context) : this.feedContainer.find('div.stream-message-container');
    context.empty();
    if (text) {
        context.removeClass('hidden');
        AJS.messages.error(context, {
            body: text,
            closeable: true,
            messageClose: function() {
                context.addClass('hidden');
            }
        });
    } else {
        context.addClass('hidden');
    }
};

ActivityFeed.prototype.setDivText = function(divId, text)
{
    var div = document.getElementById(divId);
    if (div)
    {
        // The inner div is so that we can add padding in the CSS without having the error box appear when empty
        div.innerHTML = text ? '<div class="inner">' + text + '</div>' : '';
    }
    else
    {
        alert(text);
    }
};

ActivityFeed.toggleStackTraces = function()
{
    jQuery(".activityFeed .error .stack-trace").toggle();
};

// end class ActivityFeed

function isNumeric(sText)
{
    var ValidChars = "0123456789";
    var Char;

    for (var i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            return false;
        }
    }
    return true;
}

var DateUtils =
{
    /**
     * takes a string and formats it based on the rules of Java's simple data format
     * yyyy - 4 digit year
     * yy - 2 digit year
     * MM - 2 digit month
     * MMM - 3 letter month abbrev
     * MMMMM - month word
     * d - day in month
     * k - hour in day (1-24)
     * h - hour in AM/PM
     * a - am/pm marker
     * m - minute in hour
     * s - second in minute
     *
     * @param date
     * @param formatString
     */
    format:function (date, formatString)
    {
        var str = formatString.replace("yyyy", date.getFullYear(), "g");
        str = str.replace("yy", DateUtils.getShortYear(date), "g");
        str = str.replace("MMMMM", DateUtils.getLongMonth(date), "g");
        str = str.replace("MMM", DateUtils.getMonthAbbrev(date), "g");
        str = str.replace("MM", date.getMonth() + 1, "g");
        str = str.replace("dd", date.getDate(), "g");
        str = str.replace("kk", date.getHours() + 1, "g");
        str = str.replace("hh", DateUtils.getHourIn12(date), "g");
        str = str.replace("aa", DateUtils.getAmOrPm(date), "g");
        str = str.replace("mm", date.getMinutes(), "g");
        str = str.replace("ss", date.getSeconds(), "g");

        return str;
    },

    getShortYear:function (date)
    {
        var year = date.getYear() % 100;
        return year < 10 ? '0' + year : year;
    },

    getLongMonth:function (date)
    {
        return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                [date.getMonth()];
    },

    getMonthAbbrev:function (date)
    {
        return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()];
    },

    getHourIn12:function(date)
    {
        if (date.getHours() == 0)
            return 12;
        else
            return date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    },

    getAmOrPm:function(date)
    {
        return date.getHours() < 12 ? "am" : "pm";
    },

    /** useful for ROME formatted timestamps in feeds */
    utcStringToDateByHour: function(string)
    {
        var date = new Date();

        date.setUTCFullYear(string.substr(0, 4));
        date.setUTCMonth(string.substr(5, 2) - 1);
        date.setUTCDate(string.substr(8, 2));
        date.setUTCHours(string.substr(11, 2));
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        date.setUTCMilliseconds(0);

        return date;
    }
};

function htmlEncode(value)
{
    return jQuery("<div/>").text(value).html();
}

DateUtils.MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
