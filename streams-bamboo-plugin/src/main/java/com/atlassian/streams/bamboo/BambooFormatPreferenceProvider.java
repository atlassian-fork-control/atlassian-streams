package com.atlassian.streams.bamboo;

import com.atlassian.streams.spi.DefaultFormatPreferenceProvider;

import org.joda.time.DateTimeZone;

public class BambooFormatPreferenceProvider extends DefaultFormatPreferenceProvider
{
    public DateTimeZone getUserTimeZone()
    {
        String timeZone = System.getProperty("user.timezone");

        try
        {
            return DateTimeZone.forID(timeZone);
        }
        catch (IllegalArgumentException e)
        {
            return DateTimeZone.getDefault();
        }
    }
}
