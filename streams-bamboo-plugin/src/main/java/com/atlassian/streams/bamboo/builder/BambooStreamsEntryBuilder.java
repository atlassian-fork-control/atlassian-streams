package com.atlassian.streams.bamboo.builder;

import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.NonEmptyIterable;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsUriBuilder;

import java.net.URI;

/**
 * Builder from a Bamboo object to a {@code StreamsEntry}.
 */
public interface BambooStreamsEntryBuilder<T>
{
    /**
     * Build the {@code StreamsEntry} URL where more information about the entry can be found.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the URL.
     */
    URI buildUri(URI baseUri, T t);

    /**
     * Build the {@code StreamsEntry} URI which will be used as a unique identifier for this entry.
     *
     * @param baseUri The baseUri of the application. This could be an absolute link or relative
     *                link depending on circumstance.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the URI
     */
    URI buildId(URI baseUri, StreamsUriBuilder uriBuilder, T t);

    /**
     * Build the {@code StreamsEntry} activity object.
     *
     * @param baseUri The baseUri of the application. This could be an absolute link or relative
     *                link depending on circumstance.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the activity object
     */
    ActivityObject buildActivityObject(URI baseUri, T t);

    /**
     * Build the {@code StreamsEntry} verb.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the verb
     */
    ActivityVerb buildVerb(T t);

    /**
     * Build the {@code StreamsEntry} target object.
     *
     * @param baseUri The baseUri of the application. This could be an absolute link or relative
     *                link depending on circumstance.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the target
     */
    Option<ActivityObject> buildTarget(URI baseUri, T t);

    /**
     * Build the {@code StreamsEntry} category.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the category
     */
    Iterable<String> buildCategory(T t);

    /**
     * Build the {@code StreamsEntry} author set.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the author set
     */
    NonEmptyIterable<UserProfile> getAuthors(final URI baseUri, T t);

    /**
     * Build the {@code StreamsEntry} links map to related resources such as icons.
     *
     * @param baseUri The baseUri of the application. This could be an absolute link or relative
     *                link depending on circumstance.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return the links map
     */
    Iterable<Link> buildLinks(URI baseUri, T t);

    /**
     * Return a {@code Renderer} for the entry.
     * @param t the object from which the {@code StreamsEntry} is to be produced
     * @return renderer
     */
    Renderer getRenderer(URI baseUri, T t);
}
