package com.atlassian.streams.bamboo.builder;

import java.net.URI;
import java.util.Optional;

import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.bamboo.BambooRendererFactory;
import com.atlassian.streams.bamboo.UriProvider;
import com.atlassian.streams.spi.UserProfileAccessor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.matchers.Matchers.hasStreamsLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereStreamsRel;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResultsSummaryStreamsEntryBuilderTest
{
    public static final URI BASE_URI = URI.create("http://example.com");

    @Mock ResultsSummaryStreamsEntryBuilder builder;
    @Mock UriProvider uriProvider;
    @Mock UserProfileAccessor userProfileAccessor;
    @Mock BambooRendererFactory rendererFactory;
    @Mock WebResourceManager webResourceManager;
    @Mock UserManager userManager;
    @Mock ImmutableJob job;
    @Mock Chain plan; //parent plan must be a chained plan
    @Mock ResultsSummary resultsSummary;
    StreamsEntry.Link iconLink = new StreamsEntry.Link(URI.create("http://link/to/icon"), "icon", some("sometitle"));
    StreamsEntry.Link runLink = new StreamsEntry.Link(URI.create("http://link/to/run"), "run", some("sometitle"));
    StreamsEntry.Link replyToLink = new StreamsEntry.Link(URI.create("http://link/to/comment"), "comment", some("sometitle"));

    @Before
    public void createResultsSummaryStreamsEntryBuilder()
    {
        when(uriProvider.getBuildIconLink(any(URI.class), same(resultsSummary))).thenReturn(iconLink);
        when(uriProvider.getReplyToLink(any(URI.class), same(resultsSummary))).thenReturn(replyToLink);
        when(uriProvider.getBuildTriggerLink(any(URI.class), same(resultsSummary))).thenReturn(runLink);
        when(userManager.getRemoteUsername()).thenReturn("user");

        builder = new ResultsSummaryStreamsEntryBuilder(uriProvider, userProfileAccessor, rendererFactory, webResourceManager, userManager);
    }

    @Before
    public void createResultsSummaryObject()
    {
        when(job.isSuspendedFromBuilding()).thenReturn(false);
        when(job.getParent()).thenReturn(plan);

        when(plan.isSuspendedFromBuilding()).thenReturn(false);

        Mockito.<Optional<? extends ImmutablePlan>>when(resultsSummary.getPlanIfExists()).thenReturn(Optional.ofNullable(job));
    }

    @Test
    public void assertThatRunLinkExistsWhenBothPlanAndJobAreEnabled()
    {
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), hasStreamsLink(whereStreamsRel(is(equalTo("run")))));
    }

    @Test
    public void assertThatRunLinkDoesNotExistWhenPlanIsDisabled()
    {
        when(plan.isSuspendedFromBuilding()).thenReturn(true);
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), not(hasStreamsLink(whereStreamsRel(is(equalTo("run"))))));
    }

    @Test
    public void assertThatRunLinkDoesNotExistWhenJobIsDisabled()
    {
        when(job.isSuspendedFromBuilding()).thenReturn(true);
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), not(hasStreamsLink(whereStreamsRel(is(equalTo("run"))))));
    }

    @Test
    public void assertThatRunLinkDoesNotExistWhenBothPlanAndJobAreDisabled()
    {
        when(job.isSuspendedFromBuilding()).thenReturn(true);
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), not(hasStreamsLink(whereStreamsRel(is(equalTo("run"))))));
    }

    @Test
    public void assertThatRunLinkDoesNotExistWhenThereIsNoLoggedInUser()
    {
        when(userManager.getRemoteUsername()).thenReturn(null);
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), not(hasStreamsLink(whereStreamsRel(is(equalTo("run"))))));
    }

    @Test
    public void assertThatCommentLinkExistsWhenThereIsALoggedInUser()
    {
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), hasStreamsLink(whereStreamsRel(is(equalTo("comment")))));
    }

    @Test
    public void assertThatCommentLinkDoesNotExistWhenThereIsNoLoggedInUser()
    {
        when(userManager.getRemoteUsername()).thenReturn(null);
        assertThat(builder.buildLinks(BASE_URI, resultsSummary), not(hasStreamsLink(whereStreamsRel(is(equalTo("comment"))))));
    }
}
