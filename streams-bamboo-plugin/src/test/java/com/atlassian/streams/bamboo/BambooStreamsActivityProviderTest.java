package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.core.BambooObject;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.HasAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.HasApplicationType;
import com.atlassian.streams.api.StreamsEntry.HasAuthors;
import com.atlassian.streams.api.StreamsEntry.HasId;
import com.atlassian.streams.api.StreamsEntry.HasPostedDate;
import com.atlassian.streams.api.StreamsEntry.HasRenderer;
import com.atlassian.streams.api.StreamsEntry.HasVerb;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.Evictor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooStreamsActivityProviderTest
{
    @Mock
    private BambooEntryFactory entryFactory;
    @Mock
    private ResultsSummaryManager resultsSummaryManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ActivityRequest activityRequest;
    @Mock
    private Multimap<String, Pair<Operator, Iterable<String>>> providerFilters;
    @Mock
    private Multimap<String, Pair<Operator, Iterable<String>>> standardFilters;
    @Mock
    private Project project;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private Evictor<BambooObject> bambooEvictor;
    @Mock
    private BambooAliasResultsSearcher authorSearch;
    private BambooStreamsActivityProvider streamsActivityProvider;
    private List<String> userNames = Collections.singletonList("username");
    private ImmutableSet<String> aliases = ImmutableSet.of("alias");

    @Before
    public void createStreamsActivityProvider()
    {
        prepareActivityAndHttpRequest();
        prepareProject();
        prepareI18nResolver();
        prepareAuthorSearch();
        prepareEntryFactory();
        streamsActivityProvider = new BambooStreamsActivityProvider(resultsSummaryManager,
                projectManager,
                entryFactory,
                authorSearch,
                i18nResolver,
                bambooEvictor);
    }

    public void prepareEntryFactory()
    {
        @SuppressWarnings("rawtypes")
        StreamsEntry.Parameters params = newEntryParams();
        @SuppressWarnings("unchecked")
        StreamsEntry entry = new StreamsEntry(params, i18nResolver);
        when(entryFactory.buildEntry(any(), any())).thenReturn(entry);
    }

    public void prepareActivityAndHttpRequest()
    {
        when(activityRequest.getMaxResults()).thenReturn(100);
        when(activityRequest.getProviderFilters()).thenReturn(providerFilters);
        when(activityRequest.getStandardFilters()).thenReturn(standardFilters);
    }

    public void prepareAuthorOnlyRequest()
    {
        when(standardFilters.size()).thenReturn(1);
        when(providerFilters.isEmpty()).thenReturn(true);

        Collection<Pair<Operator, Iterable<String>>> authorFilters = new ArrayList<>();
        authorFilters.add(Pair.pair(Operator.IS, userNames));
        when(standardFilters.get(USER.getKey())).thenReturn(authorFilters);
    }

    public void prepareProject()
    {
        when(projectManager.getProjectByKey("STRM")).thenReturn(project);
        when(project.getName()).thenReturn("Streams");
    }

    public void prepareI18nResolver()
    {
        when(i18nResolver.getText("streams.bamboo.emptyprojectlist.name")).thenReturn("all projects");
        when(i18nResolver.getText("streams.bamboo.title", "Streams")).thenReturn("Activity Stream for Streams");
        when(i18nResolver.getText("streams.bamboo.title", "all projects")).thenReturn("Activity Stream for all projects");
        when(i18nResolver.getText("streams.bamboo.subtitle")).thenReturn("Activity for Bamboo");
    }

    public void prepareAuthorSearch()
    {
        List<ResultsSummary> innerResults = new ArrayList<>();
        innerResults.add(getResultsSummaryWithAuthor("username"));
        innerResults.add(getResultsSummaryWithAuthor("alias"));
        PartialList<ResultsSummary> results = new PartialList<>(2, innerResults);
        when(authorSearch.getResultsByUserNames(argThat(containsString("username")), anyInt())).thenReturn(results);
        when(authorSearch.getLinkedAuthorsByUserNames(argThat(containsString("username")))).thenReturn(aliases);
    }

    private StreamsEntry.Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, HasRenderer, HasVerb, HasAuthors> newEntryParams()
    {
        return StreamsEntry.params()
                           .id(URI.create("http://example.com"))
                           .postedDate(new DateTime())
                           .alternateLinkUri(URI.create("http://example.com"))
                           .applicationType("test")
                           .authors(ImmutableNonEmptyList.of(new UserProfile.Builder("someone").fullName("Some One")
                                                                                               .build()))
                           .addActivityObject(new ActivityObject(ActivityObject.params()
                                                                               .id("activity")
                                                                               .title(some("Some activity"))
                                                                               .alternateLinkUri(URI.create("http://example.com"))
                                                                               .activityObjectType(comment())))
                           .verb(post()).renderer(newRenderer())
                           .baseUri(URI.create("http://example.com"));
    }

    private Renderer newRenderer()
    {
        return mock(Renderer.class);
    }

    private ResultsSummary getResultsSummaryWithAuthor(String authorName)
    {
        String buildKey = "BUILD-KEY-JOB1";

        Author author = mock(Author.class);
        when(author.getName()).thenReturn(authorName);
        ResultsSummary results = mock(ResultsSummary.class);
        when(results.getUniqueAuthors()).thenReturn(ImmutableSet.of(author));
        ImmutableJob job = mock(ImmutableJob.class);
        when(results.getBuildKey()).thenReturn(buildKey);
        when(results.getPlanKey()).thenReturn(PlanKeys.getPlanKey(buildKey));
        return results;
    }

    private ArgumentMatcher<Iterable<String>> containsString(final String value)
    {
        return argument -> Iterables.any(argument, value::equals);
    }

    @Test
    public void verifyThatSearchIsCalledWithUserName() throws Exception
    {
        prepareAuthorOnlyRequest();

        streamsActivityProvider.getActivityFeed(activityRequest).call();
        verify(authorSearch, atLeastOnce()).getResultsByUserNames(argThat(containsString("username")), eq(100));
    }

    @Test
    public void assertThatSearchByAuthorsReturnsEvents() throws Exception
    {

        prepareAuthorOnlyRequest();

        CancellableTask<StreamsFeed> task = streamsActivityProvider.getActivityFeed(activityRequest);
        StreamsFeed feed = task.call();
        Iterable<StreamsEntry> entries = feed.getEntries();
        assertEquals(2, Iterables.size(entries));
    }

    @Test(expected = CancelledException.class)
    public void assertThatCancelledTaskThrowsExeption() throws Exception
    {
        CancellableTask<StreamsFeed> task = streamsActivityProvider.getActivityFeed(activityRequest);
        task.cancel();
        task.call();
    }

    @Test
    public void verifyThatActivityFeedFindsBuildByProjectIfProjectKeysAreNotEmpty() throws Exception
    {
        Iterable<String> filterValues = ImmutableList.of("STRM");
        Collection<Pair<Operator, Iterable<String>>> filterPairs = ImmutableList.of(pair(IS, filterValues));
        when(standardFilters.get(PROJECT_KEY)).thenReturn(filterPairs);

        streamsActivityProvider.getActivityFeed(activityRequest).call();
        verify(projectManager, atLeastOnce()).getProjectByKey("STRM");
    }

    @Test
    public void assertThatFeedTitleIsCorrectWhenNoProjectIsDefined() throws Exception
    {
        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for all projects")));
    }

    @Test
    public void assertThatFeedTitleIsCorrectWhenAProjectIsDefined() throws Exception
    {
        Iterable<String> filterValues = ImmutableList.of("STRM");
        Collection<Pair<Operator, Iterable<String>>> filterPairs = ImmutableList.of(pair(IS, filterValues));
        when(standardFilters.get(PROJECT_KEY)).thenReturn(filterPairs);

        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getTitle(), is(equalTo("Activity Stream for Streams")));
    }

    @Test
    public void assertThatFeedSubtitleIsCorrect() throws Exception
    {
        StreamsFeed feed = streamsActivityProvider.getActivityFeed(activityRequest).call();
        assertThat(feed.getSubtitle(), is(equalTo(some("Activity for Bamboo"))));
    }
}
