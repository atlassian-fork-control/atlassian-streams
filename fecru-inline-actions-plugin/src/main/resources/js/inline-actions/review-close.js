/**
 * Registers a "Close" action against any feed items with a "review" type.
 *
 * Creates a link which closes a review.
 */
(function() {

    /**
     * Closes a review.
     *
     * @method closeReview
     * @param {Event} e Event object
     */
    function closeReview(e) {
        var activityItem = AJS.$(e.target).closest('div.activity-item'),
            url,
            feedItem = e.data && e.data.feedItem;

        if (feedItem) {
            url = feedItem.links['http://streams.atlassian.com/syndication/review-close'];
        } else {
            ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.close.failure.general'), 'error');
            return;
        }

        e.preventDefault();
        hideLink(activityItem);

        ActivityStreams.ReviewTransitions.close(e, activityItem, url, feedItem);
    }

    /**
     * Hide the link, showing the non-hyperlinked label instead.
     *
     * @method hideLink
     * @param {Object} activityItem the .activity-item div
     */
    function hideLink(activityItem) {
        activityItem.find('a.activity-item-review-close-link').addClass('hidden');
        activityItem.find('span.activity-item-review-close-label').removeClass('hidden');
    }

    /**
     * Builds a link to trigger the action.
     *
     * @method buildLink
     * @param {Object} feedItem Object representing the activity item
     * @return {HTMLElement}
     */
    function buildLink(feedItem) {
        //if no such link exists in the feed item, do not bind the entry to a trigger handler
        if (!feedItem.links['http://streams.atlassian.com/syndication/review-close']) {
            return;
        }

        var link = AJS.$('<a href="#" class="activity-item-review-close-link"></a>')
                .text(AJS.I18n.getText('streams.fecru.action.review.close.title'))
                .bind('click', {feedItem: feedItem}, closeReview),
            label = AJS.$('<span class="activity-item-review-close-label hidden"></span>')
                .text(AJS.I18n.getText('streams.fecru.action.review.close.title'));

        return link.add(label);
    }

    // Registers the action for any reviews in the feed
    ActivityStreams.registerAction('review', buildLink, 5);
})();