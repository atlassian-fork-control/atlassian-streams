# Activity Streams

## Description

An activity stream is a list of events, displayed to a user in an attractive and interactive
interface. They are usually recent activities by the current user and other people using the same
social site(s) or system(s).

An example is the [Activity Stream
gadget](https://confluence.atlassian.com/display/JIRA/Adding+the+Activity+Stream+Gadget) used on
JIRA dashboards.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Branching
`master` for Jira 8.x development (Lucene changes, AUI8 with jquery 2.2.4).

`activity-stream-6.4.x` - jira 7.13.x and confluence 6.13.x development

## Atlassian Developer?

### Internal Documentation

[Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/STRM/Activity+Streams+Developers+Guide)

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Running tests locally

#### Unit tests

`mvn clean verify`

#### Product Integration tests

`mvn clean integration-test -DtestGroups=<product>`

Product values: `confluence`, `bamboo`, `jira` or `fecru` 

### Applinks Integration tests

`mvn clean integration-test -DtestGroups=applinks -Dproduct.start.timeout=600000`

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/STRM)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/STRM).

### Documentation

[Activity Streams Documentation](https://developer.atlassian.com/display/DOCS/Activity+Streams)

## Code coverage

To measure the percentage of production code that's covered by unit and (soon) integration tests, run this command:

```
mvn clean verify -Pjacoco -Dsonar.login=<your_SonarQube_API_key>
```

This will cause a few things to happen:
* JaCoCo will generate a number of `jacoco*.exec` files, which are binary files containing coverage data for UTs or ITs.
* JaCoCo will aggregate those files into a report named `sonar-aggregator/target/site/jacoco-aggregate/jacoco.xml`.
* JaCoCo will generate a web site at `sonar-aggregator/target/site/jacoco-aggregate/index.html` that you can browse.
* The SonarQube plugin will upload static analysis results (including the above coverage data) to the Atlassian
  SonarQube instance, [here](https://sonar-enterprise.internal.atlassian.com/dashboard?id=atlassian-streams).
  
If you don't need to upload to SonarQube or don't have an API key, you can simply replace that argument with
`-Dsonar.skip=true`.