package com.atlassian.streams.thirdparty.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.common.Option;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Validates properties of API objects.  This is used internally by the Builder classes
 * for each API class; see for example {@link Activity.Builder#build()}.  You should not
 * have to construct an instance of this class yourself.
 * <p>
 * General rules for all properties in this API:
 * <ul>
 * <li> You may not pass {@code null} for any property; this will cause an immediate
 * {@link NullPointerException}, unlike all other errors which are reported through a
 * {@link ValidationErrors} instance. </li>
 * <li> No string property may be longer than {@link #MAX_STRING_LENGTH}, except for the
 * {@code content} property of an {@link ActivityObject}, whose limit is {@link #MAX_CONTENT_LENGTH}. </li>
 * <li> No URI property may be longer than {@link #Max_STRING_LENGTH} when converted to an
 * ASCII string.</li>
 * <li> Some URI properties must be absolute URIs, as specified by the corresponding
 * setter method. </li>
 * <li> URI properties that do not have this constraint, if they are not absolute URIs,
 * must be simple names - that is, "foo" is valid, but "foo/bar" is not valid. </li>
 * </ul>
 */
public class ValidationErrors
{
    /**
     * The maximum length, in characters, of an {@link Activity}'s
     * {@link Activity#getContent() content} property.
     */
    public static final int MAX_CONTENT_LENGTH = 5000;

    /**
     * The maximum length, in characters, of all {@link Activity} and {@link ActivityObject}
     * properties other than {@link Activity#getContent()}.  This applies to both String and
     * URI properties.
     */
    public static final int MAX_STRING_LENGTH = 255;

    private static final Set<String> ALLOWED_URI_SCHEMES = ImmutableSet.of("http","https");

    private Iterable<String> messages;
    
    private ValidationErrors(Iterable<String> messages)
    {
        this.messages = messages;
    }
    
    public static Builder builder()
    {
        return new Builder();
    }
    
    public static ValidationErrors validationError(String message)
    {
        return new ValidationErrors(ImmutableList.of(message));
    }
    
    public Iterable<String> getMessages()
    {
        return messages;
    }
    
    @Override
    public String toString()
    {
        return Joiner.on("; ").join(messages);
    }
    
    public static class Builder
    {
        private ImmutableList.Builder<String> messages;
        
        public Builder addError(String message)
        {
            if (messages == null)
            {
                messages = new ImmutableList.Builder<String>();
            }
            messages.add(message);
            return this;
        }
        
        public Builder addAll(ValidationErrors from)
        {
            for (String message: from.getMessages())
            {
                addError(message);
            }
            return this;
        }

        public Builder addAll(ValidationErrors from, String subPropertyName)
        {
            for (String message: from.getMessages())
            {
                addError(subPropertyName + ": " + message);
            }
            return this;
        }
        
        public boolean isEmpty()
        {
            return (messages == null);
        }
        
        public ValidationErrors build()
        {
            return new ValidationErrors(messages.build());
        }
        
        public Option<Html> checkHtml(Option<Html> value, String propertyName, int maxLength)
        {
            checkNotNull(value, propertyName);
            if (value.isDefined())
            {
                checkString(value.get().toString(), propertyName, maxLength);
            }
            return value;
        }
        
        public String checkString(String value, String propertyName)
        {
            return checkString(value, propertyName, MAX_STRING_LENGTH);
        }
        
        public String checkString(String value, String propertyName, int maxLength)
        {
            checkNotNull(value, propertyName);
            if (value.length() > maxLength)
            {
                addError(propertyName + " exceeds maximum length of " + maxLength + " characters");
            }
            return value;
        }

        public Option<String> checkString(Option<String> value, String propertyName)
        {
            return checkString(value, propertyName, MAX_STRING_LENGTH);
        }
        
        public Option<String> checkString(Option<String> value, String propertyName, int maxLength)
        {
            if (value.isDefined())
            {
                checkString(value.get(), propertyName, maxLength);
            }
            return value;
        }
        
        public Option<URI> checkSimpleNameOrAbsoluteUri(Option<URI> optionalUri, String propertyName)
        {
            return checkUriInternal(optionalUri, propertyName, false);
        }

        public Option<URI> checkAbsoluteUri(Option<URI> optionalUri, String propertyName)
        {
            return checkUriInternal(optionalUri, propertyName, true);
        }

        public Option<URI> checkSimpleNameOrAbsoluteUriString(Option<String> optionalUriString, String propertyName)
        {
            return checkUriStringInternal(optionalUriString, propertyName, false);
        }

        public Option<URI> checkAbsoluteUriString(Option<String> optionalUriString, String propertyName)
        {
            return checkUriStringInternal(optionalUriString, propertyName, true);
        }

        private Option<URI> checkUriInternal(Option<URI> optionalUri, String propertyName, boolean mustBeAbsolute)
        {
            checkNotNull(optionalUri, propertyName);
            for (URI uri: optionalUri)
            {
                checkString(uri.toASCIIString(), propertyName, MAX_STRING_LENGTH);
                if (uri.isAbsolute())
                {
                    if(!ALLOWED_URI_SCHEMES.contains(uri.getScheme()))
                    {
                        addError(propertyName + " must start with a valid scheme (http/https).");
                    }
                }
                else
                {
                    if (mustBeAbsolute)
                    {
                        addError(propertyName + " must be an absolute URI");
                    }
                    else
                    {
                        if(uri.toASCIIString().contains("/"))
                        {
                            addError(propertyName + " must be either an absolute URI or a simple name, cannot contain slashes");
                        }
                    }
                }
            }
            return optionalUri;
        }
        
        private Option<URI> checkUriStringInternal(Option<String> optionalUriString, String propertyName, boolean mustBeAbsolute)
        {
            checkNotNull(optionalUriString, propertyName);
            for (String uriString: optionalUriString)
            {
                try
                {
                    return checkUriInternal(some(new URI(uriString)), propertyName, mustBeAbsolute);
                }
                catch (URISyntaxException e)
                {
                    addError(propertyName + " is not a valid URI");
                }
            }
            return none();
        }
    }
}
