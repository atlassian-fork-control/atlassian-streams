package com.atlassian.streams.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ActivityStreamConfiguration
{
    private static final By FILTER_OPTIONS = By.id("filter-options");

    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder pageBinder;

    @FindBy(id = "filter-options")
    private WebElement filterOptions;

    @FindBy(id = "stream-title-edit")
    private WebElement title;

    @FindBy(id = "numofentries")
    private WebElement numEntries;

    @FindBy(id = "add-global-filter")
    private WebElement addGlobalFilterLink;

    @WaitUntil
    public void waitUntilConfigurationIsVisible()
    {
        driver.waitUntilElementIsVisible(FILTER_OPTIONS);
    }

    public ActivityStreamConfiguration setTitle(String newTitle)
    {
        title.clear();
        title.sendKeys(newTitle);
        return this;
    }

    public ActivityStreamConfiguration setNumEntries(int newNumEntries)
    {
        numEntries.clear();
        numEntries.sendKeys(Integer.toString(newNumEntries));
        return this;
    }

    public ActivityStreamGadget save()
    {
        filterOptions.findElement(By.cssSelector("button.submit")).click();
        driver.waitUntilElementIsNotVisible(FILTER_OPTIONS);
        return pageBinder.bind(ActivityStreamGadget.class);
    }

    public FilterConfiguration addGlobalFilter()
    {
        addGlobalFilterLink.click();
        return pageBinder.bind(FilterConfiguration.class, "streams-global-filters");
    }
}
