package com.atlassian.streams.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.google.common.collect.Iterables.get;

public class FilterConfiguration
{
    @Inject AtlassianWebDriver driver;
    @Inject PageBinder pageBinder;
    
    private final String configId;

    public FilterConfiguration(String configId)
    {
        this.configId = configId;
    }
    
    @WaitUntil
    public void waitUntilFilterConfigurationIsVisible()
    {
        driver.waitUntilElementIsVisibleAt(By.className("smart-config"), driver.findElement(By.id(configId)));
    }
    
    public Filter getFirstFilter()
    {
        return get(getFilters(), 0);
    }

    private Iterable<Filter> getFilters()
    {
        return Lists.transform(
                driver.findElement(By.id(configId)).findElements(By.className("config-row")), 
                toFilter());
    }
    
    private Function<WebElement, Filter> toFilter()
    {
        return toFilterFunction;
    }
    
    private final Function<WebElement, Filter> toFilterFunction = new Function<WebElement, Filter>()
    {
        public Filter apply(WebElement e)
        {
            return pageBinder.bind(Filter.class, e);
        }
    };
}
