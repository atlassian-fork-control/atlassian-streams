package com.atlassian.streams.common.renderer;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.renderer.Renderers;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.streams.api.common.Fold.foldl;
import static com.atlassian.streams.api.common.Option.some;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

final class ActivityObjectRenderer implements Function<ActivityObject, Option<Html>>
{
    private final TemplateRenderer templateRenderer;
    private final boolean withSummary;

    public ActivityObjectRenderer(final TemplateRenderer templateRenderer, boolean withSummary)
    {
        this.templateRenderer = templateRenderer;
        this.withSummary = withSummary;
    }

    @Override
    public Option<Html> apply(final ActivityObject o)
    {
        return titleAsHtml(o).map(renderHtml(o));
    }

    private Function<Html, Html> renderHtml(final ActivityObject o)
    {
        return title -> new Html(Renderers.render(templateRenderer, "activity-object-link.vm",
                ImmutableMap.of("activityObject", o,
                        "title", title,
                        "summary", summaryAsHtml(o),
                        "withSummary", withSummary)));
    }

    public static Option<Html> titleAsHtml(ActivityObject o)
    {
        return foldl(o.getTitle(), o.getTitleAsHtml(), (title, titleAsHtml) -> some(new Html(escapeHtml4(title))));
    }

    public static Option<Html> summaryAsHtml(ActivityObject o)
    {
        return o.getSummary().map(summary -> new Html(escapeHtml4(summary)));
    }
}