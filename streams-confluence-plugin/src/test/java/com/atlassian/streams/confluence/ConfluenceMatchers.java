package com.atlassian.streams.confluence;

import java.util.Date;

import com.atlassian.streams.confluence.changereport.ActivityItem;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public abstract class ConfluenceMatchers
{
    public static Matcher<ActivityItem> changedBy(Matcher<? super String> matcher)
    {
        return new ChangedBy(matcher);
    }

    private static final class ChangedBy extends TypeSafeDiagnosingMatcher<ActivityItem>
    {
        private final Matcher<? super String> matcher;

        public ChangedBy(Matcher<? super String> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ActivityItem item, Description mismatchDescription)
        {
            if (!matcher.matches(item.getChangedBy()))
            {
                mismatchDescription.appendText("changed by ");
                matcher.describeMismatch(item.getChangedBy(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("changed by ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<ActivityItem> modified(Matcher<? super Date> matcher)
    {
        return new Modified(matcher);
    }

    private static final class Modified extends TypeSafeDiagnosingMatcher<ActivityItem>
    {
        private final Matcher<? super Date> matcher;

        public Modified(Matcher<? super Date> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ActivityItem item, Description mismatchDescription)
        {
            if (!matcher.matches(item.getModified()))
            {
                mismatchDescription.appendText("modified ");
                matcher.describeMismatch(item.getModified(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("modified ").appendDescriptionOf(matcher);
        }
    }

    public static Matcher<ActivityItem> type(Matcher<? super String> matcher)
    {
        return new Type(matcher);
    }

    private static final class Type extends TypeSafeDiagnosingMatcher<ActivityItem>
    {
        private final Matcher<? super String> matcher;

        public Type(Matcher<? super String> matcher)
        {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(ActivityItem item, Description mismatchDescription)
        {
            if (!matcher.matches(item.getType()))
            {
                mismatchDescription.appendText("type ");
                matcher.describeMismatch(item.getType(), mismatchDescription);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("type ").appendDescriptionOf(matcher);
        }
    }
}
