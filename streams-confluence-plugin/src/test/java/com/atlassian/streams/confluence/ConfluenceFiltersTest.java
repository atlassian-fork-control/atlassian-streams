package com.atlassian.streams.confluence;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.confluence.ConfluenceFilters.getSearchTerms;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceFiltersTest
{
    @Mock
    private ActivityRequest request;
    @Mock
    private Multimap<String, Pair<Operator, Iterable<String>>> standardFilters;

    @Before
    public void setup()
    {
        when(request.getStandardFilters()).thenReturn(standardFilters);
    }

    @Test
    public void testLabelsOfIssueKeys()
    {
        String[] isIssueKey = new String[] { "ONE-1" };
        ImmutableList<Pair<Operator, Iterable<String>>> issueKeys =
            ImmutableList.of(pair(IS, iterable(isIssueKey)));
        when(standardFilters.get(ISSUE_KEY.getKey())).thenReturn(issueKeys);

        assertThat(getSearchTerms(request), onlyHasItems("ONE-1"));
    }

    /**
     * Verifies that the specified elements exist and that no other elements do.
     */
    @SafeVarargs
    private static <T> Matcher<Iterable<T>> onlyHasItems(T... ts)
    {
        return allOf(size(ts), hasItems(ts));
    }

    /**
     * Need to break this out into separate function in order to properly cast generics type
     */
    @SafeVarargs
    private static <T> Matcher<Iterable<T>> size(T... ts)
    {
        return iterableWithSize(ts.length);
    }

    /**
     * Another workaround for generics/casting compilation problem
     */
    private static <T> Iterable<T> iterable(T[] array)
    {
        return ImmutableList.copyOf(array);
    }
}
