package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.api.model.pagination.PageRequest;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.people.User;
import com.atlassian.confluence.api.service.network.NetworkService;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.confluence.ConfluenceFilterOptionProvider;
import com.atlassian.streams.spi.Evictor;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ActivityItemFactoryTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AttachmentActivityItemFactory attachmentActivityItemFactory;
    @Mock
    private Evictor<ConfluenceEntityObject> evictor;
    @Mock
    private NetworkService networkService;
    @Mock
    private PageManager pageManager;
    @Mock
    private UserManager userManager;

    private List<ConfluenceEntityObject> searchables;
    private String commentTitleByAFollowedUser;
    private ConfluenceUser followedUser;
    private ConfluenceUser loggedInUser;
    private ConfluenceUser followingUser;
    private ConfluenceUser independentUser;
    private ActivityItemFactory activityItemFactory;

    @Before
    public void setUp()
    {
        ContentEntityActivityItemFactory contentEntityActivityItemFactory = mock(ContentEntityActivityItemFactory.class);
        activityItemFactory = new ActivityItemFactory(contentEntityActivityItemFactory,
                attachmentActivityItemFactory,
                pageManager,
                evictor,
                userManager,
                networkService);

        // People follow each other in chain: victoria -> depardieu (current logged-in user) -> richard
        // One user is independent: "che"
        followedUser = mockUser("richard").getUser();
        loggedInUser = mockUser("depardieu")
                .isLoggedIn()
                .follows(followedUser)
                .getUser();
        followingUser = mockUser("victoria")
                .follows(loggedInUser)
                .getUser();
        independentUser = mockUser("che").getUser();

        // Puts comments in the stream
        searchables = Lists.newArrayList();

        // Add a comment by a followed user
        commentTitleByAFollowedUser = "Comment by the followed user";
        searchables.add(createComment(contentEntityActivityItemFactory, followedUser, commentTitleByAFollowedUser));

        // Add comments by other users
        searchables.add(createComment(contentEntityActivityItemFactory, independentUser, "Comment by an independent user"));
        searchables.add(createComment(contentEntityActivityItemFactory, loggedInUser, "Comment by the logged-in user"));
        searchables.add(createComment(contentEntityActivityItemFactory, followingUser, "Comment by a user following the logged in user"));
    }

    private static List<String> usernamesOfComments(final Collection<ActivityItem> comments) {
        return comments.stream()
                .map(ActivityItem::getChangedBy)
                .collect(toList());
    }

    @Test
    public void testFilterMyNetwork()
    {
        // Launch the request
        List<ActivityItem> activityItems = Lists.newArrayList(activityItemFactory.getActivityItems(ImmutableList.of(), searchables,
                createActivityRequest(true, false)));

        assertEquals("The comment from the followed user should be returned",
                ImmutableList.of(followedUser.getName()), usernamesOfComments(activityItems));
        assertThat("The returned comment should have the title given by the user.", titles(activityItems), hasItem(commentTitleByAFollowedUser));
    }

    @Test
    public void testExcludeMyNetwork()
    {
        // Launch the request
        List<ActivityItem> activityItems = Lists.newArrayList(activityItemFactory.getActivityItems(ImmutableList.of(), searchables,
                createActivityRequest(false, true)));

        assertEquals("All comments should be returned except the followed user",
                ImmutableList.of(followingUser.getName(), loggedInUser.getName(), independentUser.getName()),
                usernamesOfComments(activityItems));

        assertThat("The returned comment should have the title given by the user.", titles(activityItems), not(hasItem(commentTitleByAFollowedUser)));
    }

    private static List<String> titles(Iterable<ActivityItem> commentReports)
    {
        List<String> titles = Lists.newArrayList();
        for (ActivityItem commentReport : commentReports)
        {
            for (ActivityObject ao : commentReport.getActivityObjects())
            {
                Option<String> title = ao.getTitle();
                if (title.isDefined())
                {
                    titles.add(title.get());
                }
            }
        }
        return titles;
    }

    /* Keep created comments monotonic */
    private long nextDate = 0;

    private Comment createComment(ContentEntityActivityItemFactory contentEntityActivityItemFactory, ConfluenceUser author, String title)
    {
        Comment comment = new Comment();
        comment.setTitle(title);
        comment.setCreator(author);
        comment.setCreationDate(new Date(nextDate++));

        // Mock ActivityObjectType
        ActivityObjectType activityObjectType = mock(ActivityObjectType.class);

        // Create the activity
        ActivityObject commentActivity = new ActivityObject(ActivityObject.params()
                .title(some(title))
                .activityObjectType(activityObjectType));

        // Create the activity report
        CommentActivityItem commentReport = new CommentActivityItem(comment,
                Lists.newArrayList(commentActivity),
                none(ActivityObject.class),
                null,
                Predicates.alwaysFalse());

        when(contentEntityActivityItemFactory.newActivityItem(any(), eq(comment))).thenReturn(commentReport);
        return comment;
    }

    /**
     * Creates an activity request. It mocks the request that is usually created by the gadget.
     *
     * @param onlyFollowedUsers
     *            if true, will add a provider filter, to include only people followed by the logged in user.
     * @param excludeFollowedUsers
     *            if true, will add a provider filter, to exclude people followed by the logged in user. If onlyFollowedUsers is true,
     *            excludeFollowedUsers will not be read.
     */
    private static ActivityRequest createActivityRequest(boolean onlyFollowedUsers, boolean excludeFollowedUsers)
    {
        ActivityRequest activityRequest = mock(ActivityRequest.class);
        when(activityRequest.getMaxResults()).thenReturn(10);
        when(activityRequest.getStandardFilters()).thenReturn(ArrayListMultimap.create());

        Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilters = ArrayListMultimap.create();
        // Add filters if necessary
        if (onlyFollowedUsers)
        {
            providerFilters.put(
                    ConfluenceFilterOptionProvider.NETWORK_FILTER,
                    Pair.pair(
                            StreamsFilterType.Operator.IS,
                            Lists.newArrayList(ConfluenceFilterOptionProvider.NETWORK_FILTER_OPTION_FOLLOWED)));
        }
        else if (excludeFollowedUsers)
        {
            providerFilters.put(
                    ConfluenceFilterOptionProvider.NETWORK_FILTER,
                    Pair.pair(
                            StreamsFilterType.Operator.NOT,
                            Lists.newArrayList(ConfluenceFilterOptionProvider.NETWORK_FILTER_OPTION_FOLLOWED)));
        }
        when(activityRequest.getProviderFilters()).thenReturn(providerFilters);
        return activityRequest;
    }

    /**
     * Mocks a user and allows you to define more attributes
     */
    private UserMocker mockUser(String username)
    {
        return new UserMocker(userManager, networkService, username);
    }

    /**
     * Class which provides methods to mock a user
     */
    private static class UserMocker
    {
        private final UserKey userKey;
        private final ConfluenceUser user;
        private final UserManager userManager;
        private final NetworkService networkService;

        UserMocker(UserManager userManager, NetworkService networkService, String username)
        {
            this.userManager = userManager;
            this.networkService = networkService;
            this.userKey = new UserKey(username);
            this.user = mock(ConfluenceUser.class);
            when(user.getName()).thenReturn(username);
            when(user.getKey()).thenReturn(userKey);
        }

        UserMocker isLoggedIn()
        {
            when(userManager.getRemoteUserKey()).thenReturn(userKey);
            return this;
        }

        UserMocker follows(ConfluenceUser followedUser)
        {
            List<User> followers = singletonList(
                    new User(null, followedUser.getName(), followedUser.getFullName(), followedUser.getKey())
            );
            @SuppressWarnings("unchecked")
            final PageResponse<User> pr = mock(PageResponse.class);
            when(pr.getResults()).thenReturn(followers);
            when(networkService.getFollowers(eq(user.getKey()), any(PageRequest.class))).thenReturn(pr);
            return this;
        }

        public ConfluenceUser getUser()
        {
            return user;
        }
    }
}
