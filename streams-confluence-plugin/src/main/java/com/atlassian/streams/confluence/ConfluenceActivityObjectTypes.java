package com.atlassian.streams.confluence;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityObjectTypes.TypeFactory;

import static com.atlassian.streams.api.ActivityObjectTypes.ATLASSIAN_IRI_BASE;
import static com.atlassian.streams.api.ActivityObjectTypes.newTypeFactory;

public final class ConfluenceActivityObjectTypes
{
    private static final TypeFactory confluenceTypes = newTypeFactory(ATLASSIAN_IRI_BASE);

    public static ActivityObjectType mail()
    {
        return confluenceTypes.newType("mail");
    }

    public static ActivityObjectType page()
    {
        return confluenceTypes.newType("page");
    }

    public static ActivityObjectType space()
    {
        return confluenceTypes.newType("space");
    }

    public static ActivityObjectType personalSpace()
    {
        return confluenceTypes.newType("personal-space", space());
    }
}
