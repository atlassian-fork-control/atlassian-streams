package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.streams.api.common.Function2;
import com.atlassian.streams.spi.BoundedTreeSet;
import com.atlassian.streams.spi.Evictor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Comparator;

import static com.atlassian.streams.api.common.Fold.foldl;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link BoundedTreeSet} with additional functionality to clear removed {@link ActivityItem}s
 * from the current Hibernate session.
 */
public class BoundedActivityItemTreeSet extends BoundedTreeSet<ActivityItem>
{
    private static final Logger log = LoggerFactory.getLogger(BoundedActivityItemTreeSet.class);
    private final Evictor<ConfluenceEntityObject> evictor;

    public BoundedActivityItemTreeSet(Evictor<ConfluenceEntityObject> evictor, int maxSize, Comparator<ActivityItem> activityItemComparator)
    {
        super(maxSize, activityItemComparator);
        this.evictor = checkNotNull(evictor, "evictor");
    }

    @Override
    public boolean remove(Object o)
    {
        if (o instanceof ActivityItem)
        {
            return remove((ActivityItem)o);
        }
        else
        {
            log.warn("Attempting to remove a non-ActivityItem from the set");
            return super.remove(o);
        }
    }

    public boolean remove(ActivityItem activityItem)
    {
        evictor.apply(activityItem.getEntity());
        return super.remove(activityItem);
    }

    /**
     * Unused by Streams, but implemented to ensure that anything removed is also removed from the session.
     */
    @Override
    public boolean removeAll(Collection<?> os)
    {
        return foldl(ImmutableList.<Object>copyOf(os), false, (o, anyRemoved) -> remove(o) || anyRemoved);
    }

    /**
     * Unused by Streams, but implemented to ensure that anything removed is also removed from the session.
     */
    @Override
    public void clear()
    {
        removeAll(ImmutableSet.copyOf(this));
    }

    /**
     * Unused by Streams, but implemented to ensure that anything removed is also removed from the session.
     */
    @Override
    public boolean retainAll(final Collection<?> os)
    {
        return foldl(ImmutableList.copyOf(this), false, (Function2<Object, Boolean, Boolean>) (o, anyRemoved) -> {
            if (!os.contains(o))
            {
                return remove(o) || anyRemoved;
            }
            else
            {
                return anyRemoved;
            }
        });
    }
}
