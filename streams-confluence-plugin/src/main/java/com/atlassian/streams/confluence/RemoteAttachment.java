package com.atlassian.streams.confluence;

import com.atlassian.confluence.pages.Attachment;

public class RemoteAttachment
{
    private String downloadUrl;
    private String previewUrl = "";
    private boolean hasPreview = false;
    private String comment;
    private String name;
    private long size;
    private int height = 0, width = 0;

    public RemoteAttachment(Attachment attachment)
    {
        downloadUrl = attachment.getDownloadPath();
        comment = attachment.getVersionComment();
        name = attachment.getFileName();
        size = attachment.getFileSize();
    }

    public void setPreview(String url, int height, int width)
    {
        previewUrl = url;
        this.height = height;
        this.width = width;
        hasPreview = true;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl)
    {
        this.downloadUrl = downloadUrl;
    }

    public String getPreviewUrl()
    {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl)
    {
        this.previewUrl = previewUrl;
    }

    public boolean hasPreview()
    {
        return hasPreview;
    }

    public void setHasPreview(boolean hasPreview)
    {
        this.hasPreview = hasPreview;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getSize()
    {
        return size;
    }

    public void setSize(long size)
    {
        this.size = size;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }
}
