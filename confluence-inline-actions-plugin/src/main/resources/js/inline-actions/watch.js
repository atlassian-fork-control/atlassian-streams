/**
 * Registers a "Watch page" action against any feed items with a "page" type.
 *
 * Creates a link which watches a page for the specified plan.
 */
(function() {
    /**
     * Watch a page or space in Confluence.
     * 
     * @method watchEntity
     * @param {Event} e Event object
     */
    function watchEntity(e) {
        var target = AJS.$(e.target),
            activityItem = target.closest('div.activity-item'),
            url,
            feedItem = e.data && e.data.feedItem;
        
        if (feedItem) {
            url = feedItem.links['http://streams.atlassian.com/syndication/watch'];
        } else {
            return;
        }
        
        e.preventDefault();
        hideLink(activityItem);
        
        AJS.$.ajax({
            type : 'POST',
            contentType: 'application/json',
            url : ActivityStreams.InlineActions.proxy(url, feedItem),
            global: false,
            beforeSend: function() {
                target.trigger('beginInlineAction');
            },
            complete: function() {
                target.trigger('completeInlineAction');
            },
            success : function() {
                ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.confluence.action.watch.success'), 'info');
            },            
            error : function(request) {
                var msg;
                //check both request.status and request.rc for backwards compatibility
                if (request.status == 401 || request.rc == 401) {
                    msg = AJS.I18n.getText('streams.confluence.action.watch.failure.authentication');
                } else if (request.status == 412 || request.rc == 412) {
                    msg = AJS.I18n.getText('streams.confluence.action.watch.failure.precondition.failed');
                } else {
                    msg = AJS.I18n.getText('streams.confluence.action.watch.failure.general');
                }
                ActivityStreams.InlineActions.statusMessage(activityItem, msg, 'error');
            }
        });
    }
    
    /**
     * Hide the action link, showing the non-hyperlinked label instead.
     * 
     * @method hideLink
     * @param {Object} activityItem the .activity-item div
     */
    function hideLink(activityItem) {
        activityItem.find('a.activity-item-watch-link').addClass('hidden');
        activityItem.find('span.activity-item-watch-label').removeClass('hidden');
    }
    
    /**
     * Builds a "Watch" link that triggers the action
     *
     * @method buildWatchLink
     * @param {Object} feedItem Object representing the activity item
     * @return {HTMLElement}
     */
    function buildWatchLink(feedItem) {
        //if no page-watch link exists in the feed item, do not bind the entry to a trigger handler
        if (!feedItem.links['http://streams.atlassian.com/syndication/watch']) {
            return;
        } 
        
        var link = AJS.$('<a href="#" class="activity-item-watch-link"></a>')
                .text(AJS.I18n.getText('streams.confluence.action.watch.title'))
                .bind('click', {feedItem: feedItem}, watchEntity),
            label = AJS.$('<span class="activity-item-watch-label hidden"></span>')
                .text(AJS.I18n.getText('streams.confluence.action.watch.title'));
        
        return link.add(label);
    }

    // Registers the trigger action for any pages and spaces in the feed
    ActivityStreams.registerAction('page article space personal-space', buildWatchLink, 5);
})();
