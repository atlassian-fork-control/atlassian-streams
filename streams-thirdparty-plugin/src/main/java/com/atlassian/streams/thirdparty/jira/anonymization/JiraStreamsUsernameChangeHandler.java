package com.atlassian.streams.thirdparty.jira.anonymization;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.anonymize.AffectedEntity;
import com.atlassian.jira.user.anonymize.AffectedEntityType;
import com.atlassian.jira.user.anonymize.UserNameChangeHandler;
import com.atlassian.jira.user.anonymize.UserPropertyChangeParameter;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.ao.ActivityEntity;
import com.google.common.collect.ImmutableList;
import net.java.ao.Query;
import net.java.ao.RawEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

/**
 * Handles user anonymization in Jira by updating user names in the ActivityEntity table.
 *
 * @since v8.1.6
 */
@ParametersAreNonnullByDefault
public class JiraStreamsUsernameChangeHandler implements UserNameChangeHandler {
    private static final Logger log = LoggerFactory.getLogger(JiraStreamsUsernameChangeHandler.class);

    private final ActiveObjects ao;
    private final StreamsI18nResolver i18nResolver;

    public JiraStreamsUsernameChangeHandler(ActiveObjects ao, StreamsI18nResolver i18nResolver) {
        this.ao = requireNonNull(ao);
        this.i18nResolver = requireNonNull(i18nResolver);
    }

    @Nonnull
    @Override
    public Collection<AffectedEntity> getAffectedEntities(UserPropertyChangeParameter changeParameter) {
        int affectedEntitiesCount = getAffectedEntitiesCount(changeParameter.getOriginal());

        if (affectedEntitiesCount > 0) {
            return ImmutableList.of(
                    AffectedEntity.newBuilder(AffectedEntityType.ANONYMIZE)
                            .numberOfOccurrences((long) affectedEntitiesCount)
                            .descriptionKey("streams.jira.username_change_handler.entry")
                            .build()
            );
        } else {
            return Collections.emptyList();
        }
    }

    private int getAffectedEntitiesCount(String originalUserName) {
        final int posters = ao.count(ActivityEntity.class, getQueryForPoster(originalUserName));
        final int userNames = ao.count(ActivityEntity.class, getQueryForUsername(originalUserName));

        return posters + userNames;
    }

    @Nonnull
    @Override
    public ServiceResult update(UserPropertyChangeParameter changeParameter) {
        final String originalName = changeParameter.getOriginal();
        final String targetName = changeParameter.getTarget();
        final ErrorHandlingRunner runner = new ErrorHandlingRunner(new SimpleErrorCollection(), changeParameter.getContext());

        updateActiveEntityField(runner, getQueryForPoster(originalName), e -> e.setPoster(targetName));
        updateActiveEntityField(runner, getQueryForUsername(originalName), e -> e.setUsername(targetName));

        return new ServiceResultImpl(runner.getErrorCollection());
    }

    @Override
    public int getNumberOfTasks(UserPropertyChangeParameter changeParameter) {
        return getAffectedEntitiesCount(changeParameter.getOriginal());
    }

    @Nonnull
    private Query getQueryForPoster(String username) {
        return Query.select().where("POSTER = ?", username);
    }

    @Nonnull
    private Query getQueryForUsername(String username) {
        return Query.select().where("USERNAME = ?", username);
    }

    private void updateActiveEntityField(ErrorHandlingRunner runner, Query query, Consumer<ActivityEntity> updateEntity) {
        final Set<Long> ids = new LinkedHashSet<>();
        ao.stream(ActivityEntity.class, query, activityEntity -> ids.add(activityEntity.getActivityId()));

        for (final long id : ids) {
            final ActivityEntity entity = runner.execute(() -> ao.get(ActivityEntity.class, id));

            if (entity == null) {
                log.warn("Wanted to update username in activity entity with id {}, but failed to retrieve it", id);
                continue;
            }
            runner.executeAndUpdateProgress(() -> updateEntity.andThen(RawEntity::save).accept(entity));
        }
    }

    @ParametersAreNonnullByDefault
    private class ErrorHandlingRunner {
        private final ErrorCollection errorCollection;
        private final Context context;

        private ErrorHandlingRunner(ErrorCollection errorCollection, Context context) {
            this.errorCollection = requireNonNull(errorCollection);
            this.context = requireNonNull(context);
        }

        @Nullable
        ActivityEntity execute(Callable<ActivityEntity> callable) {
            try {
                return callable.call();
            } catch (Exception e) {
                log.error("There was an exception during username change in streams thirdparty plugin", e);
                errorCollection.addErrorMessage(i18nResolver.getText("streams.jira.change.handler.processing.exception", e.getMessage()));
                return null;
            }
        }

        void executeAndUpdateProgress(Runnable runnable) {
            try {
                execute(Executors.callable(runnable, null));
            } finally {
                context.start(null).complete();
            }
        }

        @Nonnull
        ErrorCollection getErrorCollection() {
            return errorCollection;
        }
    }
}
