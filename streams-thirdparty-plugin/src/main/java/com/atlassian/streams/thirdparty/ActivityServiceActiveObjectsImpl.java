package com.atlassian.streams.thirdparty;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.FeedContentSanitizer;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.streams.thirdparty.ActivityServiceActiveObjectsImpl.WhereClause.Operator;
import com.atlassian.streams.thirdparty.ActivityServiceActiveObjectsImpl.WhereClause.Separator;
import com.atlassian.streams.thirdparty.ao.ActivityEntity;
import com.atlassian.streams.thirdparty.ao.ActivityObjEntity;
import com.atlassian.streams.thirdparty.ao.ActorEntity;
import com.atlassian.streams.thirdparty.ao.MediaLinkEntity;
import com.atlassian.streams.thirdparty.ao.ObjectEntity;
import com.atlassian.streams.thirdparty.ao.TargetEntity;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;
import com.atlassian.streams.thirdparty.api.ValidationErrors;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import net.java.ao.Query;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.renderer.Renderers.replaceTextWithHyperlink;
import static com.atlassian.streams.thirdparty.api.Application.application;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;

public class ActivityServiceActiveObjectsImpl implements ActivityServiceActiveObjects
{
    /**
     * Maximum number of characters allowed for the activity title.
     */
    public static final int MAX_TITLE_LENGTH = 255;

    /**
     * Maximum number of characters allowed for the activity content (or activity object content).
     */
    public static final int MAX_CONTENT_LENGTH = 5000;
    
    /**
     * Maximum number of characters allowed for all string properties not otherwise specified.
     */
    public static final int MAX_STRING_LENGTH = 255;
    
    private static final Logger log = LoggerFactory.getLogger(ActivityServiceActiveObjectsImpl.class);

    private final ActiveObjects ao;
    private final EntityAssociationProviders entityAssociationProviders;
    private final FeedContentSanitizer sanitizer;
    private final UserManager userManager;
    private final UserProfileAccessor userProfileAccessor;
    private final ApplicationProperties applicationProperties;
    
    public ActivityServiceActiveObjectsImpl(ActiveObjects ao,
                                            EntityAssociationProviders entityAssociationProviders,
                                            FeedContentSanitizer sanitizer,
                                            UserManager userManager,
                                            UserProfileAccessor userProfileAccessor,
                                            ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        this.ao = checkNotNull(ao, "ao");
        this.entityAssociationProviders = checkNotNull(entityAssociationProviders, "entityAssociationProviders");
        this.sanitizer = checkNotNull(sanitizer, "sanitizer");
        this.userManager = checkNotNull(userManager, "userManager");
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
    }

    @Override
    public Activity postActivity(Activity activity)
    {
        final ActivityEntity entity = ao.create(ActivityEntity.class);

        entity.setPublished(activity.getPostedDate().toDate());
        entity.setGeneratorId(activity.getApplication().getId());
        entity.setGeneratorDisplayName(activity.getApplication().getDisplayName());

        Option<String> registeredUsername = getRegisteredUsername(activity.getUser().getUsername());
        if (registeredUsername.isDefined())
        {
            entity.setUsername(registeredUsername.get());
            entity.setActor(null);
        }
        else
        {
            entity.setUsername(null);
            entity.setActor(actorEntity(activity.getUser()));
        }

        if (userManager.getRemoteUsername() != null)
        {
            entity.setPoster(userManager.getRemoteUsername());
        }

        entity.setId(activity.getId().getOrElse((URI) null));
        
        for (Html html : activity.getContent())
        {
            entity.setContent(sanitizer.sanitize(html.toString()));
        }
        for (Image image : activity.getIcon())
        {
            entity.setIcon(mediaLinkEntity(image));
        }
        for (ActivityObject object : activity.getObject())
        {
            entity.setObject(objectEntity(object, ObjectEntity.class));
        }

        Iterable<EntityIdentifier> entityIdentifiers = getEntityIdentifiers(activity);
        if (isEmpty(entityIdentifiers))
        {
            for (ActivityObject target: activity.getTarget())
            {
                entity.setTarget(objectEntity(target, TargetEntity.class));
            }
        }
        else
        {
            Option<EntityIdentifier> firstIdentifier = none();
            for (EntityIdentifier entityIdentifier: entityIdentifiers)
            {
                if (!entityAssociationProviders.getCurrentUserViewPermission(entityIdentifier))
                {
                    continue;
                }
                Option<String> filterKey = entityAssociationProviders.getFilterKey(entityIdentifier);
                if (filterKey.equals(some(PROJECT_KEY)))
                {
                    entity.setProjectKey(entityIdentifier.getValue());
                }
                else if (filterKey.equals(some(ISSUE_KEY.getKey())))
                {
                    entity.setIssueKey(entityIdentifier.getValue());
                }
                if (!firstIdentifier.isDefined())
                {
                    firstIdentifier = some(entityIdentifier);
                }
            }
            for (EntityIdentifier entityIdentifier: firstIdentifier)
            {
                entity.setTarget(objectEntity(entityIdentifier, TargetEntity.class));
            }
        }

        for (Html html : activity.getTitle())
        {
            entity.setTitle(sanitizer.sanitize(html.toString()));
        }
        
        entity.setUrl(activity.getUrl().getOrElse((URI) null));
        entity.setVerb(activity.getVerb().getOrElse((URI) null));
        
        entity.save();

        Option<Activity> newActivity = getActivity(entity.getActivityId());
        if (!newActivity.isDefined())
        {
            throw new IllegalStateException("Newly posted activity could not be retrieved");
        }
        return newActivity.get();
    }

    @Override
    public Option<Activity> getActivity(long activityId)
    {
        for (ActivityEntity entity : getEntity(activityId))
        {
            return toActivity.apply(entity);
        }
        return none();
    }

    @Override
    public boolean delete(long activityId)
    {
        for (ActivityEntity entity : getEntity(activityId))
        {
            delete(entity);
            return true;
        }
        return false;
    }

    @Override
    public Iterable<Activity> activities(ActivityQuery query)
    {
        Query aoQuery = Query.select();

        WhereClause where = whereClause(Separator.AND)
            .add(query.getStartDate(), "published", Operator.GT_EQ)
            .add(query.getEndDate(), "published", Operator.LT);

        if (!isEmpty(query.getUserNames()))
        {
            WhereClause namesClause = whereClause(Separator.OR);
            for (String name: query.getUserNames())
            {
                namesClause.add(some(name), "username", Operator.EQ);
            }
            where.add(namesClause);
        }

        if (!isEmpty(query.getExcludeUserNames()))
        {
            WhereClause excludesClause = whereClause(Separator.AND);
            for (String excludeName: query.getExcludeUserNames())
            {
                excludesClause.add(some(excludeName), "username", Operator.NEQ);
            }
            WhereClause excludesOrNull = whereClause(Separator.OR);
            excludesOrNull.add(excludesClause);
            excludesOrNull.addIsNull("username");
            where.add(excludesOrNull);
        }

        addEntityFiltersClause(where, query, PROJECT_KEY, "projectKey");
        addEntityFiltersClause(where, query, ISSUE_KEY.getKey(), "issueKey");

        if (!isEmpty(query.getProviderKeys()) || !isEmpty(query.getExcludeProviderKeys()))
        {
            if (!isEmpty(query.getProviderKeys()))
            {
                WhereClause providersClause = whereClause(Separator.OR);
                for (String providerKey: query.getProviderKeys())
                {
                    String[] parts = providerKey.split("@");
                    providersClause.add(whereClause(Separator.AND)
                                        .add(some(parts[0]), "generatorId", Operator.EQ)
                                        .add(some(parts[1]), "generatorDisplayName", Operator.EQ));
                }
                where.add(providersClause);
            }
            for (String excludeProviderKey: query.getExcludeProviderKeys())
            {
                String[] parts = excludeProviderKey.split("@");
                where.add(whereClause(Separator.OR)
                          .add(some(parts[0]), "generatorId", Operator.NEQ)
                          .add(some(parts[1]), "generatorDisplayName", Operator.NEQ));
            }
        }
        
        if (!where.isEmpty())
        {
            aoQuery.where(where.getClause(), where.getParams());
        }

        aoQuery.setLimit(query.getMaxResults());
        aoQuery.setOffset(query.getStartIndex());
        aoQuery.setOrderClause(getColumnName("published") + " desc"); //reverse chronological

        Iterable<ActivityEntity> entities = ImmutableList.copyOf(ao.find(ActivityEntity.class, aoQuery));

        return catOptions(transform(entities, toActivity));
    }

    @Override
    public Iterable<Application> applications() {
        Query query = Query
                .select("GENERATOR_ID, GENERATOR_DISPLAY_NAME")
                .group("GENERATOR_ID, GENERATOR_DISPLAY_NAME")
                .order("GENERATOR_ID DESC");
        List<Application> applications = new LinkedList<>();
        ao.stream(ActivityEntity.class, query, (app) -> {
            applications.add(toApplication.apply(app));
        });

        return applications;
    }

    private Iterable<EntityIdentifier> getEntityIdentifiers(Activity activity)
    {
        for (ActivityObject target: activity.getTarget())
        {
            for (URI targetUri: target.getUrl())
            {
                return entityAssociationProviders.getEntityAssociations(targetUri);
            }
        }
        return ImmutableList.of();
    }

    private void addEntityFiltersClause(WhereClause where, ActivityQuery query, String filterKey, String propertyName)
    {
        WhereClause matchClause = whereClause(Separator.OR);
        for (Pair<String, String> filter: query.getEntityFilters())
        {
            if (filter.first().equals(filterKey))
            {
                matchClause.add(some(filter.second()), propertyName, Operator.EQ);
            }
        }
        if (!matchClause.isEmpty())
        {
            where.add(matchClause);
        }

        WhereClause excludeClause = whereClause(Separator.AND);
        for (Pair<String, String> filter: query.getExcludeEntityFilters())
        {
            if (filter.first().equals(filterKey))
            {
                excludeClause.add(some(filter.second()), propertyName, Operator.NEQ);
            }
        }
        if (!excludeClause.isEmpty())
        {
            WhereClause excludesOrNull = whereClause(Separator.OR);
            excludesOrNull.add(excludeClause);
            excludesOrNull.addIsNull(propertyName);
            where.add(excludesOrNull);
        }
    }

    private Option<ActivityEntity> getEntity(long activityId)
    {
        return option(ao.get(ActivityEntity.class, activityId));
    }

    private void delete(ActivityEntity entity)
    {
        if (entity != null)
        {
            // @PreLoad does not work with @Polymorphic - force the loading
            final TargetEntity target = entity.getTarget();
            final ActorEntity actor = entity.getActor();
            final ObjectEntity object = entity.getObject();
            final MediaLinkEntity icon = entity.getIcon();
            ao.delete(entity);
            delete(target);
            delete(actor);
            delete(object);
            delete(icon);
        }
    }

    private <T extends ActivityObjEntity> void delete(T entity)
    {
        if (entity != null)
        {
            ao.delete(entity);
            delete(entity.getImage());
        }
    }

    private void delete(ActorEntity entity)
    {
        if (entity != null)
        {
            ao.delete(entity);
        }
    }

    private void delete(MediaLinkEntity entity)
    {
        if (entity != null)
        {
            ao.delete(entity);
        }
    }

    private ActorEntity actorEntity(UserProfile user)
    {
        ActorEntity entity = ao.create(ActorEntity.class);
        entity.setUsername(user.getUsername());
        entity.setFullName(user.getFullName());
        for (URI profileUri: user.getProfilePageUri())
        {
            entity.setProfilePageUri(profileUri);
        }
        for (URI pictureUri: user.getProfilePictureUri())
        {
            entity.setProfilePictureUri(pictureUri);
        }
        entity.save();
        return entity;
    }

    private MediaLinkEntity mediaLinkEntity(Image image)
    {
        MediaLinkEntity entity = ao.create(MediaLinkEntity.class);
        entity.setUrl(image.getUrl());
        entity.setHeight(image.getHeight().getOrElse((Integer) null));
        entity.setWidth(image.getWidth().getOrElse((Integer) null));
        entity.save();
        return entity;
    }
    
    private <T extends ActivityObjEntity> T objectEntity(ActivityObject activityObject,
                                                         Class<T> entityClass)
    {
        T entity = ao.create(entityClass);

        for (URI id : activityObject.getId())
        {
            entity.setObjectId(id);
        }
        for (String s : activityObject.getDisplayName())
        {
            entity.setDisplayName(stripHtml(s));
        }
        for (URI type : activityObject.getType())
        {
            entity.setObjectType(type);
        }
        for (Html html : activityObject.getSummary())
        {
            entity.setSummary(sanitizer.sanitize(html.toString()));
        }
        for (URI u : activityObject.getUrl())
        {
            entity.setUrl(u);
        }
        entity.save();

        return entity;
    }

    private <T extends ActivityObjEntity> T objectEntity(EntityIdentifier entityIdentifier,
                                                         Class<T> entityClass)
    {
        T entity = ao.create(entityClass);

        entity.setDisplayName(entityIdentifier.getValue());
        entity.setObjectType(entityIdentifier.getType());
        entity.setUrl(entityIdentifier.getUri());
        
        entity.save();

        return entity;
    }

    private Function<ActivityEntity, Option<Activity>> toActivity = new Function<ActivityEntity, Option<Activity>>()
    {
        @Override
        public Option<Activity> apply(ActivityEntity entity)
        {
            if (entity == null || entity.getPublished() == null)
            {
                return none();
            }

            try
            {
                // Check the associated application entity (issue/project/etc.) before we do
                // anything else, because if the user doesn't have permission to view that entity,
                // we don't want to go any further.
                Option<EntityIdentifier> entityIdentifier = none();
                Option<String> entityLinkText = none();
                Option<URI> entityLinkUri = null;
                if (entity.getTarget() != null)
                {
                    URI objectType = entity.getTarget().getObjectType();
                    if (entity.getIssueKey() != null || entity.getProjectKey() != null)
                    {
                        EntityIdentifier ei = new EntityIdentifier(objectType,
                                                                   (entity.getIssueKey() != null) ? entity.getIssueKey() : entity.getProjectKey(),
                                                                   entity.getTarget().getUrl());
                        if (!entityAssociationProviders.getCurrentUserViewPermission(ei))
                        {
                            return none();
                        }
                        // Recompute the entity's URL, to make sure it stays in sync with the app's base URL.
                        entityLinkUri = entityAssociationProviders.getEntityURI(ei);
                        if (entityLinkUri.isDefined() && !entityLinkUri.get().equals(ei.getUri()))
                        {
                            ei = new EntityIdentifier(ei.getType(), ei.getValue(), entityLinkUri.get());
                        }
                        entityIdentifier = some(ei);                        
                        entityLinkText = some(ei.getValue());
                    }
                }
                
                Application application = application(entity.getGeneratorDisplayName(),
                                                      entity.getGeneratorId());
    
                UserProfile userProfile = getUserProfile(URI.create(applicationProperties.getBaseUrl()),
                        entity.getUsername(), entity.getActor());
                
                Option<Html> title = option(entity.getTitle()).map(html());
                Option<Html> content = option(entity.getContent()).map(html());
                
                Activity.Builder builder = Activity.builder(application,
                                                            new DateTime(entity.getPublished()),
                                                            userProfile)
                    .activityId(entity.getActivityId())
                    .id(option(entity.getId()))
                    .url(option(entity.getUrl()))
                    .poster(option(entity.getPoster()))
                    .registeredUser(entity.getUsername() != null)
                    .verb(option(entity.getVerb()));
    
                if (entity.getIcon() != null)
                {
                    builder.icon(Image.builder(entity.getIcon().getUrl())
                        .height(option(entity.getIcon().getHeight()))
                        .width(option(entity.getIcon().getWidth()))
                        .build());
                }
                if (entity.getObject() != null)
                {
                    builder.object(activityObjectBuilder(entity.getObject()).build());
                }
                
                if (entity.getTarget() != null)
                {
                    ActivityObject.Builder targetBuilder = activityObjectBuilder(entity.getTarget());

                    if (entityIdentifier.isDefined())
                    {
                        targetBuilder.url(some(entityIdentifier.get().getUri()));
                    }
                    
                    builder.target(targetBuilder.build());
                }
                
                if (entityLinkText.isDefined() && entityLinkUri.isDefined())
                {
                    // Hyperlink any occurrences of the linked entity's name in the title or content to its URL.
                    Function<Html, Html> addEntityLinks = replaceTextWithHyperlink(entityLinkText.get(), entityLinkUri.get());
                    title = title.map(addEntityLinks);
                    content = content.map(addEntityLinks);
                }

                if (entity.getUsername() != null)
                {
                    // Hyperlink any occurrences of the user's full name in the title or content to the user profile URL,
                    // if the latter exists.
                    for (URI profileUri: userProfile.getProfilePageUri())
                    {
                        Function<Html, Html> addUserLinks = replaceTextWithHyperlink(
                                sanitizer.sanitize(userProfile.getFullName()),
                                profileUri);
                        title = title.map(addUserLinks);
                        content = content.map(addUserLinks);
                    }
                }
                
                builder.title(title).content(content);
                
                Either<ValidationErrors, Activity> ret = builder.build();
                if (ret.isRight())
                {
                    return ret.right().toOption();
                }
                log.warn("Ignoring invalid activity in database (id=" + entity.getActivityId() + "): "
                         + ret.left().get().toString());
            }
            catch (Exception e)
            {
                log.warn("Unexpected error when retrieving activity (id=" + entity.getActivityId() + "): ", e);
            }
            return none();
        }
    };
    
    private UserProfile getUserProfile(URI baseUri, String username, ActorEntity actor)
    {
        if (username != null)
        {
            UserProfile userProfile = userProfileAccessor.getUserProfile(baseUri, username);
            if (userProfile != null)
            {
                return userProfile;
            }
        }

        // Use the provider's anonymous user profile by default, but allow its properties
        // to be overridden by the activity's actor properties, if present.
        UserProfile defaultProfile = userProfileAccessor.getAnonymousUserProfile(baseUri);

        if (actor == null)
        {
            return defaultProfile;
        }
        
        UserProfile.Builder builder = new UserProfile.Builder(actor.getUsername().equals("") ?
            defaultProfile.getUsername() : actor.getUsername());
        builder.fullName((actor.getFullName() == null || actor.getFullName().equals("")) ?
            defaultProfile.getFullName() : actor.getFullName());
        if (actor.getProfilePictureUri() != null)
        {
            builder.profilePictureUri(some(actor.getProfilePictureUri()));
        }
        else
        {
            builder.profilePictureUri(defaultProfile.getProfilePictureUri());
        }
        return builder.build();
    }

    private static ActivityObject.Builder activityObjectBuilder(ActivityObjEntity entity)
    {
        return ActivityObject.builder()
            .displayName(option(entity.getDisplayName()))
            .id(option(entity.getObjectId()))
            .type(option(entity.getObjectType()))
            .summary(option(entity.getSummary()).map(html()))
            .url(option(entity.getUrl()));
    };

    private static Function<ActivityEntity, Application> toApplication = new Function<ActivityEntity, Application>()
    {
        @Override
        public Application apply(ActivityEntity entity)
        {
            return Application.application(entity.getGeneratorDisplayName(), entity.getGeneratorId());
        }
    };

    /**
     * Returns the username if such a user exists on the system, or none() if not
     * @param username the username
     * @return the username if such a user exists on the system, or none() if not
     */
    private Option<String> getRegisteredUsername(String username)
    {
        return userManager.getUserProfile(username) == null ? none(String.class) : some(username);
    }

    private String stripHtml(String input)
    {
        return input.replaceAll("<[^>]*>", "");
    }

    private static String getColumnName(String property)
    {
        // This is really horrible.  ActiveObjects currently converts property names to SQL column
        // names by changing camelCase to under_scores, but the class that handles this conversion
        // is not exposed in the API, so we have to just assume that this is what it does.
        StringBuilder ret = new StringBuilder(property.length() * 2);
        for (int i = 0; i < property.length(); i++)
        {
            char ch = property.charAt(i);
            if (Character.isUpperCase(ch))
            {
                ret.append('_');
                ret.append(Character.toLowerCase(ch));
            }
            else
            {
                ret.append(ch);
            }
        }
        return ret.toString().toUpperCase();
    }

    private WhereClause whereClause(WhereClause.Separator separator)
    {
        return new WhereClause(separator);
    }

    /**
     * Internal class for building a WHERE clause for a {@link Query} instance.
     */
    static class WhereClause
    {
        private final StringBuilder where = new StringBuilder();
        private final ImmutableList.Builder<Object> list = ImmutableList.builder();
        private final Separator separator;

        WhereClause(Separator separator)
        {
            this.separator = separator;
        }

        boolean isEmpty()
        {
            return (where.length() == 0);
        }
        
        /**
         * Conditionally add a new clause to the WHERE clause.
         *
         * @param option if specified, the value with which to filter
         * @param property the property name to filter against
         * @param operator the operator to use in the filtering
         * @return this {@link WhereClause} instance
         */
        WhereClause add(Option<?> option, String property, Operator operator)
        {
            if (option.isDefined())
            {
                String column = getColumnName(property);

                if (where.length() > 0)
                {
                    where.append(separator.value());
                }

                where.append(column)
                    .append(" ")
                    .append(operator.value())
                    .append(" ?");
                list.add(option.get());
            }
            return this;
        }

        WhereClause addIsNull(String property)
        {
            String column = getColumnName(property);
            if (where.length() > 0)
            {
                where.append(separator.value());
            }
            where.append(column).append(" IS NULL");
            return this;
        }

        /**
         * Add a subclause to the WHERE clause.
         * @param subClause a previously constructed {@link WhereClause} instance
         * @return this {@link WhereClause} instance
         */
        WhereClause add(WhereClause subClause)
        {
            if (subClause.where.length() > 0)
            {
                if (where.length() > 0)
                {
                    where.append(separator.value());
                }
                where.append("(");
                where.append(subClause.getClause());
                where.append(")");
                list.addAll(subClause.list.build());
            }
            return this;
        }

        String getClause()
        {
            return where.toString();
        }

        Object[] getParams()
        {
            return list.build().toArray();
        }

        static enum Separator
        {
            AND(" and "),
            OR(" or ");

            private final String value;

            private Separator(String value)
            {
                this.value = value;
            }

            String value()
            {
                return value;
            }
        }

        static enum Operator
        {
            EQ("="),
            NEQ("!="),
            GT(">"),
            GT_EQ(">="),
            LT("<"),
            LT_EQ("<=");

            private final String value;

            private Operator(String value)
            {
                this.value = value;
            }

            String value()
            {
                return value;
            }
        }
    }
}
