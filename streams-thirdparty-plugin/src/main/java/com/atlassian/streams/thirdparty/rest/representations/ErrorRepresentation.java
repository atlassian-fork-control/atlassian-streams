package com.atlassian.streams.thirdparty.rest.representations;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * JSON representation of an error response from the Activity REST API.
 */
public class ErrorRepresentation
{
    @JsonProperty private final String errorMessage;
    @JsonProperty private final String subCode;

    /**
     * Constructor for use by Jackson
     * @param errorMessage
     * @param subCode
     */
    @JsonCreator
    public ErrorRepresentation(@JsonProperty("errorMessage") String errorMessage,
                               @JsonProperty("subCode") String subCode)
    {
        this.errorMessage = errorMessage;
        this.subCode = subCode;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public String getSubCode()
    {
        return subCode;
    }
}
