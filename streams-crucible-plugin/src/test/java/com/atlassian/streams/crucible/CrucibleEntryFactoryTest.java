package com.atlassian.streams.crucible;

import com.atlassian.fecru.user.FecruUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.State;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.getActivityObjectTypes;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.crucible.CrucibleActivityObjectTypes.review;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.abandon;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.close;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.complete;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.reopen;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.start;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.uncomplete;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.REVIEW_CLOSE_REL;
import static com.atlassian.streams.crucible.CrucibleStreamsActivityProvider.REVIEW_SUMMARIZE_AND_CLOSE_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.testing.matchers.Matchers.hasStreamsLink;
import static com.atlassian.streams.testing.matchers.Matchers.whereStreamsRel;
import static com.cenqua.crucible.model.managers.LogAction.COMMENT_ADDED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_COMPLETED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_CREATED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_STATE_CHANGE;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_UNCOMPLETED;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrucibleEntryFactoryTest
{
    private static final URI BASE_URI = URI.create("http://localhost:3990/streams");
    private static final String USERNAME_1 = "admin";
    private static final String USERNAME_2 = "fred";
    private static final UserProfile USERPROFILE_1 = new UserProfile.Builder(USERNAME_1).build();
    private static final UserProfile USERPROFILE_2 = new UserProfile.Builder(USERNAME_2).build();
    private static final Integer REVIEW_ID = 0;
    private static final int COMMENT_ID = 1;
    private static final DateTime TIMESTAMP = new DateTime();
    private static final DateTime REVIEW_TIMESTAMP = new DateTime().minusSeconds(1);

    private CrucibleEntryFactoryImpl entryFactory;
    private UriProvider uriProvider;
    @Mock
    private ActivityRequest request;

    @Mock
    private Comment comment;
    @Mock
    private Review review;
    @Mock
    private State state;
    @Mock
    private FecruUser user1;
    @Mock
    private FecruUser user2;
    @Mock
    private StreamsCrucibleActivityItem reviewLogData;
    @Mock
    private StreamsCrucibleActivityItem commentLogData;
    @Mock
    private CrucibleCommentManager commentManager;
    @Mock
    private UserManager userManager;
    @Mock
    private StreamsEntry.Renderer commentRenderer;
    @Mock
    private UserProfileAccessor userProfileAccessor;
    @Mock
    private ReviewRendererFactory reviewRendererFactory;
    @Mock
    private StreamsEntry.Renderer renderer;
    @Mock
    private StreamsI18nResolver i18nResolver;
    @Mock
    private CruciblePermissionAccessor permissionAccessor;
    @Mock
    private ApplicationProperties applicationProperties;

    @Before
    public void setup()
    {
        when(i18nResolver.getText("streams.item.crucible.tooltip")).thenReturn("tooltip");
        when(reviewRendererFactory.newCommentRenderer(anyString(), anyBoolean())).thenReturn(commentRenderer);
        when(reviewRendererFactory.newRenderer(any(), eq(review), any())).thenReturn(renderer);

        uriProvider = new UriProvider();
        entryFactory =
            new CrucibleEntryFactoryImpl(commentManager,
                                         userProfileAccessor,
                                         uriProvider,
                                         reviewRendererFactory,
                                         userManager,
                                         i18nResolver,
                                         permissionAccessor,
                                         applicationProperties);

        setupUsers();
        setupReview();
        setupReviewLogData();
        setupComment();
        setupCommentLogData();
    }

    private void setupUsers()
    {
        when(user1.getUsername()).thenReturn(USERNAME_1);
        when(user2.getUsername()).thenReturn(USERNAME_2);

        when(userProfileAccessor.getUserProfile(BASE_URI, USERNAME_1)).thenReturn(USERPROFILE_1);
        when(userProfileAccessor.getUserProfile(BASE_URI, USERNAME_2)).thenReturn(USERPROFILE_2);

        when(userManager.getRemoteUsername()).thenReturn(USERNAME_1);
    }

    private void setupReview()
    {
        when(review.getAuthor()).thenReturn(user1);
        when(review.getId()).thenReturn(REVIEW_ID);
        when(review.getCreateDate()).thenReturn(REVIEW_TIMESTAMP.toDate());
        when(review.getPermaId()).thenReturn("CR-1");
        when(review.getName()).thenReturn("Review 1");
        when(review.isAllReviewersComplete()).thenReturn(true);
        when(review.getState()).thenReturn(state);
        when(state.isReviewState()).thenReturn(true);
        when(state.isSummarizeState()).thenReturn(false);
    }

    private void setupReviewLogData()
    {
        when(reviewLogData.getUsername()).thenReturn(USERNAME_1);
        when(reviewLogData.getCreateDate()).thenReturn(TIMESTAMP.toDate());
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_CREATED);
        when(reviewLogData.getReview()).thenReturn(review);
    }

    private void setupComment()
    {
        when(comment.getMessage()).thenReturn("An insightful message");
        when(comment.getUser()).thenReturn(user2);
        when(comment.getReview()).thenReturn(review);
        when(comment.getId()).thenReturn(COMMENT_ID);
        when(comment.isDeleted()).thenReturn(false);
        when(comment.getDefectRaised()).thenReturn(false);
        when(commentManager.getById(COMMENT_ID)).thenReturn(comment);
    }

    private void setupCommentLogData()
    {
        when(commentLogData.getEntityId()).thenReturn(COMMENT_ID);
        when(commentLogData.getLogAction()).thenReturn(COMMENT_ADDED);
    }

    @Test
    public void testGetActivityObjectTypeForComment()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(commentLogData, post()), request), BASE_URI).get();
        assertThat(getActivityObjectTypes(entry.getActivityObjects()), hasItem(comment()));
    }

    @Test
    public void testGetActivityObjectTypeForReview()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        assertThat(getActivityObjectTypes(entry.getActivityObjects()), hasItem(review()));
    }

    @Test
    public void verifyCommentKeyIsUsedHtmlForRenderingCommentTitle()
    {
        entryFactory.getEntryFromActivityItem(pair(pair(commentLogData, post()), request), BASE_URI).get();
        verify(reviewRendererFactory).newCommentRenderer(anyString(), eq(false));
    }

    @Test
    public void verifyPostReviewKeyIsUsedHtmlForRenderingReviewCreatedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_CREATED);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(post(), review, BASE_URI);
    }

    @Test
    public void verifyReopenReviewKeyIsUsedHtmlForRenderingReviewReopenedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, reopen()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(reopen(), review, BASE_URI);
    }

    @Test
    public void verifySummarizeReviewKeyIsUsedHtmlForRenderingReviewSummarizedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, summarize()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(summarize(), review, BASE_URI);
    }

    @Test
    public void verifyCloseReviewKeyIsUsedHtmlForRenderingReviewClosedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, close()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(close(), review, BASE_URI);
    }

    @Test
    public void verifyCompleteReviewKeyIsUsedHtmlForRenderingReviewCompletedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_COMPLETED);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(complete(), review, BASE_URI);
    }

    @Test
    public void verifyUncompleteReviewKeyIsUsedHtmlForRenderingReviewUncompletedTitle()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_UNCOMPLETED);

        entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, uncomplete()), request), BASE_URI).get();
        verify(reviewRendererFactory).newRenderer(uncomplete(), review, BASE_URI);
    }

    /**
     * Verify that the entry's author is the change's user instead of the review's owner.
     */
    @Test
    public void testAuthorForReviewChange()
    {
        when(reviewLogData.getUsername()).thenReturn(USERNAME_2);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        assertThat(entry.getAuthors(), contains(USERPROFILE_2));
    }

    @Test
    public void testTimestampIsTakenFromStateChangeLog()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, summarize()), request), BASE_URI).get();
        assertThat(entry.getPostedDate(), is(equalTo(TIMESTAMP)));
    }

    @Test
    public void testTimestampIsTakenFromReviewIfActionIsCreate()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        assertThat(entry.getPostedDate(), is(equalTo(REVIEW_TIMESTAMP)));
    }

    @Test
    public void testCategoryForComment()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(commentLogData, post()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("commented"));
    }

    @Test
    public void testCategoryForCreatedReview()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_CREATED);
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("post"));
    }

    @Test
    public void testCategoryForReopenedReview()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, reopen()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("reopen"));
    }

    @Test
    public void testCategoryForSummarizedReview()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, summarize()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("summarize"));
    }

    @Test
    public void testCategoryForClosedReview()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, close()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("close"));
    }

    @Test
    public void testGetIconUrlForReview()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI).get();
        assertThat(getIconLink(entry).toASCIIString(),
                is(equalTo(
                    "http://localhost:3990/streams/download/resources/com.atlassian.streams.crucible/images/review.gif")));
    }

    @Test
    public void testCategoryForReviewCompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_COMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("complete"));
    }

    @Test
    public void testGetIconUrlForReviewCompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_COMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI).get();
        assertThat(getIconLink(entry).toASCIIString(),
                is(equalTo("http://localhost:3990/streams/download/resources/com.atlassian.streams.crucible/images/review.gif")));
    }

    @Test
    public void testGetActivityObjectTypeForReviewCompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_COMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI).get();
        assertThat(getActivityObjectTypes(entry.getActivityObjects()), hasItem(review()));
    }

    @Test
    public void testCategoryForReviewUncompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_UNCOMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, uncomplete()), request), BASE_URI).get();
        assertThat(entry.getCategories(), contains("uncomplete"));
    }

    @Test
    public void testGetIconUrlForReviewUncompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_UNCOMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, uncomplete()), request), BASE_URI).get();
        assertThat(getIconLink(entry).toASCIIString(),
                is(equalTo("http://localhost:3990/streams/download/resources/com.atlassian.streams.crucible/images/review.gif")));
    }

    @Test
    public void testGetActivityObjectTypeForReviewUncompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_UNCOMPLETED);

        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, uncomplete()), request), BASE_URI).get();
        assertThat(getActivityObjectTypes(entry.getActivityObjects()), hasItem(review()));
    }

    @Test
    public void testGetVerbsForComment()
    {
        StreamsEntry entry = entryFactory.getEntryFromActivityItem(pair(pair(commentLogData, post()), request), BASE_URI).get();
        assertThat(entry.getVerb(), is(equalTo(post())));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewCreated()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_CREATED);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, post()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewCompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_COMPLETED);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewUncompleted()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_UNCOMPLETED);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, uncomplete()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForCommentAdded()
    {
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(commentLogData, post()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewReopened()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, reopen()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewAbandoned()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, abandon()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewApproved()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, start()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewClosed()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, close()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryExistsByDefaultForReviewSummarized()
    {
        when(reviewLogData.getLogAction()).thenReturn(REVIEW_STATE_CHANGE);
        assertThat(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, summarize()), request), BASE_URI), is(not(none(StreamsEntry.class))));
    }

    @Test
    public void testStreamsEntryHasSummarizeAndCloseLinkWhenAllConditionsAreMetAndCurrentUserIsAuthor()
    {
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   hasStreamsLink(whereStreamsRel(equalTo(REVIEW_SUMMARIZE_AND_CLOSE_REL))));
    }

    @Test
    public void testStreamsEntryHasSummarizeAndCloseLinkWhenAllConditionsAreMetAndCurrentUserIsModerator()
    {
        when(review.getAuthor()).thenReturn(user2);
        when(review.getModerator()).thenReturn(user1);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   hasStreamsLink(whereStreamsRel(equalTo(REVIEW_SUMMARIZE_AND_CLOSE_REL))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveSummarizeAndCloseLinkWhenNotTheReviewOwnerOrModerator()
    {
        when(review.getAuthor()).thenReturn(user2);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_SUMMARIZE_AND_CLOSE_REL)))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveSummarizeAndCloseLinkWhenReviewHasUncompletedReviewers()
    {
        when(review.isAllReviewersComplete()).thenReturn(false);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_SUMMARIZE_AND_CLOSE_REL)))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveSummarizeAndCloseLinkWhenReviewIsNotInReviewState()
    {
        when(state.isReviewState()).thenReturn(false);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_SUMMARIZE_AND_CLOSE_REL)))));
    }

    @Test
    public void testStreamsEntryHasCloseLinkWhenAllConditionsAreMetAndCurrentUserIsAuthor()
    {
        when(state.isReviewState()).thenReturn(false);
        when(state.isSummarizeState()).thenReturn(true);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   hasStreamsLink(whereStreamsRel(equalTo(REVIEW_CLOSE_REL))));
    }

    @Test
    public void testStreamsEntryHasCloseLinkWhenAllConditionsAreMetAndCurrentUserIsModerator()
    {
        when(state.isReviewState()).thenReturn(false);
        when(state.isSummarizeState()).thenReturn(true);
        when(review.getAuthor()).thenReturn(user2);
        when(review.getModerator()).thenReturn(user1);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   hasStreamsLink(whereStreamsRel(equalTo(REVIEW_CLOSE_REL))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveCloseLinkWhenNotTheReviewOwnerOrModerator()
    {
        when(review.getAuthor()).thenReturn(user2);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_CLOSE_REL)))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveCloseLinkWhenReviewHasUncompletedReviewers()
    {
        when(state.isReviewState()).thenReturn(false);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_CLOSE_REL)))));
    }

    @Test
    public void testStreamsEntryDoesNotHaveCloseLinkWhenReviewIsNotInSummarizeState()
    {
        when(state.isReviewState()).thenReturn(false);
        assertThat(getLinks(entryFactory.getEntryFromActivityItem(pair(pair(reviewLogData, complete()), request), BASE_URI)),
                   not(hasStreamsLink(whereStreamsRel(equalTo(REVIEW_CLOSE_REL)))));
    }

    private Iterable<StreamsEntry.Link> getLinks(Option<StreamsEntry> entryOption)
    {
        return entryOption.map(entry -> entry.getLinks().values()).getOrElse(ImmutableList.of());
    }

    private static URI getIconLink(StreamsEntry entry)
    {
        return getOnlyElement(entry.getLinks().get(ICON_LINK_REL)).getHref();
    }
}
