package com.atlassian.streams.crucible;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Project;
import com.cenqua.crucible.model.Review;

/**
 * Exists as a wrapper around Crucible's classes which are heavily coupled with Hibernate in order to mock them out for our unit tests.
 */
public interface CruciblePermissionAccessor
{
    /**
     * Returns true if the current user (logged in or anonymous) can view the specified {@link Review}, false if not.
     *
     * @param review the review
     * @return true if the current user (logged in or anonymous) can view the specified {@link Review}, false if not.
     */
    boolean currentUserCanView(Review review);

    /**
     * Returns true if the current user (logged in or anonymous) can view the specified {@link Project}, false if not.
     *
     * @param project the project
     * @return true if the current user (logged in or anonymous) can view the specified {@link Project}, false if not.
     */
    boolean currentUserCanView(Project project);

    /**
     * Returns true if the current user (logged in or anonymous) can reply to the specified {@link Comment}, false if not.
     *
     * @param comment the comment
     * @return true if the current user (logged in or anonymous) can reply to the specified {@link Comment}, false if not.
     */
    boolean currentUserCanReplyToComment(Comment comment);

    /**
     * Returns true if the current user (logged in or anonymous) can comment on the specified {@link Review}, false if not.
     *
     * @param review the review
     * @return true if the current user (logged in or anonymous) can comment on the specified {@link Review}, false if not.
     */
    boolean currentUserCanComment(Review review);

    /**
     * Returns all {@link Project}s visible to the current user.
     * 
     * @return all {@link Project}s visible to the current user.
     */
    Iterable<Project> getCurrentlyVisibleProjects();
}
