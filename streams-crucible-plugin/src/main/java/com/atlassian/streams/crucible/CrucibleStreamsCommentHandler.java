package com.atlassian.streams.crucible;

import com.atlassian.crucible.spi.TxCallback;
import com.atlassian.fecru.user.FecruUser;
import com.atlassian.fisheye.spi.TxTemplate;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.spi.StreamsCommentHandler;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.ReviewManager;
import org.springframework.transaction.TransactionStatus;

import java.net.URI;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.DELETED_OR_PERMISSION_DENIED;
import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.UNAUTHORIZED;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.size;

public class CrucibleStreamsCommentHandler implements StreamsCommentHandler
{
    private final UserManager salUserManager;
    private final ReviewManager reviewManager;
    private final UriProvider uriProvider;
    private final CrucibleCommentManager commentManager;
    private final TxTemplate txTemplate;
    private final com.cenqua.fisheye.user.UserManager cenquaUserManager;
    private final CruciblePermissionAccessor permissionAccessor;
    private final ApplicationProperties applicationProperties;

    public CrucibleStreamsCommentHandler(final CrucibleCommentManager commentManager,
                                         final UserManager salUserManager,
                                         final ReviewManager reviewManager,
                                         final UriProvider uriProvider,
                                         final TxTemplate txTemplate,
                                         final com.cenqua.fisheye.user.UserManager cenquaUserManager,
                                         final CruciblePermissionAccessor permissionAccessor,
                                         final ApplicationProperties applicationProperties)
    {
        this.commentManager = checkNotNull(commentManager, "commentManager");
        this.salUserManager = checkNotNull(salUserManager, "salUserManager");
        this.reviewManager = checkNotNull(reviewManager, "reviewManager");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
        this.cenquaUserManager = checkNotNull(cenquaUserManager, "cenquaUserManager");
        this.permissionAccessor = checkNotNull(permissionAccessor, "permissionAccessor");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }

    @Override
    public Either<PostReplyError, URI> postReply(final URI baseUri,
                                                 final Iterable<String> itemPath,
                                                 final String comment)
    {
        checkArgument(size(itemPath) == 2, "Item path must contain exactly 2 parts.");
        int reviewId = Integer.parseInt(get(itemPath, 0));
        int commentId = Integer.parseInt(get(itemPath, 1));
        FecruUser user = cenquaUserManager.getUser(salUserManager.getRemoteUsername());

        Review review = reviewManager.getReviewById(reviewId);
        if (review == null)
        {
            return left(new PostReplyError(DELETED_OR_PERMISSION_DENIED));
        }
        else if (!permissionAccessor.currentUserCanComment(review))
        {
            return left(new PostReplyError(UNAUTHORIZED));
        }

        Comment replyToComment = commentManager.getById(commentId);
        if (replyToComment == null)
        {
            return left(new PostReplyError(DELETED_OR_PERMISSION_DENIED));
        }
        else if (!permissionAccessor.currentUserCanReplyToComment(replyToComment))
        {
            return left(new PostReplyError(UNAUTHORIZED));
        }

        Comment newComment = addReply(comment, replyToComment, review, user);
        return right(uriProvider.getCommentUri(baseUri, newComment));
    }

    @Override
    public Either<PostReplyError, URI> postReply(final Iterable<String> itemPath, final String comment)
    {
        return postReply(URI.create(applicationProperties.getBaseUrl()), itemPath, comment);
    }

    protected Comment addReply(final String content,
                               final Comment inReplyTo,
                               final Review review,
                               final FecruUser user)
    {
        return txTemplate.execute(createCommentCreatorCallback(content, inReplyTo, review, user));
    }

    protected CommentCreatorCallback createCommentCreatorCallback(String content, Comment inReplyTo, Review review, FecruUser user)
    {
        return new CommentCreatorCallback(content, inReplyTo, review, user);
    }

    protected class CommentCreatorCallback implements TxCallback<Comment>
    {
        private final String content;
        private final Comment inReplyTo;
        private final Review review;
        private final FecruUser user;

        public CommentCreatorCallback(String content, Comment inReplyTo, Review review, FecruUser user)
        {
            super();
            this.content = content;
            this.inReplyTo = inReplyTo;
            this.review = review;
            this.user = user;
        }

        @Override
        public Comment doInTransaction(TransactionStatus status) throws Exception
        {
            Comment newComment = commentManager.createComment(content, review, user);
            newComment.setDeleted(false);
            newComment.setDraft(false);
            inReplyTo.addComment(newComment);
            commentManager.announceCommentCreation(review, newComment);
            return newComment;
        }
    }

}
