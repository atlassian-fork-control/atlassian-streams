package com.atlassian.streams.crucible;

import com.atlassian.fisheye.spi.TxTemplate;

import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Principal;
import com.cenqua.crucible.model.Project;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.UserActionManager;

import static com.cenqua.crucible.model.managers.impl.DefaultPermissionManager.getProjectsPrincipalCanView;
import static com.cenqua.crucible.tags.ReviewUtil.principalCanDoProjectAction;
import static com.cenqua.crucible.tags.ReviewUtil.principalCanDoReviewAction;
import static com.google.common.base.Preconditions.checkNotNull;

public class CruciblePermissionAccessorImpl implements CruciblePermissionAccessor
{
    private final TxTemplate txTemplate;
    private final CrucibleCommentManager commentManager;

    public CruciblePermissionAccessorImpl(TxTemplate txTemplate, CrucibleCommentManager commentManager)
    {
        this.txTemplate = checkNotNull(txTemplate, "txTemplate");
        this.commentManager = checkNotNull(commentManager, "commentManager");
    }

    public boolean currentUserCanView(Review review)
    {
        try
        {
            return principalCanDoReviewAction(getCurrentUser(), UserActionManager.ACTION_VIEW, review, null);
        }
        catch (NullPointerException e)
        {
            // for some reason, IDOG is throwing a NPE when getting the PermissionScheme id in
            // com.cenqua.crucible.tags.ReviewUtil$PrincipalReviewKey.create (STRM-828)
            return false;
        }
    }

    public boolean currentUserCanView(Project project)
    {
        return principalCanDoProjectAction(getCurrentUser(), UserActionManager.ACTION_VIEW, project);
    }

    public boolean currentUserCanReplyToComment(Comment comment)
    {
        return currentUserCanComment(comment.getReview());
    }

    public boolean currentUserCanComment(Review review)
    {
        return review.checkWriteAccess(txTemplate.getEffectiveCrucibleUser()) && commentManager.canAddComment(review);
    }

    public Iterable<Project> getCurrentlyVisibleProjects()
    {
        return getProjectsPrincipalCanView(getCurrentUser());
    }

    /**
     * Returns the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     * @return the current {@code Principal}, or {@code Principal.Anonymous} if not logged in.
     */
    private Principal getCurrentUser()
    {
        return txTemplate.getEffectivePrincipal();
    }
}
