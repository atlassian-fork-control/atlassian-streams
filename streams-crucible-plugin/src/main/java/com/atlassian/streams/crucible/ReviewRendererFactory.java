package com.atlassian.streams.crucible;

import java.net.URI;
import java.util.Set;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.cenqua.crucible.model.FileRevisionExtraInfo;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.ReviewParticipant;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Functions.trimToNone;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.createAndStart;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarize;
import static com.atlassian.streams.crucible.CrucibleActivityVerbs.summarizeAndClose;
import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.atlassian.streams.spi.renderer.Renderers.stripBasicMarkup;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.filter;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ReviewRendererFactory
{
    private final TemplateRenderer templateRenderer;
    private final UserProfileAccessor userProfileAccessor;
    private final StreamsEntryRendererFactory rendererFactory;
    private final UriProvider uriProvider;
    private final Function<Iterable<UserProfile>, Html> reviewersRenderer;

    public ReviewRendererFactory(final TemplateRenderer templateRenderer,
            final UserProfileAccessor userProfileAccessor,
            final StreamsEntryRendererFactory rendererFactory,
            final UriProvider uriProvider)
    {
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.userProfileAccessor = checkNotNull(userProfileAccessor, "userProfileAccessor");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        reviewersRenderer = rendererFactory.newUserProfileRenderer();
    }

    public Renderer newRenderer(final ActivityVerb verb, final Review review, URI baseUri)
    {
        String key = "streams.item.crucible.action." + verb.key() + ".review";

        if (post().equals(verb) || createAndStart().equals(verb))
        {
            return new CreatedReviewRenderer(baseUri, key, review);
        }
        else if (summarize().equals(verb) || summarizeAndClose().equals(verb))
        {
            return new SummarizeReviewRenderer(key, review);
        }
        return new DefaultReviewRenderer(key);
    }

    public Renderer newCommentRenderer(String message, boolean defect)
    {
        final Function<StreamsEntry, Html> titleRenderer = defect ?
                rendererFactory.newTitleRenderer("streams.item.crucible.action.raised") :
                    rendererFactory.newTitleRenderer("streams.item.crucible.action.commented");

        return rendererFactory.newCommentRenderer(titleRenderer, message);
    }

    private final class DefaultReviewRenderer extends ReviewRenderer
    {
        public DefaultReviewRenderer(final String key)
        {
            super(key);
        }

        @Override
        public Option<Html> renderContentAsHtml(final StreamsEntry entry)
        {
            return none();
        }
    }

    private final class CreatedReviewRenderer extends ReviewRenderer
    {
        private final URI baseUri;
        private final Review review;

        public CreatedReviewRenderer(final URI baseUri, final String key, final Review review)
        {
            super(key);
            this.baseUri = baseUri;
            this.review = review;
        }

        @Override
        public Option<Html> renderContentAsHtml(final StreamsEntry entry)
        {
            return some(new Html(buildReviewDescription(entry)));
        }

        private String buildReviewDescription(final StreamsEntry entry)
        {
            ImmutableMap.Builder<String, Object> context = ImmutableMap.builder();

            ImmutableSet.Builder<FileRevisionExtraInfo> bld = ImmutableSet.builder();
            bld.addAll(review.getFrxs());

            String description = stripBasicMarkup(review.getDescription());
            context.put("review", review).
                put("reviewFrxs", bld.build()).
                put("reviewDescription", description == null ? "" : description).
                put("reviewersRenderer", reviewersRenderer).
                put("reviewers", getReviewers(baseUri, review)).
                put("reviewProjectUri", uriProvider.getProjectUri(baseUri, review.getProject()).toASCIIString());

            return render(templateRenderer, "review-details.vm", context.build());
        }

        private Set<UserProfile> getReviewers(final URI baseUri, final Review review)
        {
            return ImmutableSet.copyOf(transform(filter(review.getParticipants(), reviewers),
                new Function<ReviewParticipant, UserProfile>()
                {
                    public UserProfile apply(final ReviewParticipant participant)
                    {
                        return userProfileAccessor.getUserProfile(baseUri, participant.getUser().getUsername());
                    }
                }));
        }
    }

    private final class SummarizeReviewRenderer extends ReviewRenderer
    {
        private final Review review;

        public SummarizeReviewRenderer(final String key, final Review review)
        {
            super(key);
            this.review = review;
        }

        @Override
        public Option<Html> renderContentAsHtml(final StreamsEntry entry)
        {
            return some(new Html(buildSummaryContent()));
        }

        private String buildSummaryContent()
        {
            return render(templateRenderer, "summarize-content.vm", ImmutableMap
                .<String, Object>of("reviewSummary", summary(review).map(stripBasicMarkup()).flatMap(trimToNone())));
        }

        private Option<String> summary(Review review)
        {
            if (review == null || isBlank(review.getSummary()))
            {
                return none();
            }
            return some(review.getSummary());
        }
    }

    private abstract class ReviewRenderer implements Renderer
    {
        private final Function<StreamsEntry, Html> titleRenderer;

        public ReviewRenderer(final String key)
        {
            this.titleRenderer = rendererFactory.newTitleRenderer(key);
        }

        @Override
        public Option<Html> renderSummaryAsHtml(final StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Html renderTitleAsHtml(final StreamsEntry entry)
        {
            return titleRenderer.apply(entry);
        }
    }

    private final Predicate<ReviewParticipant> reviewers = new Predicate<ReviewParticipant>()
    {
        public boolean apply(final ReviewParticipant participant)
        {
            return participant.getUser() != null && participant.isReviewer();
        }
    };
}
