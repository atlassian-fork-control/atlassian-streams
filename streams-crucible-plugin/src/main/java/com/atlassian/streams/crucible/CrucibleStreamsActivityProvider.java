package com.atlassian.streams.crucible;

import java.util.Set;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.StreamsActivityProvider;

import com.cenqua.crucible.model.managers.LogAction;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import static com.atlassian.streams.api.StreamsEntry.byPostedDate;
import static com.atlassian.streams.api.common.Iterables.memoize;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.cenqua.crucible.model.managers.LogAction.COMMENT_ADDED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_COMPLETED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_CREATED;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_STATE_CHANGE;
import static com.cenqua.crucible.model.managers.LogAction.REVIEW_UNCOMPLETED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class CrucibleStreamsActivityProvider implements StreamsActivityProvider
{
    public static final String REVIEW_SUMMARIZE_AND_CLOSE_REL = "http://streams.atlassian.com/syndication/review-summarize-close";
    public static final String REVIEW_CLOSE_REL = "http://streams.atlassian.com/syndication/review-close";
    public static final String PROVIDER_KEY = "reviews";

    static final Set<LogAction> SUPPORTED_ACTIONS =
        ImmutableSet.of(REVIEW_CREATED, REVIEW_COMPLETED, REVIEW_UNCOMPLETED, REVIEW_STATE_CHANGE, COMMENT_ADDED);

    private final CrucibleEntryFactory crucibleEntryFactory;
    private final I18nResolver i18nResolver;
    private final CrucibleReviewFinder finder;
    private final CrucibleActivityItemAggregator aggregator;

    public CrucibleStreamsActivityProvider(CrucibleEntryFactory crucibleEntryFactory,
           I18nResolver i18nResolver,
           CrucibleReviewFinder finder,
           CrucibleActivityItemAggregator aggregator)
    {
        this.crucibleEntryFactory = checkNotNull(crucibleEntryFactory, "crucibleEntryFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.finder = checkNotNull(finder, "finder");
        this.aggregator = checkNotNull(aggregator, "aggregator");
    }

    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest request) throws StreamsException
    {
        return new CancellableTask<StreamsFeed>()
        {
            @Override
            public StreamsFeed call() throws Exception
            {
                Iterable<StreamsEntry> entries = getCrucibleEntries(request);
                Iterable<StreamsEntry> sortedEntries = byPostedDate().sortedCopy(entries);

                return new StreamsFeed(i18nResolver.getText("streams.feed.crucible.title"),
                                       take(request.getMaxResults(), sortedEntries), none(String.class));
            }

            @Override
            public Result cancel()
            {
                return Result.INTERRUPT;
            }
        };
    }

    public Iterable<StreamsEntry> getCrucibleEntries(final ActivityRequest activityRequest)
    {
        Iterable<Pair<StreamsCrucibleActivityItem, ActivityVerb>> activityItem = finder.getNotificationsFor(activityRequest);
        Iterable<Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest>> logEntries = transform(aggregator.aggregate(activityItem), new PairRequestWithActivityItem(activityRequest));

        return memoize(catOptions(transform(logEntries,
                new Function<Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest>, Option<StreamsEntry>>()
                {
                    @Override
                    public Option<StreamsEntry> apply(Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>,
                                                      ActivityRequest> activityEntry)
                    {
                        // getting entry properties may be expensive, so check for interrupts here
                        CancelledException.throwIfInterrupted();

                        return crucibleEntryFactory.getEntryFromActivityItem(activityEntry, activityRequest.getContextUri());
                    }
                })));
    }

    private static final class PairRequestWithActivityItem implements Function<Pair<StreamsCrucibleActivityItem, ActivityVerb>, Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest>>
    {
        private final ActivityRequest request;

        public PairRequestWithActivityItem(ActivityRequest request)
        {
            this.request = request;
        }

        @Override
        public Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest> apply(Pair<StreamsCrucibleActivityItem, ActivityVerb> activityItem)
        {
            return pair(activityItem, request);
        }
    }
}
